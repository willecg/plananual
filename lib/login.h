/*
 * liblogin: login.h
 * Perform a graphical and standar login on a application.
 * Only for use on Gtk
 *
 * Copyright (C) 2014 William Ernesto Cárdenas Gomez
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef LOGIN_H
#define LOGIN_H


#include <gtkmm/entry.h>
#include <gtkmm/builder.h>
#include <gtkmm/applicationwindow.h>
#include <gtkmm/button.h>
#include <gtkmm/application.h>

/**\brief Login operations.
 *
 * Have all the stuff to do a simple login Gtk operation.*/
namespace login{

/* Perform a login operation */
/**\brief Perform a login operation.
 *
 * Esta clase muestra una venta que permite escribir las credenciales
 * necesarias para acceder. Si todo sale bién cualquier programa puede
 * continuar su operación en caso de error se muestra el error
 * correspondiente. */
class Login
{
private:
    Glib::RefPtr<Gtk::Application> ap; // application.
    Gtk::ApplicationWindow *window; // window.
    Gtk::Button *b_ok; // Ok button.
    Gtk::Button *b_cancel; // Cancel button .
    Gtk::Entry *e_user; // entry for the user name.
    Gtk::Entry *e_password; // entry for the user password.
    Glib::RefPtr<Gtk::Builder> builder; // the Builder reference.

    /* pointer to a function that performs the login operation */
    bool (*funcion_login)
        (const std::string *const,
         const std::string *const,
         std::string *);
    std::string errores; // Manages the errors.
    std::string user;    // User to be logged in.
    std::string password;// Password from the user.
    
public:
    Login();
    Login(
            bool (*)
            (const std::string *const,
             const std::string *const,
             std::string*),
            int, char**);// Builder of the class

    void close_window(); // close the window
    int open_window(); // Open the window
    void on_cancel_clicked(); // When click on cancel button
    void on_ok_clicked(); // when click on ok button
    void on_window_close(); // When you close this window

    /* Non GUI functions */
    std::string get_errores(); // By this way you can get the mistakes.
    std::string get_user(); // That function get the username to be processed.
    std::string get_password(); // Same than get_user but with password field.
private:
    void set_user(); // Get the user from the gui and set the user atribute.
    void set_password(); // Get the password from the gui and set this
                         // atribute.
};

}
#endif //- LOGIN_H
