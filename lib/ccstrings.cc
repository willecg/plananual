#include "ccstrings.h"
#include <iostream>

/**\brief Help to work with strings.
 *
 * The elements in this namespace make some adds to the standar header cstring
 * and std::string.*/
namespace ccstring{
/**\brief get a pair like a hash table.
 *
 * Gets a string (first parameter) and find a pair
 * separeted by the last parameter
 * @param "char *str" Full string.
 * @param "const char *key" Separator.
 * @param "const char *flag" id for the pair.
 * @return The pair of the flag */
std::string get_pairs(char *str, const char *key, const char *flag){
    char * ptr_key;
    ptr_key = strstr(str, key);
    
    ptr_key = strtok(ptr_key, flag);
    ptr_key = strtok(NULL, " ");
    
    return ptr_key;
}
}

