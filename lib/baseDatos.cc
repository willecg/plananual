/* libbaseDatos: baseDatos.cc
 * libbaseDatos is used to be an 
 * way to connect to a postgresql server.
 *
 * Copyright (C) 2014 William E. Cárdenas Gómez
 *
 * This file is part of libbaseDatos
 *
 * libbaseDatos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libbaseDatos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libbaseDatos.  If not, see <http://www.gnu.org/licenses/>.*
 */
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif // HAVE_CONFIG_H

#include "baseDatos.h"
#include "ccstrings.h"
using namespace ccstring;
/**\brief Namespace for the class BaseDatos and its helpers stuff.
 *
 * It contains the necesary for the operation of the class BaseDatos.*/
namespace db {
/*---------------------------------------------------------------------------*
 *                                                                           *
 *                      Constructors and destructors                         *
 *                                                                           * 
 *---------------------------------------------------------------------------*/
/**\brief Constructor
 *
 * This constructor gets an connection string
 * and gets all the fields from it.
 * @param "const sting url" the string connection to connect to the postgresql
 * server */
BaseDatos::BaseDatos(const string url) :
    pilaErrores(100)
{
    char * c_url = new char[url.length() + 1];
    std::strcpy(c_url, url.c_str());
    setUrl(c_url);
    setEstado(DESCONECTADO);
}

/**\brief Constructor
 *
 * This constructor make the url by every single field. 
 * @param "const string usuario" User name who is trying to connect
 * @param "const string password" Password of the user
 * @param "const string ip_servidor" Server IP or name of the host with the db.
 * @param "const string nombre_base_datos" Name of the data base
 * @param "const string puerto" Port that is used by postgresql
*/
BaseDatos::BaseDatos(
        const string usuario,
        const string password,
        const string ip_servidor,
        const string nombre_base_datos,
        const string puerto) :
    pilaErrores(100)
{
    setUrl(usuario, password, ip_servidor, nombre_base_datos, puerto);
    setEstado(DESCONECTADO);
}
        
/**\brief Destructor
 *
 * Default destructor*/
BaseDatos::~BaseDatos()
{}

/*---------------------------------------------------------------------------*
 *                                                                           *
 *                           Set and get functions                           *
 *                                                                           * 
 *---------------------------------------------------------------------------*/
//         set functions
/**\brief Set the username.
 *
 * Set a username field to the object, to the connection.
 * @param "const string usuario" The name of the user*/
void BaseDatos::setUsuario(const string usuario)
{
    this->usuario = usuario;
}

/**\brief Set the user password.
 *
 * Set the password attribute for the object.
 * @param "const string password" Password of the user */
void BaseDatos::setPassword(const string password)
{
    this->password = password;
}

/**\brief Set name of the host.
 *
 * Set the host by ip address or name of the Postgresql server
 * @param "const string host" Server IP or name */
void BaseDatos::setHost(const string host)
{
    this->host = host;
}

/**\brief Set the database name
 *
 * This is the way to entablish the data base name.
 * @param "const string nombre_base_datos" data base name */
void BaseDatos::setNombreBaseDatos(const string nombre_base_datos)
{
    nombreBaseDatos = nombre_base_datos;
}

/**\brief Sets the port number.
 * 
 * Set the network port number for the connection.
 * @param "const string puerto" port number*/
void BaseDatos::setPuerto(const string puerto)
{
    this->puerto = puerto;
}

/**\brief Sets the url to connect and fill all the members.
 *
 * Overload function to make a connection & fill the member with just one url.
 * This should contain database name, port, network address or name, user and
 * password.
 * @param "const string url" the url*/
void BaseDatos::setUrl(const string url)
{
    char *c_url = new char[url.length() + 1];
    std::strcpy(c_url, url.c_str());

    std::string iph = get_pairs(c_url, "host", "=");
    std::string dbn = get_pairs(c_url, "dbname", "=");
    std::string prt = get_pairs(c_url, "port", "=");
    std::string usr = get_pairs(c_url, "user", "=");
    std::string pas = get_pairs(c_url, "pass", "=");

    setUrl(usr, pas, iph, dbn, prt);
}

/**\brief Sets the url to connect and fill all the members.
 *
 * Overload function to make a connection and make the url from any of all
 * single field. 
 * @param "const string ip_servidor" Server IP or name of the host with the data
 * base
 * @param "const string nombre_base_datos" Name of the data base
 * @param "const string puerto" Port that is used by postgresql
 * @param "const string usuario" User name who is trying to connect
 * @param "const string password" Password of the user*/
void BaseDatos::setUrl(
                        const string usuario,
                        const string password,
                        const string ip_servidor,
                        const string nombre_base_datos,
                        const string puerto)
{
    setUsuario(usuario);
    setPassword(password);
    setHost(ip_servidor);
    setNombreBaseDatos(nombre_base_datos);
    setPuerto(puerto);

    string url = "host=";
    url.append(getHost());
    url.append(" dbname=");
    url.append(getNombreBaseDatos());
    url.append(" port=");
    url.append(getPuerto());
    url.append(" user=");
    url.append(getUsuario());
    url.append(" password=");
    url.append(getPassword());

    this->url = url;
}

/**\brief Sets the state of the data base connection
 *
 * Set the state of the connection DESCONECTADO or CONECTADO.
 * @param "const Estado estado" State of the data base connection*/
void BaseDatos::setEstado(const Estado estado)
{
    this->estado = estado;
}
    
//             Get functions
/**\brief Get the username to the connection
 *
 * Simply gets the user name for the object
 * @return The name of the user*/
string BaseDatos::getUsuario()
{
    return usuario;
}

/**\brief Get the user password
 *
 * Be carefull with this function.
 * @return Password of the user. */
string BaseDatos::getPassword()
{
    return password;
}
  
/**\brief Get the host by ip address or name of the Postgresql server
 *
 * By this you can get the name of the host.
 * @return Server IP or name */
string BaseDatos::getHost()
{
    return host;
}

/**\brief Get the database name.
 *
 * This member function returns the name of the database that contains the
 * information.
 * @return Database name */
string BaseDatos::getNombreBaseDatos()
{
    return nombreBaseDatos;
}

/**\brief Gets the port number.
 *
 * Get the network port number. For postgresql is usually 5432
 * @return Port number*/
string BaseDatos::getPuerto()
{
    return puerto;
}

/**\brief Gets the url to connect and fill all the members.
 *
 * Get the url used to make a session inside the data base.
 * @return the url*/
string BaseDatos::getUrl()
{
    return url;
}

/**\brief Gets the state of the data base connection.
 *
 * Get the entablish state DESCONECTADO or CONECTADO
 * @return State of the data base connection*/
Estado BaseDatos::getEstado()
{
    return estado;
}

/*---------------------------------------------------------------------------*
 *                                                                           *
 *           Member functions for operation of the class                     *
 *                                                                           *
 *---------------------------------------------------------------------------*/
/**\brief Make queries.
 *
 * Send queries to the postgresql data base server. Check the connection
 * status, send the query, fills the resultados object and if any error
 * returns false elsewere returns true 
 * @param "const std::string query" Query with the SQL instructions
 * @return True if success. False if failed */
bool BaseDatos::ejecutaQuery(const string query)
{
    setEstado(CONECTADO);
    try {
        pqxx::connection c(getUrl());
        pqxx::work txn(c);
        resultados = txn.exec(query);
        txn.commit();
    }
    catch (const pqxx::pqxx_exception &e) {
        setEstado(DESCONECTADO);
        addError(e.base().what());
    }
    return (getEstado()==CONECTADO? true: false);
}

/**\brief Gets as a string a data in resultados object member.
 *
 * Gets a data from the resultset of a query made previously. You should
 * indicate the position in the matrix using the arguments to indicate that.
 * If there is no information one object std::string is going to save in the
 * list of errors.
 * @param "const int registro" The row selected from data in the resultset
 * @param "const int campo" the column or field of the pqxx::result
 * @return The selected single data or null. */
string BaseDatos::regresaDato(const int registro, const int campo)
{
    string dato;
    if (campo <= nColumnas() && registro < nRegistros() &&
            getEstado() == CONECTADO) {
        try {
            dato = resultados[registro][campo].as<string>();
        }
        catch(pqxx::conversion_error &error){
            dato = "";
        }
    }
    else {
        dato = "";
        if (getEstado() == DESCONECTADO)
            addError(_("ERROR: No connected."));
        if (campo > nColumnas())
            addError(_("ERROR: Column out of the range."));
        if (registro >= nRegistros())
            addError(_("ERROR: Row out of the range."));
    }
    return dato;
}

/**\brief Gets the number of columns in the resultset.
 *
 * Is the number of columns or fields.
 * @return Number of columns in resultados.*/
int BaseDatos::nColumnas() 
{
    return resultados.columns();
}

/**\Gets the number of rows in the resultset.
 *
 * Gets the number of results getted by the query.
 * @return Number of rows in resultados. */
int BaseDatos::nRegistros() 
{
    return resultados.size();
}

//             Errors managment
/**\briefAdds an error to the vector of errors.
 *
 * If something bad pass this function should be called to save this mistake.
 * Later you can get them.
 * @param "const std::string error" The string error to be saved.*/
void BaseDatos::addError(const string error)
{
    pilaErrores.push_back(error);
}

/**\brief Gets and delete the last error from pilaErrores.
 *
 * If there is a problem during the execution of some function in this class
 * the function void BaseDatos::addError(const string error) should be called
 * to save the std::string that explain the mistake, so with this function you
 * can get this text.
 * @return The last error string */
string BaseDatos::getLastError()
{
    string error = pilaErrores.back();
    pilaErrores.pop_back();
    return error;
}
}
