#ifndef CCSTRINGS_H
#define CCSTRINGS_H
#include <cstring>
#include <string>
/* Help to work with strings */
namespace ccstring {
const int MAXSIZE = 100;

/* Gets a string (first parameter) and find a pair of the key
 * (second parameter) separeted by the last parameter */
std::string get_pairs(char *, const char *, const char *);
}
#endif // CCSTRINGS_H
