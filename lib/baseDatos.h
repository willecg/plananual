#ifndef BASEDATOS_H
#define BASEDATOS_H
/* libbaseDatos: baseDatos.h
 * libbaseDatos is used to be an 
 * way to connect to a postgresql server.
 *
 * Copyright (C) 2014 William E. Cárdenas Gómez
 *
 * This file is part of libbaseDatos
 *
 * libbaseDatos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libbaseDatos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libbaseDatos.  If not, see <http://www.gnu.org/licenses/>.*
 */

#include <pqxx/pqxx>
#include <glibmm/i18n.h>
#include <string>
#include <vector>
using std::string;

/*Namespace for the database class*/
namespace db{

const string default_host = "127.0.0.1";/**<The IP address for
                                          localhost*/
const string default_db = "postgres";/**<The default name of the
                                       dbms on postgresql*/
const string default_port = "5432";/**<Default port on postgresql*/
const string default_url = "host=127.0.0.1 dbname=postgres port=5432 \
            user=postgres password=postgres";/**<An URL with all
                                                default values*/

/**\brief Indicates the BaseDatos object status.
 *
 * When an object of BaseDatos class performs an operation there are two
 * posibles results one is if the operation is performed correctly */
enum Estado{
    CONECTADO,/**<When the operation are success*/
    DESCONECTADO/**<When the operation generate an mistake*/};

/* This is the way to connect to the data base */
/**\brief Esta clase permite conectarse y operar la base de datos.
 *
 * Los miembros de esta clase son los elementos para realizar una conexión y
 * desconexión segura a la base de datos, asi como las operaciones de
 * consulta, inserciones, actualizaciones y eliminaciones de registros.
 * También es posible realizar manejo de errores por medio de una pila de
 * ellos. */
class BaseDatos
{
/*-----------------------------------------------------------------------*
 *                                                                       *
 *                          Class's operations                           *
 *                                                                       *
 *-----------------------------------------------------------------------*/
public:
    /*Constructors*/
    /* This constructor gets an connection string
     * and gets all the fields from it. Default constructor */
    BaseDatos(const string = default_url);
    /* This constructor make the url by every one of the
     * single field */
    BaseDatos(
            const string,                 // user name
            const string,                 // user password 
            const string = default_host,  // host or IP addres
            const string = default_db,    // Data base name
            const string = default_port); // port for postgres
   
    /* Destructor */
    virtual ~BaseDatos();
    
    /*----------- Member functions for operation of the class -----------*/
    bool ejecutaQuery(const string);
    string regresaDato(const int, const int); // get data from column, row
    int nColumnas(); // gets the number of columns in the resultset
    int nRegistros(); // gets the number of rows in the resultset.
    
    /* Errors managment */
    void addError(const string error);
    string getLastError();
    
    /*------------------ Set and Get member functions -------------------*/
    // get funcions 
    string getUsuario();
    string getPassword();
    string getHost();
    string getNombreBaseDatos();
    string getPuerto();
    string getUrl();
    Estado getEstado();
    
    // set functions
        // Two ways to set the url. Are the same than the constructors
    void setUrl(string default_url);
    void setUrl(
            const string,                 // user name
            const string,                 // user password
            const string = default_host,  // host or IP addres
            const string = default_db,    // Data base name
            const string = default_port); // port for postgres
    void setUsuario(const string);
    void setPassword(const string);
    void setHost(const string);
private:
    void setNombreBaseDatos(const string);
    void setPuerto(const string);
    void setEstado(const Estado);
    
/*-----------------------------------------------------------------------*
 *                                                                       *
 *                          Class's members                              *
 *                                                                       *
 *-----------------------------------------------------------------------*/
    pqxx::result resultados; // resultset that gives a query
    std::vector<string> pilaErrores; // all the mistakes from de data base
    string url; // URL for postgres (see default values below)
    string host; // Server IP addres or its name (default localhost)
    string nombreBaseDatos; // Data base name (default postgres)
    string puerto; // listening port (default 5432)
    string usuario; // username (default postgres)
    string password; // password (empty by default)
    Estado estado; // Server connection status
};
}
#endif //*BASEDATOS_H
