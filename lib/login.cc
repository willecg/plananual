/*
 * liblogin: login.cc
 * Perform a graphical and standar login on a application.
 * Only for use on Gtk
 *
 * Copyright (C) 2014 William Ernesto Cárdenas Gomez
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifdef HAVE_CONFIG_H
#include <config.h> 
#endif

#include <iostream>
#include <glibmm/i18n.h>
#include <gtkmm/messagedialog.h>

#include "login.h"

/**Define the ui_file created by glade.*/
#define ui_file PACKAGE_DATA_DIR"/ui/login.glade"
//#define ui_file "gui/login.glade" // Only for testing from the build dir

/**\brief Login operations.
 *
 * Have all the stuff to do a simple login Gtk operation.*/
namespace login{

/**\brief initiate the window and prints on screen.
 *
 * Using this constructor initiates the class Login and prepare to work.
 * @param "int argc" the argc argument from main.
 * @param "char *argv[]" The argv argument from the main.*/
Login::Login(
        bool (*fun_login)
        (const std::string *const usr,
         const std::string *const pass,
         std::string *errores
         ),
        int argc, char **argv):
    window(0),
    b_ok(0),
    b_cancel(0),
    e_user(0),
    e_password(0),
    funcion_login(fun_login)
{
    ap = Gtk::Application::create(
            argc,
            argv,
            "wecg.lib.login"); // create the app

    /* gets the file  *
     * create builder */
    try {
        builder = Gtk::Builder::create_from_file(ui_file);
    }
    catch(Glib::Error &file_exeption) {
        std::cerr << _("File not found\n") <<
            file_exeption.what() << std::endl;
    }

    /* Get the widgets to the class members */
    builder->get_widget("login_window", window);
    builder->get_widget("b_cancel", b_cancel);
    builder->get_widget("b_ok", b_ok);
    builder->get_widget("e_user", e_user);
    builder->get_widget("e_password", e_password);

    /*Now is time for the events. Buttons b_cancel and b_ok*/
    b_cancel->signal_clicked().connect(
            sigc::mem_fun(*this, &Login::on_cancel_clicked));
    b_ok->signal_clicked().connect(
            sigc::mem_fun(*this, &Login::on_ok_clicked));
}

/**\brief Open a login window
 *
 * Prints a login window on the screen.
 * @return 0 if no problem. Others if any problem.*/
int Login::open_window()
{
    return ap->run(*window);
}

/**\brief Hide the login window.
 *
 * Close the window and terminate the application.*/
void Login::close_window()
{
    window->hide();
}

/* Events */
/**\brief Close the window.
 *
 * Cancel the operation of the Login window and close it.*/
void Login::on_cancel_clicked()
{
    close_window();
}

/**\brief Make a login operation.
 *
 * Make the login operation if success close the window, else show a mistake
 * message.*/
void Login::on_ok_clicked()
{
    const std::string u = get_user(), p = get_password();
    if (funcion_login(&u, &p, &errores)) {
        errores = "--EXITO--";
        close_window();
    }
    else {
        Gtk::MessageDialog *d = new Gtk::MessageDialog(
                _("Database mistake"), false, Gtk::MESSAGE_ERROR, Gtk::BUTTONS_OK);
        d->set_secondary_text(get_errores());
        d->run();
        delete d;
    }
}

/**\brief Get mistakes from the login process.
 *
 * If there some mistake this function can be used to get them. But if the
 * process was successfull this only get the null string ("").
 *
 * @return The error of the process. */
std::string Login::get_errores()
{
    return errores;
}

/**\brief Returns the user storaged on the entry for the user.
 *
 * @return The user in the entry user. */
std::string Login::get_user()
{
    set_user();
    return user;
}

/**\brief Returns the password.
 *
 * Return the value setted on the password atribute.
 *
 * @return The password storaged.*/
std::string Login::get_password()
{
    set_password();
    return password;
}

/**\brief Sets the user atribute.
 *
 * Gets the information in the entry e_user, make it to lower case and store in
 * the user atribute. */
void Login::set_user()
{
    user = e_user->get_text().lowercase().raw();
}

/**\brief Sets the password atribute.
 *
 * Gets the information in the entry e_password, and store in the user
 * atribute. */
void Login::set_password()
{
    password = e_password->get_text().raw();
}
}
