-- estructura.sql: Creación de la base de datos
-- Para crear la estructura de la base de datos se ejecuta el siguiente
-- comando:
--  
--   $ psql -h <host> -d <basedatos> -U <usuario> < <ruta>/estructura.sql
--
-- Con el usuario administrativo de la base de datos o con el usuario
-- postgres. NOTA: La base de datos debe de haber sido creada previamente
-- con el comando: $ createdb <basedatos>.
--
--   Copyright (C) 2014 William Ernesto Cárdenas Gómez
--                                          <sistemas.will@alinzacpa.com.mx>
-- 
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.


------------------------------------------------------------------------------
-- Elementos del esquema público                                            --
------------------------------------------------------------------------------

-- Tablas --------------------------------------------------------------------

-- Tabla sucursales
CREATE TABLE public.sucursales(
    -- Campos
    id_sucursal INTEGER NOT NULL,
    sucursal CHARACTER VARYING(50) NOT NULL,

    -- Restricciones
    CONSTRAINT sucursales_pkey PRIMARY KEY(id_sucursal),
    CONSTRAINT sucursal_unique UNIQUE(sucursal)
);

-- Tabla usuarios
CREATE TABLE public.usuarios(
    -- Campos
    id_usuario INTEGER NOT NULL,
    usuario CHARACTER VARYING(50) NOT NULL,
    sucursal INTEGER NOT NULL,

    -- Restricciones
    CONSTRAINT usuarios_pkey PRIMARY KEY(id_usuario),
    CONSTRAINT usuario_sucursal_fkey FOREIGN KEY(sucursal)
        REFERENCES public.sucursales(id_sucursal),
    CONSTRAINT usuario_unique UNIQUE(usuario)
);

-- Secuencias ----------------------------------------------------------------
CREATE SEQUENCE public.sec_sucursales
INCREMENT BY 1
MINVALUE 1 NO MAXVALUE
START WITH 1
OWNED BY public.sucursales.id_sucursal;

CREATE SEQUENCE public.sec_usuarios
INCREMENT BY 1
MINVALUE 1 NO MAXVALUE
START WITH 1
OWNED BY public.usuarios.id_usuario;


------------------------------------------------------------------------------
-- Esquema metas                                                            --
------------------------------------------------------------------------------
-- DROP SCHEMA metas CASCADE;
CREATE SCHEMA metas;

-- Tabla díasCaptura inicializado con un valor 5
CREATE TABLE metas.diascaptura(dias INTEGER NOT NULL);
INSERT INTO metas.diascaptura(dias) VALUES(5);

-- Tabla Socios
CREATE TABLE metas.socios(
    -- Campos de la clave primaria
    anyo INTEGER NOT NULL,
    mes INTEGER NOT NULL,
    sucursal INTEGER NOT NULL, -- clave foranea

    -- Campos de informacion
    ingreso INTEGER,
    retiro INTEGER,
    presupuesto INTEGER NOT NULL,

    -- Restricciones
    CONSTRAINT socios_pkey PRIMARY KEY (anyo, mes, sucursal),
    CONSTRAINT socio_sucursal_fkey FOREIGN KEY (sucursal)
        REFERENCES public.sucursales(id_sucursal)
);

-- Tabla Menores
CREATE TABLE metas.menores(
    -- Campos de la clave primaria
    anyo INTEGER NOT NULL,
    mes INTEGER NOT NULL,
    sucursal INTEGER NOT NULL, -- clave foranea

    -- Campos de informacion
    ingreso INTEGER,
    retiro INTEGER,
    presupuesto INTEGER NOT NULL,

    -- Restricciones
    CONSTRAINT menores_pkey PRIMARY KEY (anyo, mes, sucursal),
    CONSTRAINT menor_sucursal_fkey FOREIGN KEY (sucursal)
        REFERENCES public.sucursales(id_sucursal)
);

-- Tabla Ahorro de Socios
CREATE TABLE metas.ahorrosocios(
    -- Campos de la clave primaria
    anyo INTEGER NOT NULL,
    mes INTEGER NOT NULL,
    sucursal INTEGER NOT NULL, -- clave foranea

    -- Campos de informacion
    realizado REAL,
    presupuesto REAL NOT NULL,

    -- Restricciones
    CONSTRAINT ahorrosocios_pkey PRIMARY KEY (anyo, mes, sucursal),
    CONSTRAINT ahorrosocio_sucursal_fkey FOREIGN KEY (sucursal)
        REFERENCES public.sucursales(id_sucursal)    
);

-- Tabla Ahorro de Menores
CREATE TABLE metas.ahorromenores(
    -- Campos de la clave primaria
    anyo INTEGER NOT NULL,
    mes INTEGER NOT NULL,
    sucursal INTEGER NOT NULL, -- clave foranea

    -- Campos de informacion
    realizado REAL,
    presupuesto REAL NOT NULL,

    -- Restricciones
    CONSTRAINT ahorromenores_pkey PRIMARY KEY (anyo, mes, sucursal),
    CONSTRAINT ahorromenor_sucursal_fkey FOREIGN KEY (sucursal)
        REFERENCES public.sucursales(id_sucursal)    
);

-- Tabla Depositos a la Vista
CREATE TABLE metas.depositoalavista(
    -- Campos de la clave primaria
    anyo INTEGER NOT NULL,
    mes INTEGER NOT NULL,
    sucursal INTEGER NOT NULL, -- clave foranea

    -- Campos de informacion
    realizado REAL,
    presupuesto REAL NOT NULL,

    -- Restricciones
    CONSTRAINT depositoalavista_pkey PRIMARY KEY (anyo, mes, sucursal),
    CONSTRAINT depositoalavista_sucursal_fkey FOREIGN KEY (sucursal)
        REFERENCES public.sucursales(id_sucursal)    
);

-- Tabla Plazo Fijo
CREATE TABLE metas.plazofijo(
    -- Campos de la clave primaria
    anyo INTEGER NOT NULL,
    mes INTEGER NOT NULL,
    sucursal INTEGER NOT NULL, -- clave foranea

    -- Campos de informacion
    realizado REAL,
    presupuesto REAL NOT NULL,

    -- Restricciones
    CONSTRAINT plazofijo_pkey PRIMARY KEY (anyo, mes, sucursal),
    CONSTRAINT plazofijo_sucursal_fkey FOREIGN KEY (sucursal)
        REFERENCES public.sucursales(id_sucursal)    
);

-- Tabla Prestamo
CREATE TABLE metas.prestamo(
    -- Campos de la clave primaria
    anyo INTEGER NOT NULL,
    mes INTEGER NOT NULL,
    sucursal INTEGER NOT NULL, -- clave foranea

    -- Campos de informacion
    realizado REAL,
    presupuesto REAL NOT NULL,

    -- Restricciones
    CONSTRAINT prestamo_pkey PRIMARY KEY (anyo, mes, sucursal),
    CONSTRAINT prestamo_sucursal_fkey FOREIGN KEY (sucursal)
        REFERENCES public.sucursales(id_sucursal)    
);

-- Tabla Morosidad
CREATE TABLE metas.morosidad(
    -- Campos de la clave primaria
    anyo INTEGER NOT NULL,
    mes INTEGER NOT NULL,
    sucursal INTEGER NOT NULL, -- clave foranea

    -- Campos de informacion
    realizado REAL,
    presupuesto REAL NOT NULL,

    -- Restricciones
    CONSTRAINT morosidad_pkey PRIMARY KEY (anyo, mes, sucursal),
    CONSTRAINT morosidad_sucursal_fkey FOREIGN KEY (sucursal)
        REFERENCES public.sucursales(id_sucursal)    
);


------------------------------------------------------------------------------
-- Roles y usuarios                                                         --
------------------------------------------------------------------------------

-- Rol administrativo.
-- DROP ROLE administrador_plan;
CREATE ROLE administrador_plan WITH
NOSUPERUSER NOCREATEDB CREATEROLE LOGIN CONNECTION LIMIT 1
ENCRYPTED PASSWORD 'plan';

-- Usuarios simples
-- DROP ROLE usuarios_plan;
CREATE ROLE usuarios_plan WITH
NOSUPERUSER NOCREATEDB NOCREATEROLE CONNECTION LIMIT 1;

------------------------------------------------------------------------------
-- Permisos sobre tablas y secuencias                                       --
------------------------------------------------------------------------------

-- Esquema público
  -- Tablas
GRANT SELECT ON TABLE public.usuarios TO usuarios_plan WITH GRANT OPTION;
GRANT SELECT ON TABLE public.sucursales TO usuarios_plan WITH GRANT OPTION;

GRANT SELECT ON TABLE public.usuarios TO administrador_plan WITH GRANT OPTION;
GRANT INSERT ON TABLE public.usuarios TO administrador_plan WITH GRANT OPTION;
GRANT UPDATE ON TABLE public.usuarios TO administrador_plan WITH GRANT OPTION;
GRANT DELETE ON TABLE public.usuarios TO administrador_plan WITH GRANT OPTION;

GRANT SELECT ON TABLE public.sucursales TO administrador_plan
    WITH GRANT OPTION;
GRANT INSERT ON TABLE public.sucursales TO administrador_plan
    WITH GRANT OPTION;
GRANT UPDATE ON TABLE public.sucursales TO administrador_plan
    WITH GRANT OPTION;
GRANT DELETE ON TABLE public.sucursales TO administrador_plan
    WITH GRANT OPTION;

  -- Secuencias
GRANT USAGE ON SEQUENCE public.sec_usuarios TO administrador_plan
    WITH GRANT OPTION;
GRANT USAGE ON SEQUENCE public.sec_sucursales TO administrador_plan
    WITH GRANT OPTION;

-- Esquema metas
GRANT USAGE ON SCHEMA metas TO usuarios_plan WITH GRANT OPTION;
GRANT USAGE ON SCHEMA metas TO administrador_plan WITH GRANT OPTION;

-- Tablas
  -- usuarios_plan
GRANT SELECT ON TABLE metas.socios TO usuarios_plan WITH GRANT OPTION;
GRANT UPDATE ON TABLE metas.socios TO usuarios_plan WITH GRANT OPTION;

GRANT SELECT ON TABLE metas.menores TO usuarios_plan WITH GRANT OPTION;
GRANT UPDATE ON TABLE metas.menores TO usuarios_plan WITH GRANT OPTION;

GRANT SELECT ON TABLE metas.ahorrosocios TO usuarios_plan WITH GRANT OPTION;
GRANT UPDATE ON TABLE metas.ahorrosocios TO usuarios_plan WITH GRANT OPTION;

GRANT SELECT ON TABLE metas.ahorromenores TO usuarios_plan WITH GRANT OPTION;
GRANT UPDATE ON TABLE metas.ahorromenores TO usuarios_plan WITH GRANT OPTION;

GRANT SELECT ON TABLE metas.depositoalavista TO usuarios_plan
    WITH GRANT OPTION;
GRANT UPDATE ON TABLE metas.depositoalavista TO usuarios_plan
    WITH GRANT OPTION;

GRANT SELECT ON TABLE metas.plazofijo TO usuarios_plan WITH GRANT OPTION;
GRANT UPDATE ON TABLE metas.plazofijo TO usuarios_plan WITH GRANT OPTION;

GRANT SELECT ON TABLE metas.prestamo TO usuarios_plan WITH GRANT OPTION;
GRANT UPDATE ON TABLE metas.prestamo TO usuarios_plan WITH GRANT OPTION;

GRANT SELECT ON TABLE metas.morosidad TO usuarios_plan WITH GRANT OPTION;
GRANT UPDATE ON TABLE metas.morosidad TO usuarios_plan WITH GRANT OPTION;

GRANT SELECT ON TABLE metas.diascaptura TO usuarios_plan WITH GRANT OPTION;

  -- Administrador Plan
GRANT SELECT ON TABLE metas.socios TO administrador_plan WITH GRANT OPTION;
GRANT UPDATE ON TABLE metas.socios TO administrador_plan WITH GRANT OPTION;
GRANT INSERT ON TABLE metas.socios TO administrador_plan WITH GRANT OPTION;
GRANT DELETE ON TABLE metas.socios TO administrador_plan WITH GRANT OPTION;

GRANT SELECT ON TABLE metas.menores TO administrador_plan WITH GRANT OPTION;
GRANT UPDATE ON TABLE metas.menores TO administrador_plan WITH GRANT OPTION;
GRANT INSERT ON TABLE metas.menores TO administrador_plan WITH GRANT OPTION;
GRANT DELETE ON TABLE metas.menores TO administrador_plan WITH GRANT OPTION;

GRANT SELECT ON TABLE metas.ahorrosocios TO administrador_plan 
    WITH GRANT OPTION;
GRANT UPDATE ON TABLE metas.ahorrosocios TO administrador_plan 
    WITH GRANT OPTION;
GRANT INSERT ON TABLE metas.ahorrosocios TO administrador_plan 
    WITH GRANT OPTION;
GRANT DELETE ON TABLE metas.ahorrosocios TO administrador_plan 
    WITH GRANT OPTION;

GRANT SELECT ON TABLE metas.ahorromenores TO administrador_plan 
    WITH GRANT OPTION;
GRANT UPDATE ON TABLE metas.ahorromenores TO administrador_plan 
    WITH GRANT OPTION;
GRANT INSERT ON TABLE metas.ahorromenores TO administrador_plan
    WITH GRANT OPTION;
GRANT DELETE ON TABLE metas.ahorromenores TO administrador_plan
    WITH GRANT OPTION;

GRANT SELECT ON TABLE metas.depositoalavista TO administrador_plan
    WITH GRANT OPTION;
GRANT UPDATE ON TABLE metas.depositoalavista TO administrador_plan
    WITH GRANT OPTION;
GRANT INSERT ON TABLE metas.depositoalavista TO administrador_plan
    WITH GRANT OPTION;
GRANT DELETE ON TABLE metas.depositoalavista TO administrador_plan
    WITH GRANT OPTION;

GRANT SELECT ON TABLE metas.plazofijo TO administrador_plan WITH GRANT OPTION;
GRANT UPDATE ON TABLE metas.plazofijo TO administrador_plan WITH GRANT OPTION;
GRANT INSERT ON TABLE metas.plazofijo TO administrador_plan WITH GRANT OPTION;
GRANT DELETE ON TABLE metas.plazofijo TO administrador_plan WITH GRANT OPTION;

GRANT SELECT ON TABLE metas.prestamo TO administrador_plan WITH GRANT OPTION;
GRANT UPDATE ON TABLE metas.prestamo TO administrador_plan WITH GRANT OPTION;
GRANT INSERT ON TABLE metas.prestamo TO administrador_plan WITH GRANT OPTION;
GRANT DELETE ON TABLE metas.prestamo TO administrador_plan WITH GRANT OPTION;

GRANT SELECT ON TABLE metas.morosidad TO administrador_plan WITH GRANT OPTION;
GRANT UPDATE ON TABLE metas.morosidad TO administrador_plan WITH GRANT OPTION;
GRANT INSERT ON TABLE metas.morosidad TO administrador_plan WITH GRANT OPTION;
GRANT DELETE ON TABLE metas.morosidad TO administrador_plan WITH GRANT OPTION;

GRANT SELECT ON TABLE metas.diascaptura TO administrador_plan
    WITH GRANT OPTION;
GRANT UPDATE ON TABLE metas.diascaptura TO administrador_plan
    WITH GRANT OPTION;

------------------------------------------------------------------------------
-- Triggers                                                                 --
------------------------------------------------------------------------------
-- Funciones de triggers

-- INSERT
CREATE OR REPLACE FUNCTION metas.insertsocios()
RETURNS TRIGGER AS $insertsocios$
DECLARE
    existe INTEGER;
    fecha TIMESTAMP;
    anyo_actual INTEGER;
BEGIN
    IF NEW.mes <= 12 AND NEW.mes >= 1 THEN -- Mes valido de 1 a 12. 1 es Enero.
        SELECT CURRENT_DATE INTO fecha;
        SELECT EXTRACT(YEAR FROM fecha) INTO anyo_actual;
        IF NEW.anyo <= anyo_actual THEN
            RAISE EXCEPTION 'El año debe ser posterior al año actual';
        END IF;
    ELSE
        RAISE EXCEPTION 'Mes no válido'
            USING HINT='Solo de 1 a 12';
    END IF;
    RETURN NEW;
END;
$insertsocios$ LANGUAGE plpgsql;


-- UPDATE
CREATE OR REPLACE FUNCTION metas.updatesocios()
RETURNS TRIGGER AS $updatesocios$
DECLARE
    existe INTEGER;
    fecha TIMESTAMP;
    dia_actual INTEGER;
    usuario VARCHAR(50);
    dias_habiles INTEGER;
BEGIN
    IF NEW.mes <= 12 AND NEW.mes >= 1 THEN -- Mes valido
        SELECT CURRENT_USER INTO usuario;
        IF usuario <> 'administrador_plan' THEN -- usuario
            SELECT CURRENT_DATE INTO fecha;
            SELECT dias 
                INTO dias_habiles
                FROM metas.diascaptura;
            SELECT EXTRACT(DAY FROM fecha) INTO dia_actual;

            IF dia_actual > dias_habiles THEN --dia valido
                RAISE EXCEPTION 'Fecha limite rebasada';
            END IF; --dia valido
        END IF; --usuario
    ELSE
        RAISE EXCEPTION 'Mes no válido'
            USING HINT='Solo de 1 a 12';
    END IF; --Mes válido
    RETURN NEW;
END;
$updatesocios$ LANGUAGE plpgsql;


-- Creación de los disparadores
CREATE TRIGGER insertsocios BEFORE INSERT ON metas.socios
    FOR EACH ROW EXECUTE PROCEDURE metas.insertsocios();
CREATE TRIGGER updatesocios BEFORE UPDATE ON metas.socios
    FOR EACH ROW EXECUTE PROCEDURE metas.updatesocios();

CREATE TRIGGER insertsocios BEFORE INSERT ON metas.menores
    FOR EACH ROW EXECUTE PROCEDURE metas.insertsocios();
CREATE TRIGGER updatesocios BEFORE UPDATE ON metas.menores
    FOR EACH ROW EXECUTE PROCEDURE metas.updatesocios();

CREATE TRIGGER insertsocios BEFORE INSERT ON metas.ahorromenores
    FOR EACH ROW EXECUTE PROCEDURE metas.insertsocios();
CREATE TRIGGER updatesocios BEFORE UPDATE ON metas.ahorromenores
    FOR EACH ROW EXECUTE PROCEDURE metas.updatesocios();

CREATE TRIGGER insertsocios BEFORE INSERT ON metas.ahorrosocios
    FOR EACH ROW EXECUTE PROCEDURE metas.insertsocios();
CREATE TRIGGER updatesocios BEFORE UPDATE ON metas.ahorrosocios
    FOR EACH ROW EXECUTE PROCEDURE metas.updatesocios();

CREATE TRIGGER insertsocios BEFORE INSERT ON metas.prestamo
    FOR EACH ROW EXECUTE PROCEDURE metas.insertsocios();
CREATE TRIGGER updatesocios BEFORE UPDATE ON metas.prestamo
    FOR EACH ROW EXECUTE PROCEDURE metas.updatesocios();

CREATE TRIGGER insertsocios BEFORE INSERT ON metas.plazofijo
    FOR EACH ROW EXECUTE PROCEDURE metas.insertsocios();
CREATE TRIGGER updatesocios BEFORE UPDATE ON metas.plazofijo
    FOR EACH ROW EXECUTE PROCEDURE metas.updatesocios();

CREATE TRIGGER insertsocios BEFORE INSERT ON metas.depositoalavista
    FOR EACH ROW EXECUTE PROCEDURE metas.insertsocios();
CREATE TRIGGER updatesocios BEFORE UPDATE ON metas.depositoalavista
    FOR EACH ROW EXECUTE PROCEDURE metas.updatesocios();

CREATE TRIGGER insertsocios BEFORE INSERT ON metas.morosidad
    FOR EACH ROW EXECUTE PROCEDURE metas.insertsocios();
CREATE TRIGGER updatesocios BEFORE UPDATE ON metas.morosidad
    FOR EACH ROW EXECUTE PROCEDURE metas.updatesocios();

------------------------------------------------------------------------------
-- Funciones                                                                --
------------------------------------------------------------------------------

-- Crear, borrar y actualizar sucursales
-- Insertar nuevas sucursales
CREATE OR REPLACE FUNCTION nuevaSucursal( nombre CHARACTER VARYING (30) )
RETURNS VOID AS $$
DECLARE
    repetido INTEGER;
BEGIN
    nombre = LOWER( nombre );
    SELECT count(*)
    INTO repetido
    FROM public.sucursales
    WHERE nombre = sucursal;

    IF ( repetido = 0 )
    THEN
        INSERT INTO public.sucursales( id_sucursal, sucursal )
	VALUES ( nextval( 'sec_sucursales' ), nombre );
	RAISE NOTICE 'La sucursal % fue dada de alta exitosamente.', nombre;
    ELSE
	RAISE EXCEPTION 'La sucursal % ya existe.', nombre;
    END IF;
END;
$$ LANGUAGE plpgsql;

--Modificar una sucursal ya existente
CREATE OR REPLACE FUNCTION cambiarSucursal(
    nombre CHARACTER VARYING(50), nuevoNombre CHARACTER VARYING(50))
RETURNS VOID AS $$
DECLARE
    repetido INTEGER;
    existente INTEGER;
BEGIN
    nombre = LOWER( nombre );
    nuevoNombre = LOWER( nuevoNombre );

    SELECT count(*)
    INTO repetido
    FROM public.sucursales
    WHERE sucursal = nuevoNombre;

    SELECT count(*)
    INTO existente
    FROM public.sucursales
    WHERE sucursal = nombre;

    IF ( repetido = 0 )
    THEN
        IF(existente <> 0) THEN
            UPDATE public.sucursales 
            SET sucursal = nuevoNombre
            WHERE sucursal = nombre;
	
            RAISE NOTICE 'La sucursal % fue modificada exitosamente',
                nuevoNombre;
        ELSE
            RAISE EXCEPTION 'No existe la sucursal %', nombre;
        END IF;
    ELSE
    	RAISE EXCEPTION 'La sucursal % ya existe', nuevoNombre;
    END IF;
END;
$$ LANGUAGE plpgsql;

-- Borrar una sucursal ya existente
CREATE OR REPLACE FUNCTION borrarSucursal(
    nombre CHARACTER VARYING(50))
RETURNS VOID AS $$
DECLARE
    existente INTEGER;
    enlazado INTEGER;
BEGIN
    nombre = LOWER( nombre );

    SELECT count(*)
    INTO existente
    FROM public.sucursales
    WHERE sucursal = nombre;

    IF ( existente > 0 )
    THEN
        SELECT count(*)
    	INTO enlazado
        FROM public.sucursales s
        INNER JOIN public.usuarios u
        ON u.sucursal = s.id_sucursal
        INNER JOIN metas.socios ms
        ON ms.sucursal = s.id_sucursal
        INNER JOIN metas.menores m
        ON m.sucursal = s.id_sucursal
        INNER JOIN metas.ahorrosocios aso
        ON aso.sucursal = s.id_sucursal
        INNER JOIN metas.ahorromenores am
        ON am.sucursal = s.id_sucursal
        INNER JOIN metas.depositoalavista d
        ON d.sucursal = s.id_sucursal
        INNER JOIN metas.plazofijo pf
        ON pf.sucursal = s.id_sucursal
        INNER JOIN metas.prestamo p
        ON p.sucursal = s.id_sucursal
        INNER JOIN metas.morosidad pm
        ON pm.sucursal = s.id_sucursal
        WHERE s.sucursal = nombre;

	IF enlazado = 0 THEN
            DELETE FROM public.sucursales
            WHERE sucursal = nombre;
            RAISE NOTICE 'La sucursal % fue eliminada exitosamente', nombre;
        ELSE
            RAISE EXCEPTION 
                'La sucursal % tiene elementos que dependen de ella', nombre;
        END IF;
    ELSE
    	RAISE EXCEPTION 'La sucursal % no existe', nombre;
    END IF;
END;
$$ LANGUAGE plpgsql;

-- Crear, borrar y actualizar usuarios ---------------------------------------
-- Nuevo usuario
CREATE OR REPLACE FUNCTION public.crearUsuario(
        nombreDelUsuario CHARACTER VARYING(50),
        password CHARACTER VARYING(10),
        suc CHARACTER VARYING(50))

RETURNS VOID AS $$
DECLARE
    repetido INTEGER;
    valor    INTEGER;
    id_suc   INTEGER; 
BEGIN
    -- Convertir todo a minusculas (menos la contraseña por seguridad)
    nombreDelUsuario = LOWER( nombreDelUsuario );
    suc              = LOWER( suc );

    -- Revisamos si la sucursal existe de verdad y
    -- tomamos el índice de la sucursal indicada.
    SELECT id_sucursal 
        INTO id_suc
        FROM public.sucursales
        WHERE sucursal = suc;

    IF id_suc > 0 THEN-- y aqui se comprueba
        -- ahora checamos que el usuario sea verdaderamente nuevo
	SELECT count(*)
            INTO repetido
            FROM public.usuarios
            WHERE usuario = nombreDelUsuario;

	IF repetido = 0	THEN
    	    -- Una vez cumplidas las comprobaciones creamos el usuario
	    EXECUTE 'CREATE USER '
	    || nombreDelUsuario
	    || ' WITH ENCRYPTED PASSWORD '
	    || quote_literal( password )
	    || ' IN GROUP usuarios_plan';
		
	    valor = nextval( 'public.sec_usuarios' );
	    -- Por último agregamos el registro a la tabla de usuarios
	    INSERT INTO public.usuarios ( id_usuario, usuario, sucursal )
	        VALUES ( valor, nombreDelUsuario, id_suc );
	    RAISE NOTICE 'El usuario % se ha dado de alta en la sucursal %',
                nombreDelUsuario, suc;
    	    -- El resto a partir de aqui solo son mensajes de error
	ELSE
	    RAISE EXCEPTION 'Error al crear el usuario' 
                USING HINT = 'El usuario ya existe';
	END IF; -- if...else anidado    
    ELSE
        RAISE EXCEPTION 'Error al crear el usuario'
            USING HINT = 'Revisa bién la sucursal';
    END IF; -- if...else principal
    -- finamrium 
    -- (o sea fin en latín)
END;
$$ LANGUAGE plpgsql;

-- Modificar sucursal de un usuario
CREATE OR REPLACE FUNCTION public.cambiarDeSucursal(
    nombre CHARACTER VARYING(50), nuevasuc CHARACTER VARYING(50))
RETURNS VOID AS $$
DECLARE
    suc_id INTEGER;
    us_id INTEGER;
BEGIN
    nombre := LOWER( nombre );
    nuevasuc := LOWER( nuevasuc );
    
    -- Buscamos que la sucursal exista
    SELECT id_sucursal
        INTO suc_id
        FROM public.sucursales
        WHERE sucursal = nuevasuc;

    IF suc_id > 0
    THEN
        -- Buscamos que el usuario exista
        SELECT count(*)
            INTO us_id
            FROM public.usuarios
            WHERE usuario = nombre;

        IF us_id > 0 THEN
             -- Cambiar los datos.
            UPDATE public.usuarios 
	        SET sucursal = suc_id
	        WHERE usuario = nombre;
	    RAISE NOTICE 'Usuario: %, Sucursal: %', nombre, nuevasuc;
        ELSE
            RAISE EXCEPTION 'No existe ese usuario';
        END IF;
				
    ELSE
        RAISE EXCEPTION 'No existe esa sucursal'
            USING HINT := 'Prueba otra sucursal';
    END IF;

    -- Muy simple
    -- en realidad toda la complejidad vendrá en el trigger
END;
$$ LANGUAGE plpgsql;

-- Cambiar la contraseña de un usuario
CREATE OR REPLACE FUNCTION public.cambiarPassword(
    usuario CHARACTER VARYING(50),
    nuevoPassword CHARACTER VARYING(10))
RETURNS VOID AS $$
BEGIN
    
    EXECUTE 'ALTER ROLE '
    || usuario
    || ' WITH ENCRYPTED PASSWORD '
    || quote_literal( nuevoPassword );

END;
$$ LANGUAGE plpgsql;

-- Cambiar nombre y por obligación contraseña del usuario
--  Esto se debe a que se usa una encriptación en la que
--  participa el nombre de usuario.
CREATE OR REPLACE FUNCTION public.modificarNombreUsuario(
    nombre CHARACTER VARYING(50),
    nuevoNombre CHARACTER VARYING(50),
    nuevoPassword CHARACTER VARYING(10))

RETURNS VOID AS $$
DECLARE
    usr_id INTEGER;
BEGIN
    nombre      := LOWER( nombre );
    nuevoNombre := LOWER( nuevoNombre );

    -- Buscamos que el usuario exista
    SELECT count(*)
        INTO usr_id
        FROM public.usuarios
        WHERE usuario = nombre;

    IF usr_id = 1
    THEN
        EXECUTE 'ALTER USER '
        || nombre
        || ' RENAME TO '
        || nuevoNombre;
	    
        EXECUTE 'ALTER USER '
        || nuevoNombre
        || ' WITH ENCRYPTED PASSWORD '
        || quote_literal(nuevoPassword);	   
        
        -- Cambiar los datos.
	UPDATE public.usuarios 
    	    SET usuario = nuevoNombre 
	    WHERE usuario = nombre;
				
	RAISE NOTICE 'Usuario: %, Ahora de llama: %', nombre, nuevoNombre;
    ELSE
        RAISE EXCEPTION 'No existe ese usuario.'
            USING HINT := 'Prueba otra vez.';
    END IF;
END;
$$ LANGUAGE plpgsql;

-- Borrar un usuario de la base de datos
CREATE OR REPLACE FUNCTION public.borrarusuario(nombre CHARACTER VARYING(50))
RETURNS VOID AS $$
DECLARE
    usr_id INTEGER;
BEGIN
    nombre := LOWER( nombre );

    -- Buscamos que el usuario exista
    SELECT count(*)
        INTO usr_id
        FROM public.usuarios
        WHERE usuario = nombre;

    IF usr_id = 1
    THEN
        EXECUTE 'DROP USER '
        || nombre;
        
        -- Cambiar los datos.
	DELETE FROM public.usuarios 
	    WHERE usuario = nombre;
				
	RAISE NOTICE 'Usuario: "%" eliminado', nombre;
    ELSE
        RAISE EXCEPTION 'No existe ese usuario.'
            USING HINT := 'Prueba otra vez.';
    END IF;
END;
$$ LANGUAGE plpgsql;

-- Metas.Socios y menores -----------------------------------------------------
-- Agregar un registro nuevo cada año.
-- si tabla es cero agrega el registro en metas.socios de lo contrario lo
-- agrega en la tabla metas.menore
CREATE OR REPLACE FUNCTION metas.agregarregistro(
    arg_sucursal VARCHAR(50),
    arg_presupuesto INTEGER,
    arg_anyo INTEGER,
    arg_mes INTEGER,
    tabla INTEGER)

RETURNS VOID AS $$
DECLARE
    existe INTEGER;
    suc_id INTEGER;
BEGIN
    suc_id := 0;
    arg_sucursal := LOWER(arg_sucursal);
    
    -- Se revisa que la sucursal exista
    SELECT id_sucursal
        INTO suc_id
        FROM public.sucursales
        WHERE sucursal = arg_sucursal;

    IF suc_id <> 0 THEN
        IF tabla = 0 THEN
            INSERT 
                INTO metas.socios(sucursal, anyo, mes, presupuesto)
                VALUES (suc_id, arg_anyo, arg_mes, arg_presupuesto);
        ELSE
            INSERT 
                INTO metas.menores(sucursal, anyo, mes, presupuesto)
                VALUES (suc_id, arg_anyo, arg_mes, arg_presupuesto);
        
        RAISE NOTICE 'Captura exitosa';
        END IF;
    ELSE
        RAISE EXCEPTION 'No existe la sucursal %', arg_sucursal;
    END IF; 
END;
$$ LANGUAGE plpgsql;

-- Metas. Todas las demas -----------------------------------------------------
-- Agregar un registro nuevo cada año.
-- si tabla =
--  0: metas.ahorrosocios
--  1: metas.ahorromenores
--  2: metas.depositoalavista
--  3: metas.plazofijo
--  4: metas.prestamo
--  5: metas.morosidad
CREATE OR REPLACE FUNCTION metas.agregarregistro2(
    arg_sucursal VARCHAR(50),
    arg_presupuesto REAL,
    arg_anyo INTEGER,
    arg_mes INTEGER,
    tabla INTEGER)

RETURNS VOID AS $$
DECLARE
    existe INTEGER;
    suc_id INTEGER;
BEGIN
    suc_id := 0;
    arg_sucursal := LOWER(arg_sucursal);
    
    -- Se revisa que la sucursal exista
    SELECT id_sucursal
        INTO suc_id
        FROM public.sucursales
        WHERE sucursal = arg_sucursal;

    IF suc_id <> 0 THEN
        CASE tabla
            WHEN 0 THEN
                INSERT 
                    INTO metas.ahorrosocios(sucursal, anyo, mes, presupuesto)
                    VALUES (suc_id, arg_anyo, arg_mes, arg_presupuesto);
            WHEN 1 THEN
                INSERT 
                    INTO metas.ahorromenores(sucursal, anyo, mes, presupuesto)
                    VALUES (suc_id, arg_anyo, arg_mes, arg_presupuesto);
            WHEN 2 THEN
                INSERT 
                    INTO metas.depositoalavista(sucursal, anyo, mes, presupuesto)
                    VALUES (suc_id, arg_anyo, arg_mes, arg_presupuesto);
            WHEN 3 THEN
                INSERT 
                    INTO metas.plazofijo(sucursal, anyo, mes, presupuesto)
                    VALUES (suc_id, arg_anyo, arg_mes, arg_presupuesto);
            WHEN 4 THEN
                INSERT 
                    INTO metas.prestamo(sucursal, anyo, mes, presupuesto)
                    VALUES (suc_id, arg_anyo, arg_mes, arg_presupuesto);
            WHEN 5 THEN
                INSERT 
                    INTO metas.morosidad(sucursal, anyo, mes, presupuesto)
                    VALUES (suc_id, arg_anyo, arg_mes, arg_presupuesto);
            RAISE NOTICE 'Captura exitosa';
        END CASE;
    ELSE
        RAISE EXCEPTION 'No existe la sucursal %', arg_sucursal;
    END IF; 
END;
$$ LANGUAGE plpgsql;



-- Modificar mes de una tabla de metas
-- indicando la tabla en el último argumento.
-- si tabla =
--  0: metas.socios
--  1: metas.menores
--  2: metas.ahorrosocios
--  3: metas.ahorromenores
--  4: metas.depositoalavista
--  5: metas.plazofijo
--  6: metas.prestamo
--  7: metas.morosidad
CREATE OR REPLACE FUNCTION metas.modificar_mes(
    arg_sucursal VARCHAR(50),
    arg_anyo INTEGER,
    new_mes INTEGER,
    old_mes INTEGER, 
    tabla INTEGER)

RETURNS VOID AS $$
DECLARE
    suc_id INTEGER;
    existe INTEGER;
BEGIN
    suc_id := 0;
    arg_sucursal := LOWER(arg_sucursal);
    
    -- Se revisa que la sucursal exista
    SELECT id_sucursal
        INTO suc_id
        FROM public.sucursales
        WHERE sucursal = arg_sucursal;
    
    IF suc_id <> 0 THEN
        CASE tabla
            WHEN 0 THEN     
                SELECT count(*) 
                    INTO existe
                    FROM metas.socios
                    WHERE 
                        mes = old_mes AND
                        anyo = arg_anyo AND
                        sucursal = suc_id;
                IF existe <> 0 THEN
                    UPDATE metas.socios
                        SET mes = new_mes
                        WHERE 
                            mes = old_mes AND
                            anyo = arg_anyo AND
                            sucursal = suc_id;
                ELSE
                    RAISE EXCEPTION 'No existe el registro'
                        USING HINT='Verifique la información por favor';
                END IF;
            WHEN 1 THEN     
                SELECT count(*) 
                    INTO existe
                    FROM metas.menores
                    WHERE 
                        mes = old_mes AND
                        anyo = arg_anyo AND
                        sucursal = suc_id;
                IF existe <> 0 THEN
                    UPDATE metas.menores
                        SET mes = new_mes
                        WHERE 
                            mes = old_mes AND
                            anyo = arg_anyo AND
                            sucursal = suc_id;
                ELSE
                    RAISE EXCEPTION 'No existe el registro'
                        USING HINT='Verifique la información por favor';
                END IF;
           WHEN 2 THEN     
                SELECT count(*) 
                    INTO existe
                    FROM metas.ahorrosocios
                    WHERE 
                        mes = old_mes AND
                        anyo = arg_anyo AND
                        sucursal = suc_id;
                IF existe <> 0 THEN
                    UPDATE metas.ahorrosocios
                        SET mes = new_mes
                        WHERE 
                            mes = old_mes AND
                            anyo = arg_anyo AND
                            sucursal = suc_id;
                 ELSE
                    RAISE EXCEPTION 'No existe el registro'
                        USING HINT='Verifique la información por favor';
                END IF;
           WHEN 3 THEN     
                SELECT count(*) 
                    INTO existe
                    FROM metas.ahorromenores
                    WHERE 
                        mes = old_mes AND
                        anyo = arg_anyo AND
                        sucursal = suc_id;
                IF existe <> 0 THEN
                    UPDATE metas.ahorromenores
                        SET mes = new_mes
                        WHERE 
                            mes = old_mes AND
                            anyo = arg_anyo AND
                            sucursal = suc_id;
                ELSE
                    RAISE EXCEPTION 'No existe el registro'
                        USING HINT='Verifique la información por favor';
                END IF;
            WHEN 4 THEN     
                SELECT count(*) 
                    INTO existe
                    FROM metas.depositoalavista
                    WHERE 
                        mes = old_mes AND
                        anyo = arg_anyo AND
                        sucursal = suc_id;
                IF existe <> 0 THEN
                    UPDATE metas.depositoalavista
                        SET mes = new_mes
                        WHERE 
                            mes = old_mes AND
                            anyo = arg_anyo AND
                            sucursal = suc_id;
                ELSE
                    RAISE EXCEPTION 'No existe el registro'
                        USING HINT='Verifique la información por favor';
                END IF;
            WHEN 5 THEN     
                SELECT count(*) 
                    INTO existe
                    FROM metas.plazofijo
                    WHERE 
                        mes = old_mes AND
                        anyo = arg_anyo AND
                        sucursal = suc_id;
                IF existe <> 0 THEN
                    UPDATE metas.plazofijo
                        SET mes = new_mes
                        WHERE 
                            mes = old_mes AND
                            anyo = arg_anyo AND
                            sucursal = suc_id;
                ELSE
                    RAISE EXCEPTION 'No existe el registro'
                        USING HINT='Verifique la información por favor';
                END IF;
            WHEN 6 THEN     
                SELECT count(*) 
                    INTO existe
                    FROM metas.prestamo
                    WHERE 
                        mes = old_mes AND
                        anyo = arg_anyo AND
                        sucursal = suc_id;
                IF existe <> 0 THEN
                    UPDATE metas.prestamo
                        SET mes = new_mes
                        WHERE 
                            mes = old_mes AND
                            anyo = arg_anyo AND
                            sucursal = suc_id;
                ELSE
                    RAISE EXCEPTION 'No existe el registro'
                        USING HINT='Verifique la información por favor';
                END IF;
            WHEN 7 THEN     
                SELECT count(*) 
                    INTO existe
                    FROM metas.morosidad
                    WHERE 
                        mes = old_mes AND
                        anyo = arg_anyo AND
                        sucursal = suc_id;
                IF existe <> 0 THEN
                    UPDATE metas.morosidad
                        SET mes = new_mes
                        WHERE 
                            mes = old_mes AND
                            anyo = arg_anyo AND
                            sucursal = suc_id;
                ELSE
                    RAISE EXCEPTION 'No existe el registro'
                        USING HINT='Verifique la información por favor';
                END IF;
        END CASE;
        RAISE NOTICE 'Captura exitosa';
    ELSE
        RAISE EXCEPTION 'No existe la sucursal %', arg_sucursal;
    END IF; 
END;
$$ LANGUAGE plpgsql;

-- Modificar año de una tabla de metas
-- indicando la tabla en el último argumento.
-- si tabla =
--  0: metas.socios
--  1: metas.menores
--  2: metas.ahorrosocios
--  3: metas.ahorromenores
--  4: metas.depositoalavista
--  5: metas.plazofijo
--  6: metas.prestamo
--  7: metas.morosidad
CREATE OR REPLACE FUNCTION metas.modificar_anyo(
    arg_sucursal VARCHAR(50),
    old_anyo INTEGER,
    new_anyo INTEGER,
    arg_mes INTEGER, 
    tabla INTEGER)

RETURNS VOID AS $$
DECLARE
    suc_id INTEGER;
    existe INTEGER;
BEGIN
    suc_id := 0;
    arg_sucursal := LOWER(arg_sucursal);
    
    -- Se revisa que la sucursal exista
    SELECT id_sucursal
        INTO suc_id
        FROM public.sucursales
        WHERE sucursal = arg_sucursal;
    
    IF suc_id <> 0 THEN
        CASE tabla
            WHEN 0 THEN     
                SELECT count(*) 
                    INTO existe
                    FROM metas.socios
                    WHERE 
                        mes = arg_mes AND
                        anyo = old_anyo AND
                        sucursal = suc_id;
                IF existe <> 0 THEN
                    UPDATE metas.socios
                        SET anyo = new_anyo
                        WHERE 
                            mes = arg_mes AND
                            anyo = old_anyo AND
                            sucursal = suc_id;
                ELSE
                    RAISE EXCEPTION 'No existe el registro'
                        USING HINT='Verifique la información por favor';
                END IF;
            WHEN 1 THEN     
                SELECT count(*) 
                    INTO existe
                    FROM metas.menores
                    WHERE 
                        mes = arg_mes AND
                        anyo = old_anyo AND
                        sucursal = suc_id;
                IF existe <> 0 THEN
                    UPDATE metas.menores
                        SET anyo = new_anyo
                        WHERE 
                            mes = arg_mes AND
                            anyo = old_anyo AND
                            sucursal = suc_id;
                ELSE
                    RAISE EXCEPTION 'No existe el registro'
                        USING HINT='Verifique la información por favor';
                END IF;
            WHEN 2 THEN     
                SELECT count(*) 
                    INTO existe
                    FROM metas.ahorrosocios
                    WHERE 
                        mes = arg_mes AND
                        anyo = old_anyo AND
                        sucursal = suc_id;
                IF existe <> 0 THEN
                    UPDATE metas.ahorrosocios
                        SET anyo = new_anyo
                        WHERE 
                            mes = arg_mes AND
                            anyo = old_anyo AND
                            sucursal = suc_id;
                ELSE
                    RAISE EXCEPTION 'No existe el registro'
                        USING HINT='Verifique la información por favor';
                END IF;
            WHEN 3 THEN     
                SELECT count(*) 
                    INTO existe
                    FROM metas.ahorromenores
                    WHERE 
                        mes = arg_mes AND
                        anyo = old_anyo AND
                        sucursal = suc_id;
                IF existe <> 0 THEN
                    UPDATE metas.ahorromenores
                        SET anyo = new_anyo
                        WHERE 
                            mes = arg_mes AND
                            anyo = old_anyo AND
                            sucursal = suc_id;
                ELSE
                    RAISE EXCEPTION 'No existe el registro'
                        USING HINT='Verifique la información por favor';
                END IF;
            WHEN 4 THEN     
                SELECT count(*) 
                    INTO existe
                    FROM metas.depositoalavista
                    WHERE 
                        mes = arg_mes AND
                        anyo = old_anyo AND
                        sucursal = suc_id;
                IF existe <> 0 THEN
                    UPDATE metas.depositoalavista
                        SET anyo = new_anyo
                        WHERE 
                            mes = arg_mes AND
                            anyo = old_anyo AND
                            sucursal = suc_id;
                ELSE
                    RAISE EXCEPTION 'No existe el registro'
                        USING HINT='Verifique la información por favor';
                END IF;
            WHEN 5 THEN     
                SELECT count(*) 
                    INTO existe
                    FROM metas.plazofijo
                    WHERE 
                        mes = arg_mes AND
                        anyo = old_anyo AND
                        sucursal = suc_id;
                IF existe <> 0 THEN
                    UPDATE metas.plazofijo
                        SET anyo = new_anyo
                        WHERE 
                            mes = arg_mes AND
                            anyo = old_anyo AND
                            sucursal = suc_id;
                ELSE
                    RAISE EXCEPTION 'No existe el registro'
                        USING HINT='Verifique la información por favor';
                END IF;
            WHEN 6 THEN     
                SELECT count(*) 
                    INTO existe
                    FROM metas.prestamo
                    WHERE 
                        mes = arg_mes AND
                        anyo = old_anyo AND
                        sucursal = suc_id;
                IF existe <> 0 THEN
                    UPDATE metas.prestamo
                        SET anyo = new_anyo
                        WHERE 
                            mes = arg_mes AND
                            anyo = old_anyo AND
                            sucursal = suc_id;
                ELSE
                    RAISE EXCEPTION 'No existe el registro'
                        USING HINT='Verifique la información por favor';
                END IF;
            WHEN 7 THEN     
                SELECT count(*) 
                    INTO existe
                    FROM metas.morosidad
                    WHERE 
                        mes = arg_mes AND
                        anyo = old_anyo AND
                        sucursal = suc_id;
                IF existe <> 0 THEN
                    UPDATE metas.morosidad
                        SET anyo = new_anyo
                        WHERE 
                            mes = arg_mes AND
                            anyo = old_anyo AND
                            sucursal = suc_id;
                ELSE
                    RAISE EXCEPTION 'No existe el registro'
                        USING HINT='Verifique la información por favor';
                END IF;
        END CASE;
        RAISE NOTICE 'Captura exitosa';
    ELSE
        RAISE EXCEPTION 'No existe la sucursal %', arg_sucursal;
    END IF; 
END;
$$ LANGUAGE plpgsql;


-- Modificar una sucursal de una tabla de metas
-- indicando la tabla en el último argumento.
-- si tabla =
--  0: metas.socios
--  1: metas.menores
--  2: metas.ahorrosocios
--  3: metas.ahorromenores
--  4: metas.depositoalavista
--  5: metas.plazofijo
--  6: metas.prestamo
--  7: metas.morosidad
CREATE OR REPLACE FUNCTION metas.modificar_sucursal(
    new_sucursal VARCHAR(50),
    old_sucursal VARCHAR(50),
    arg_anyo INTEGER,
    arg_mes INTEGER, 
    tabla INTEGER)

RETURNS VOID AS $$
DECLARE
    existe INTEGER;
    new_suc_id INTEGER;
    old_suc_id INTEGER;
BEGIN
    new_suc_id := 0;
    old_suc_id := 0;
    new_sucursal := LOWER(new_sucursal);
    old_sucursal := LOWER(old_sucursal);
    
    -- Se revisa que la sucursal exista
    SELECT id_sucursal
        INTO new_suc_id
        FROM public.sucursales
        WHERE sucursal = new_sucursal;
    
    SELECT id_sucursal
        INTO old_suc_id
        FROM public.sucursales
        WHERE sucursal = old_sucursal;

    IF old_suc_id <> 0 AND new_suc_id <> 0 THEN
        CASE tabla
            WHEN 0 THEN     
                SELECT count(*) 
                    INTO existe
                    FROM metas.socios
                    WHERE 
                        mes = arg_mes AND
                        anyo = arg_anyo AND
                        sucursal = old_suc_id;
                IF existe <> 0 THEN
                    UPDATE metas.socios
                        SET sucursal = new_suc_id
                        WHERE 
                            mes = arg_mes AND
                            anyo = arg_anyo AND
                            sucursal = old_suc_id;
                ELSE
                    RAISE EXCEPTION 'No existe el registro'
                        USING HINT='Verifique la información por favor';
                END IF;
            WHEN 1 THEN     
                SELECT count(*) 
                   INTO existe
                   FROM metas.monores
                   WHERE 
                       mes = arg_mes AND
                       anyo = arg_anyo AND
                       sucursal = old_suc_id;
                IF existe <> 0 THEN
                    UPDATE metas.menores
                        SET sucursal = new_suc_id
                        WHERE 
                            mes = arg_mes AND
                            anyo = arg_anyo AND
                            sucursal = old_suc_id;
                ELSE
                    RAISE EXCEPTION 'No existe el registro'
                        USING HINT='Verifique la información por favor';
                END IF;
            WHEN 2 THEN     
                SELECT count(*) 
                    INTO existe
                    FROM metas.ahorrosocios
                    WHERE 
                        mes = arg_mes AND
                        anyo = arg_anyo AND
                        sucursal = old_suc_id;
                IF existe <> 0 THEN
                    UPDATE metas.ahorrosocios
                        SET sucursal = new_suc_id
                        WHERE 
                            mes = arg_mes AND
                            anyo = arg_anyo AND
                            sucursal = old_suc_id;
                ELSE
                    RAISE EXCEPTION 'No existe el registro'
                        USING HINT='Verifique la información por favor';
                END IF;
            WHEN 3 THEN     
                SELECT count(*) 
                   INTO existe
                   FROM metas.ahorromenores
                   WHERE 
                       mes = arg_mes AND
                       anyo = arg_anyo AND
                       sucursal = old_suc_id;
                IF existe <> 0 THEN
                    UPDATE metas.ahorromenores
                        SET sucursal = new_suc_id
                        WHERE 
                            mes = arg_mes AND
                            anyo = arg_anyo AND
                            sucursal = old_suc_id;
                ELSE
                    RAISE EXCEPTION 'No existe el registro'
                        USING HINT='Verifique la información por favor';
                END IF;
            WHEN 4 THEN     
                SELECT count(*) 
                   INTO existe
                   FROM metas.depositoalavista
                   WHERE 
                       mes = arg_mes AND
                       anyo = arg_anyo AND
                       sucursal = old_suc_id;
                IF existe <> 0 THEN
                    UPDATE metas.depositoalavista
                        SET sucursal = new_suc_id
                        WHERE 
                            mes = arg_mes AND
                            anyo = arg_anyo AND
                            sucursal = old_suc_id;
                ELSE
                    RAISE EXCEPTION 'No existe el registro'
                        USING HINT='Verifique la información por favor';
                END IF;
            WHEN 5 THEN     
                SELECT count(*) 
                   INTO existe
                   FROM metas.plazofijo
                   WHERE 
                       mes = arg_mes AND
                       anyo = arg_anyo AND
                       sucursal = old_suc_id;
                IF existe <> 0 THEN
                    UPDATE metas.plazofijo
                        SET sucursal = new_suc_id
                        WHERE 
                            mes = arg_mes AND
                            anyo = arg_anyo AND
                            sucursal = old_suc_id;
                ELSE
                    RAISE EXCEPTION 'No existe el registro'
                        USING HINT='Verifique la información por favor';
                END IF;
            WHEN 6 THEN     
                SELECT count(*) 
                   INTO existe
                   FROM metas.prestamo
                   WHERE 
                       mes = arg_mes AND
                       anyo = arg_anyo AND
                       sucursal = old_suc_id;
                IF existe <> 0 THEN
                    UPDATE metas.prestamo
                        SET sucursal = new_suc_id
                        WHERE 
                            mes = arg_mes AND
                            anyo = arg_anyo AND
                            sucursal = old_suc_id;
                ELSE
                    RAISE EXCEPTION 'No existe el registro'
                        USING HINT='Verifique la información por favor';
                END IF;
            WHEN 7 THEN     
                SELECT count(*) 
                   INTO existe
                   FROM metas.morosidad
                   WHERE 
                       mes = arg_mes AND
                       anyo = arg_anyo AND
                       sucursal = old_suc_id;
                IF existe <> 0 THEN
                    UPDATE metas.morosidad
                        SET sucursal = new_suc_id
                        WHERE 
                            mes = arg_mes AND
                            anyo = arg_anyo AND
                            sucursal = old_suc_id;
                ELSE
                    RAISE EXCEPTION 'No existe el registro'
                        USING HINT='Verifique la información por favor';
                END IF;
        END CASE;
            RAISE NOTICE 'Modificación exitosa';
    ELSE
        RAISE EXCEPTION 'No existe la sucursal.';
    END IF; 
END;
$$ LANGUAGE plpgsql;

-- Eliminar un registro de una tabla de metas
-- indicando la tabla en el último argumento.
-- si tabla =
--  0: metas.socios
--  1: metas.menores
--  2: metas.ahorrosocios
--  3: metas.ahorromenores
--  4: metas.depositoalavista
--  5: metas.plazofijo
--  6: metas.prestamo
--  7: metas.morosidad
CREATE OR REPLACE FUNCTION metas.borrar_registro(
    arg_sucursal VARCHAR(50),
    arg_anyo INTEGER,
    arg_mes INTEGER, 
    tabla INTEGER)

RETURNS VOID AS $$
DECLARE
    suc_id INTEGER;
    existe INTEGER;
BEGIN
    suc_id := 0;
    arg_sucursal := LOWER(arg_sucursal);
    
    -- Se revisa que la sucursal exista
    SELECT id_sucursal
        INTO suc_id
        FROM public.sucursales
        WHERE sucursal = arg_sucursal;
    
    IF suc_id <> 0 THEN
        CASE tabla
            WHEN 0 THEN     
                SELECT count(*) 
                    INTO existe
                    FROM metas.socios
                    WHERE 
                        mes = arg_mes AND
                        anyo = arg_anyo AND
                        sucursal = suc_id;
                IF existe <> 0 THEN
                    DELETE FROM metas.socios
                        WHERE 
                            mes = arg_mes AND
                            anyo = arg_anyo AND
                            sucursal = suc_id;
                ELSE
                    RAISE EXCEPTION 'No existe el registro'
                        USING HINT='Verifique la información por favor';
                END IF;
            WHEN 1 THEN     
                SELECT count(*) 
                    INTO existe
                    FROM metas.menores
                    WHERE 
                        mes = arg_mes AND
                        anyo = arg_anyo AND
                        sucursal = suc_id;
                IF existe <> 0 THEN
                    DELETE FROM metas.menores
                        WHERE 
                            mes = arg_mes AND
                            anyo = arg_anyo AND
                            sucursal = suc_id;
                ELSE
                    RAISE EXCEPTION 'No existe el registro'
                        USING HINT='Verifique la información por favor';
                END IF;
            WHEN 2 THEN     
                SELECT count(*) 
                    INTO existe
                    FROM metas.ahorrosocios
                    WHERE 
                        mes = arg_mes AND
                        anyo = arg_anyo AND
                        sucursal = suc_id;
                IF existe <> 0 THEN
                    DELETE FROM metas.ahorrosocios
                        WHERE 
                            mes = arg_mes AND
                            anyo = arg_anyo AND
                            sucursal = suc_id;
                ELSE
                    RAISE EXCEPTION 'No existe el registro'
                        USING HINT='Verifique la información por favor';
                END IF;
            WHEN 3 THEN     
                SELECT count(*) 
                    INTO existe
                    FROM metas.ahorromenores
                    WHERE 
                        mes = arg_mes AND
                        anyo = arg_anyo AND
                        sucursal = suc_id;
                IF existe <> 0 THEN
                    DELETE FROM metas.ahorromenores
                        WHERE 
                            mes = arg_mes AND
                            anyo = arg_anyo AND
                            sucursal = suc_id;
                ELSE
                    RAISE EXCEPTION 'No existe el registro'
                        USING HINT='Verifique la información por favor';
                END IF;
            WHEN 4 THEN     
                SELECT count(*) 
                    INTO existe
                    FROM metas.depositoalavista
                    WHERE 
                        mes = arg_mes AND
                        anyo = arg_anyo AND
                        sucursal = suc_id;
                IF existe <> 0 THEN
                    DELETE FROM metas.depositoalavista
                        WHERE 
                            mes = arg_mes AND
                            anyo = arg_anyo AND
                            sucursal = suc_id;
                ELSE
                    RAISE EXCEPTION 'No existe el registro'
                        USING HINT='Verifique la información por favor';
                END IF;
            WHEN 5 THEN     
                SELECT count(*) 
                    INTO existe
                    FROM metas.plazofijo
                    WHERE 
                        mes = arg_mes AND
                        anyo = arg_anyo AND
                        sucursal = suc_id;
                IF existe <> 0 THEN
                    DELETE FROM metas.plazofijo
                        WHERE 
                            mes = arg_mes AND
                            anyo = arg_anyo AND
                            sucursal = suc_id;
                ELSE
                    RAISE EXCEPTION 'No existe el registro'
                        USING HINT='Verifique la información por favor';
                END IF;
            WHEN 6 THEN     
                SELECT count(*) 
                    INTO existe
                    FROM metas.prestamo
                    WHERE 
                        mes = arg_mes AND
                        anyo = arg_anyo AND
                        sucursal = suc_id;
                IF existe <> 0 THEN
                    DELETE FROM metas.prestamo
                        WHERE 
                            mes = arg_mes AND
                            anyo = arg_anyo AND
                            sucursal = suc_id;
                ELSE
                    RAISE EXCEPTION 'No existe el registro'
                        USING HINT='Verifique la información por favor';
                END IF;
            WHEN 7 THEN     
                SELECT count(*) 
                    INTO existe
                    FROM metas.morosidad
                    WHERE 
                        mes = arg_mes AND
                        anyo = arg_anyo AND
                        sucursal = suc_id;
                IF existe <> 0 THEN
                    DELETE FROM metas.morosidad
                        WHERE 
                            mes = arg_mes AND
                            anyo = arg_anyo AND
                            sucursal = suc_id;
                ELSE
                    RAISE EXCEPTION 'No existe el registro'
                        USING HINT='Verifique la información por favor';
                END IF;
        END CASE;
        RAISE NOTICE 'Captura exitosa';
    ELSE
        RAISE EXCEPTION 'No existe la sucursal %', arg_sucursal;
    END IF; 
END;
$$ LANGUAGE plpgsql;

-- Funciones para actualizar los presupuestos
-- Metas.Socios y menores -----------------------------------------------------
-- si tabla es cero modifica el registro en metas.socios de lo contrario lo
-- modifica en la tabla metas.menores
CREATE OR REPLACE FUNCTION metas.modificar_presupuesto(
    arg_sucursal VARCHAR(50),
    arg_presupuesto INTEGER,
    arg_anyo INTEGER,
    arg_mes INTEGER,
    tabla INTEGER)

RETURNS VOID AS $$
DECLARE
    existe INTEGER;
    suc_id INTEGER;
BEGIN
    suc_id := 0;
    arg_sucursal := LOWER(arg_sucursal);
    
    -- Se revisa que la sucursal exista
    SELECT id_sucursal
        INTO suc_id
        FROM public.sucursales
        WHERE sucursal = arg_sucursal;

    IF suc_id <> 0 THEN
        IF tabla = 0 THEN
            SELECT count(*) 
                INTO existe
                FROM metas.socios
                WHERE 
                    mes = arg_mes AND
                    anyo = arg_anyo AND
                    sucursal = suc_id;
            IF existe <> 0 THEN
                UPDATE metas.socios
                    SET presupuesto = arg_presupuesto
                    WHERE 
                        anyo = arg_anyo AND
                        mes = arg_mes AND
                        sucursal = suc_id;
            ELSE
                RAISE EXCEPTION 'No existe el registro';
            END IF;
        ELSE
            SELECT count(*) 
                INTO existe
                FROM metas.menores
                WHERE 
                    mes = arg_mes AND
                    anyo = arg_anyo AND
                    sucursal = suc_id;
            IF existe <> 0 THEN
                UPDATE metas.menores
                    SET presupuesto = arg_presupuesto
                    WHERE 
                        anyo = arg_anyo AND
                        mes = arg_mes AND
                        sucursal = suc_id;
            ELSE
                RAISE EXCEPTION 'Registro inexistente';
            END IF;
        END IF;
        RAISE NOTICE 'Captura exitosa';
    ELSE
        RAISE EXCEPTION 'No existe la sucursal %', arg_sucursal;
    END IF; 
END;
$$ LANGUAGE plpgsql;

-- Metas. Todas las demas -----------------------------------------------------
-- Agregar un registro nuevo cada año.
-- si tabla =
--  0: metas.ahorrosocios
--  1: metas.ahorromenores
--  2: metas.depositoalavista
--  3: metas.plazofijo
--  4: metas.prestamo
--  5: metas.morosidad
CREATE OR REPLACE FUNCTION metas.modificar_presupuesto2(
    arg_sucursal VARCHAR(50),
    arg_presupuesto REAL,
    arg_anyo INTEGER,
    arg_mes INTEGER,
    tabla INTEGER)

RETURNS VOID AS $$
DECLARE
    existe INTEGER;
    suc_id INTEGER;
BEGIN
    suc_id := 0;
    arg_sucursal := LOWER(arg_sucursal);
    
    -- Se revisa que la sucursal exista
    SELECT id_sucursal
        INTO suc_id
        FROM public.sucursales
        WHERE sucursal = arg_sucursal;

    IF suc_id <> 0 THEN
        CASE tabla
            WHEN 0 THEN
                SELECT count(*) 
                    INTO existe
                    FROM metas.ahorrosocios
                    WHERE 
                        mes = arg_mes AND
                        anyo = arg_anyo AND
                        sucursal = suc_id;
                IF existe <> 0 THEN
                    UPDATE metas.ahorrosocios
                        SET presupuesto = arg_presupuesto
                        WHERE
                            mes = arg_mes AND
                            anyo = arg_anyo AND
                            sucursal = suc_id;

                ELSE
                    RAISE EXCEPTION 'Registro inexistente';
                END IF;
            WHEN 1 THEN
                SELECT count(*) 
                    INTO existe
                    FROM metas.ahorromenores
                    WHERE 
                        mes = arg_mes AND
                        anyo = arg_anyo AND
                        sucursal = suc_id;
                IF existe <> 0 THEN
                    UPDATE metas.ahorromenores
                        SET presupuesto = arg_presupuesto
                        WHERE
                            mes = arg_mes AND
                            anyo = arg_anyo AND
                            sucursal = suc_id;
                ELSE
                    RAISE EXCEPTION 'Registro inexistente';
                END IF;
            WHEN 2 THEN
                SELECT count(*) 
                    INTO existe
                    FROM metas.depositoalavista
                    WHERE 
                        mes = arg_mes AND
                        anyo = arg_anyo AND
                        sucursal = suc_id;
                IF existe <> 0 THEN
                    UPDATE metas.depositoalavista
                        SET presupuesto = arg_presupuesto
                        WHERE
                            mes = arg_mes AND
                            anyo = arg_anyo AND
                            sucursal = suc_id;

                ELSE
                    RAISE EXCEPTION 'Registro inexistente';
                END IF;
           WHEN 3 THEN
                SELECT count(*) 
                    INTO existe
                    FROM metas.plazofijo
                    WHERE 
                        mes = arg_mes AND
                        anyo = arg_anyo AND
                        sucursal = suc_id;
                IF existe <> 0 THEN
                    UPDATE metas.plazofijo
                        SET presupuesto = arg_presupuesto
                        WHERE
                            mes = arg_mes AND
                            anyo = arg_anyo AND
                            sucursal = suc_id;

                ELSE
                    RAISE EXCEPTION 'Registro inexistente';
                END IF;
            WHEN 4 THEN
                SELECT count(*) 
                    INTO existe
                    FROM metas.prestamo
                    WHERE 
                    mes = arg_mes AND
                    anyo = arg_anyo AND
                    sucursal = suc_id;
                IF existe <> 0 THEN
                    UPDATE metas.prestamo
                        SET presupuesto = arg_presupuesto
                        WHERE
                            mes = arg_mes AND
                            anyo = arg_anyo AND
                            sucursal = suc_id;

                ELSE
                    RAISE EXCEPTION 'Registro inexistente';
                END IF;
            WHEN 5 THEN
                SELECT count(*) 
                    INTO existe
                    FROM metas.morosidad
                    WHERE 
                    mes = arg_mes AND
                    anyo = arg_anyo AND
                    sucursal = suc_id;
                IF existe <> 0 THEN
                    UPDATE metas.morosidad
                        SET presupuesto = arg_presupuesto
                        WHERE
                            mes = arg_mes AND
                            anyo = arg_anyo AND
                            sucursal = suc_id;

                ELSE
                    RAISE EXCEPTION 'Registro inexistente';
                END IF;
            RAISE NOTICE 'Captura exitosa';
        END CASE;
    ELSE
        RAISE EXCEPTION 'No existe la sucursal %', arg_sucursal;
    END IF; 
END;
$$ LANGUAGE plpgsql;

-- Capturar en el campo Realizado y en el campo Ingreso y retiro según
-- correponda

-- Metas.Socios y menores -----------------------------------------------------
-- si tabla es cero modifica el registro en metas.socios de lo contrario lo
-- modifica en la tabla metas.menores
CREATE OR REPLACE FUNCTION metas.captura_socios(
    arg_sucursal VARCHAR(50),
    arg_ingreso INTEGER,
    arg_retiro INTEGER,
    arg_anyo INTEGER,
    arg_mes INTEGER,
    tabla INTEGER)

RETURNS VOID AS $$
DECLARE
    existe INTEGER;
    suc_id INTEGER;
BEGIN
    suc_id := 0;
    arg_sucursal := LOWER(arg_sucursal);
    
    -- Se revisa que la sucursal exista
    SELECT id_sucursal
        INTO suc_id
        FROM public.sucursales
        WHERE sucursal = arg_sucursal;

    IF suc_id <> 0 THEN
        IF tabla = 0 THEN
            SELECT count(*) 
                INTO existe
                FROM metas.socios
                WHERE 
                    mes = arg_mes AND
                    anyo = arg_anyo AND
                    sucursal = suc_id;
            IF existe <> 0 THEN
                UPDATE metas.socios
                    SET
                        ingreso = arg_ingreso,
                        retiro = arg_retiro
                    WHERE 
                        anyo = arg_anyo AND
                        mes = arg_mes AND
                        sucursal = suc_id;
            ELSE
                RAISE EXCEPTION 'No existe el registro';
            END IF;
        ELSE
            SELECT count(*) 
                INTO existe
                FROM metas.menores
                WHERE 
                    mes = arg_mes AND
                    anyo = arg_anyo AND
                    sucursal = suc_id;
            IF existe <> 0 THEN
                UPDATE metas.menores
                    SET 
                        ingreso = arg_ingreso,
                        retiro = arg_retiro
                    WHERE 
                        anyo = arg_anyo AND
                        mes = arg_mes AND
                        sucursal = suc_id;
            ELSE
                RAISE EXCEPTION 'Registro inexistente';
            END IF;
        END IF;
        RAISE NOTICE 'Captura exitosa';
    ELSE
        RAISE EXCEPTION 'No existe la sucursal %', arg_sucursal;
    END IF; 
END;
$$ LANGUAGE plpgsql;

-- Metas. Todas las demas -----------------------------------------------------
-- Agregar un registro nuevo cada año.
-- si tabla =
--  0: metas.ahorrosocios
--  1: metas.ahorromenores
--  2: metas.depositoalavista
--  3: metas.plazofijo
--  4: metas.prestamo
--  5: metas.morosidad
CREATE OR REPLACE FUNCTION metas.capturar2(
    arg_sucursal VARCHAR(50),
    arg_realizado REAL,
    arg_anyo INTEGER,
    arg_mes INTEGER,
    tabla INTEGER)

RETURNS VOID AS $$
DECLARE
    existe INTEGER;
    suc_id INTEGER;
BEGIN
    suc_id := 0;
    arg_sucursal := LOWER(arg_sucursal);
    
    -- Se revisa que la sucursal exista
    SELECT id_sucursal
        INTO suc_id
        FROM public.sucursales
        WHERE sucursal = arg_sucursal;

    IF suc_id <> 0 THEN
        CASE tabla
            WHEN 0 THEN
                SELECT count(*) 
                    INTO existe
                    FROM metas.ahorrosocios
                    WHERE 
                        mes = arg_mes AND
                        anyo = arg_anyo AND
                        sucursal = suc_id;
                IF existe <> 0 THEN
                    UPDATE metas.ahorrosocios
                        SET realizado = arg_realizado
                        WHERE
                            mes = arg_mes AND
                            anyo = arg_anyo AND
                            sucursal = suc_id;

                ELSE
                    RAISE EXCEPTION 'Registro inexistente';
                END IF;
            WHEN 1 THEN
                SELECT count(*) 
                    INTO existe
                    FROM metas.ahorromenores
                    WHERE
                        mes = arg_mes AND
                        anyo = arg_anyo AND
                        sucursal = suc_id;
                IF existe <> 0 THEN
                    UPDATE metas.ahorromenores
                        SET realizado = arg_realizado
                        WHERE
                            mes = arg_mes AND
                            anyo = arg_anyo AND
                            sucursal = suc_id;
                ELSE
                    RAISE EXCEPTION 'Registro inexistente';
                END IF;
            WHEN 2 THEN
                SELECT count(*) 
                    INTO existe
                    FROM metas.depositoalavista
                    WHERE 
                        mes = arg_mes AND
                        anyo = arg_anyo AND
                        sucursal = suc_id;
                IF existe <> 0 THEN
                    UPDATE metas.depositoalavista
                        SET realizado = arg_realizado
                        WHERE
                            mes = arg_mes AND
                            anyo = arg_anyo AND
                            sucursal = suc_id;

                ELSE
                    RAISE EXCEPTION 'Registro inexistente';
                END IF;
           WHEN 3 THEN
                SELECT count(*) 
                    INTO existe
                    FROM metas.plazofijo
                    WHERE 
                        mes = arg_mes AND
                        anyo = arg_anyo AND
                        sucursal = suc_id;
                IF existe <> 0 THEN
                    UPDATE metas.plazofijo
                        SET realizado = arg_realizado
                        WHERE
                            mes = arg_mes AND
                            anyo = arg_anyo AND
                            sucursal = suc_id;

                ELSE
                    RAISE EXCEPTION 'Registro inexistente';
                END IF;
            WHEN 4 THEN
                SELECT count(*) 
                    INTO existe
                    FROM metas.prestamo
                    WHERE 
                    mes = arg_mes AND
                    anyo = arg_anyo AND
                    sucursal = suc_id;
                IF existe <> 0 THEN
                    UPDATE metas.prestamo
                        SET realizado = arg_realizado
                        WHERE
                            mes = arg_mes AND
                            anyo = arg_anyo AND
                            sucursal = suc_id;

                ELSE
                    RAISE EXCEPTION 'Registro inexistente';
                END IF;
            WHEN 5 THEN
                SELECT count(*) 
                    INTO existe
                    FROM metas.morosidad
                    WHERE 
                    mes = arg_mes AND
                    anyo = arg_anyo AND
                    sucursal = suc_id;
                IF existe <> 0 THEN
                    UPDATE metas.morosidad
                        SET realizado = arg_realizado
                        WHERE
                            mes = arg_mes AND
                            anyo = arg_anyo AND
                            sucursal = suc_id;

                ELSE
                    RAISE EXCEPTION 'Registro inexistente';
                END IF;

            RAISE NOTICE 'Captura exitosa';
        END CASE;
    ELSE
        RAISE EXCEPTION 'No existe la sucursal %', arg_sucursal;
    END IF; 
END;
$$ LANGUAGE plpgsql;
