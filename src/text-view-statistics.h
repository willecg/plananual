/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 4; tab-width: 4 -*-  */
/*
 * text-view-statistics.h
 * Copyright (C) 2014 William E. Cardenas Gomez <sistemas.will@alianzacpa.com.mx>
 *
 * plananual-1.2 is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * plananual-1.2 is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _TEXT_VIEW_STATISTICS_H_
#define _TEXT_VIEW_STATISTICS_H_

#include <gtkmm/textview.h>
#include <gtkmm/builder.h>
#include <gtkmm/textbuffer.h>

#include "estadisticas/estadistica.h"
#include "../lib/baseDatos.h"

/**\brief Namespace to capture the branch advance.
 * 
 * This namespace is used to the plancaptura program.*/
namespace capture{

/**\brief Shows the information about the statistics on the screen.
 * 
 * This class inherits from a Gtk::TextView, and uses its own Gtk::TextBuffer
 * to print the information on it.*/
class TextViewStatistics : public Gtk::TextView 
{
public:
    /**\brief Default constructor.
     *
     * This should only used by the function get_derived_widget of the 
     * object Gtk::Builder.
     * \param "BaseObjectType * cobject" Reference to the object.
     * \param "Glib::RefPtr<Gtk::Builder> builder" Smart pointer to the builder
     *        object.*/
    TextViewStatistics(BaseObjectType *cobject,
                       Glib::RefPtr<Gtk::Builder> builder);

    /**Destructor.*/
    ~TextViewStatistics();

    /**\brief Make a query for the database.
     *
     * Makes a particular query for fill the data in the start of this
     * application.
     * \param "estadisticas::TipoCuenta acount" Type of acount.
     * \param "Glib::ustring branch" Branch to get the information.
     * \param "int year" year to get information.
     * \return An Glib::ustring with the query. */
    static Glib::ustring get_query(
            estadisticas::TipoCuenta acount,
            Glib::ustring branch,
            int year);

    /**\brief Set the text on the Gtk::TextBuffer.
     *
     * Prepares the Gtk::TextBuffer, iter on all the kinds of acounts, and show
     * the information on the textview.*/
    void set_text(int month, int day, db::BaseDatos * conn);

/*---------------------------------------------------------------------------*
 *                                                                           *
 *                           Get and set functions                           *
 *                                                                           *
 *---------------------------------------------------------------------------*/
    /**Set the year.
     * \param "int year" The year.*/
    void set_year(int year)
    {
        this->year = year;
    }

    /**Set the branch name.
     * \param "Glib::ustring branch" Branch name.*/
    void set_branch(Glib::ustring branch)
    {
        this->branch = branch;
    }

    /**Get the year.
     * \return Year.*/
    int get_year()
    {
        return year;
    }

    /**Get the branch name.
     * \return Branch name.*/
    Glib::ustring get_branch()
    {
        return branch;
    }

private:
    int year;
    Glib::ustring branch;
    Glib::RefPtr<Gtk::TextBuffer> buffer;
    Glib::RefPtr<Gtk::TextBuffer::TagTable> ref_tag_table;
    Glib::RefPtr<Gtk::TextBuffer::Tag> ref_tag_default;
    Glib::RefPtr<Gtk::TextBuffer::Tag> ref_tag_header;
    Glib::RefPtr<Gtk::TextBuffer::Tag> ref_tag_header2;
    Glib::RefPtr<Gtk::TextBuffer::Tag> ref_tag_table_header;
};
} // End of the namespace 
#endif // _TEXT_VIEW_STATISTICS_H_

