/*
 * planadmin: administracion-gui.h
 * Gui for the administration.
 *
 * Copyright © 2014 William Ernesto Cárdenas Gomez
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**\brief Control of the admin of the statistics.
 *
 * This namespace store the gui and operation of the plan admin program.*/
#ifndef ADMINISTRACION_GUI
#define ADMINISTRACION_GUI

#include <gtkmm/builder.h>
#include <gtkmm/applicationwindow.h>
#include <gtkmm/toggleaction.h>
#include <gtkmm/viewport.h>
#include <gtkmm/spinbutton.h>

#include "combo_branches.h"
#include "list_capture.h"
#include "list_reports.h"
#include "print-form-operation.h"
#include "../lib/baseDatos.h"

/**\brief Namespace admin for the admin operations.
 *
 * Declares the gui for the admin operations of the plan proyect. May be on
 * follow versions will be integrated in plan namespace.*/ 
namespace admin
{
/**\brief Gui.
 *
 * Shows and operates the events of the gui */
class Admin_gui{
/*---------------------------------------------------------------------------*
 *                                                                           *
 *                            Member functions                               *
 *                                                                           *
 *---------------------------------------------------------------------------*/
public:
    /*          Constructor and destructor              */
    /**\brief The constructor.
     * 
     * Prepares the gui and the events. */
    Admin_gui(db::BaseDatos *conn);
    /**Destructor*/
    ~Admin_gui();

    /*           Member functions       */
    /**\brief Shows the window and its widgets.
     *
     * If all was correctly prepared, this function can load the gui to start
     * to work.*/
    int open_window(int argc, char *argv[]);
private:
    /**\brief Help for the constructor.
     *
     * By this all the events are asigned to the right member functions in this
     * class. */
    void load_events();

    /**\brief Set the interface for the reports.
     *
     * This is only able for the report.
     * @param "bool reports" True for the reports interface False for the
     * capture. */
    void load_reports_interface(bool reports);

public:
    /**\brief Print or preview dialog.
     *
     * Call the print or the preview dialog.
     *\param "Gtk::PrintOperationAction print_action" Type of dialog*/
    void print_or_preview(Gtk::PrintOperationAction print_action);

    /*++++++++++++++++++++++++++++Events+++++++++++++++++++++++++++++++++*/
    /*-------------------------Action events-----------------------------*/
    /*                        Menu file events                           */
    /**\brief Save what is is the treeview at this time.
     *
     * Call the function of the object listCapture for store the information
     * on the database.*/
    void clic_on_save();

    /**\brief Config the page to print. */
    void clic_on_config_page();

    /**\brief Show the preview dialog.*/
    void clic_on_preview();

    /**\brief Show the print dialog.*/
    void clic_on_print();

    /**\brief Print to a pdf file.*/
    void clic_on_export_pdf();
  
    /**\brief Close the main window.
     *
     * Close the main window and send the TERM signal to the process. */
    void clic_on_exit();

    /*                          Menu edit event                          */
    /**\brief Prepares the TreeView with the new year records.
     *
     * With this function the tree view is filled with one row by branch and
     * theirs other columns with empty values */
    void clic_on_add();
    /**\brief Delete the records for the next year.
     *
     * Delete from the database information for the next year. */
    void clic_on_delete();
    /**\brief Clear the Gtk::TreeView.
     *
     * Clear the Gtk::TreeView. */
    void clic_on_clear();
    /**\brief Prepares the TreeView to edit records.
     *
     * With this function the tree view is filled with one row by branch and
     * theirs other columns with stored values. */
    void clic_on_edit();
    
    /*                         Menu preferences event                    */
    /**\brief Choose betweeen two interfaces.
     *
     * If is selected is able to create reports, elsewere can create a new
     * capture of the year*/
    void clic_on_reports();

    /*                           Menu help event                          */
    /**\brief Show the about dialog.
     *
     * This function is associated with the about action*/
    void clic_on_about();

    /*--------------------------Other events------------------------------*/
    /**\brief This function should be associated to the signal_delete_event.
     *
     * Ask if the user want to close the application, if the answer is yes then
     * the application is clossed, elsewere, the application still working.*/
    bool on_exit_window(GdkEventAny *event);
    /**\brief This function is to create an sql query for the statistics.
     *
     * When clic on the ok button.*/
    void on_ok_button();

    /*--------------------------Print events---------------------------------*/
    /**\brief This event is called when the print process present a change.
     * 
     * When there is a change this function is called.  
     * \param "const Glib::RefPtr<plan::PrintFormOperation>& operation"*/ 
    void on_print_operation_status_changed(
            const Glib::RefPtr<plan::PrintFormOperation>& operation);

    /**\brief This event is called when the print process is done.
     * 
     * When the print proccess is done this function is called.  
     * \param "const Glib::RefPtr<plan::PrintFormOperation>& operation"*/
    void on_printoperation_done(
            Gtk::PrintOperationResult result,
            const Glib::RefPtr<plan::PrintFormOperation>& operation);

private:
/*---------------------------------------------------------------------------*
 *                                                                           *
 *                          Member fields                                    *
 *                                                                           *
 *---------------------------------------------------------------------------*/
    /* Database connection info.
     *
     * This data member store the information about the connection to database
     * and is the way to make operations on the database */
    db::BaseDatos *conn;
    bool interface;
    std::string query;

    /* Widgets */
    Glib::RefPtr<Gtk::Builder> builder;

    Gtk::ApplicationWindow *main_window;
    Gtk::Button *ok_button;
    Gtk::Viewport *port;
    ComboBoxBranches *combo_branches;
    Gtk::TreeView *treeCapture;
    Gtk::SpinButton *spinnButton;
    
    ListCapture listCapture;
    ListReports *treeReports;
    
    /* Actions */
    Glib::RefPtr<Gtk::Action> save_action;
    Glib::RefPtr<Gtk::Action> config_page_action;
    Glib::RefPtr<Gtk::Action> preview_action;
    Glib::RefPtr<Gtk::Action> print_action;
    Glib::RefPtr<Gtk::Action> export_pdf_action;
    Glib::RefPtr<Gtk::Action> quit_action;
    Glib::RefPtr<Gtk::Action> add_action;
    Glib::RefPtr<Gtk::Action> delete_action;
    Glib::RefPtr<Gtk::Action> edit_action;
    Glib::RefPtr<Gtk::Action> clear_action;
    Glib::RefPtr<Gtk::ToggleAction> reports_option_action;
    Glib::RefPtr<Gtk::Action> about_action;

    /* Print objects */
    Glib::RefPtr<Gtk::PageSetup> refPageSetup;
    Glib::RefPtr<Gtk::PrintSettings> refPrintSettings;
}; // Class End
} // Namespace end
#endif //_ADMINISTRACION_GUI
