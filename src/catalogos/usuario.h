/*
 * plananual: usuario.cc
 * Clase usuario: Declaracion.
 * Representa un objeto de la clase Usuario. Por esta clase se puede modificar
 * el nombre, borrar o crear un nuevo usuario en la base de datos.
 *
 * Copyright © 2014 William Ernesto Cárdenas Gomez
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef HAVE_USUARIOS_H
#define HAVE_USUARIOS_H

#include <string>
#include "../../lib/baseDatos.h"
#include "sucursal.h"
using namespace db;

/* Nombre de espacios para catalogos. */
namespace catalogos
{
/**\brief Manipula los usuarios de la aplicación. 
 *
 * Por medio de esta clase se pueden agregar, eliminar y modificar usuarios en
 * la base de datos */
class Usuario {
/*---------------------------------------------------------------------------*
 *                                                                           *
 *                     Funciones miembro de la clase                         *
 *                                                                           *
 *---------------------------------------------------------------------------*/
public:
    /* Constructores y destructores */
    Usuario(); // Constructor defaul. Valores basura
    Usuario(const int *const, BaseDatos *const); // usuario por el id
    Usuario(const std::string *const, BaseDatos *); // por nombre
    virtual ~Usuario(); // destructor

    /* Funciones set y get */
private:
        /* Funciones set */
    void setID(const int *const); // establece el id del objeto
    void setNombre(const std::string *const); // establece el nombre del objeto
    void setPassword(const std::string *const); // establece la contraseña del
                                                // objeto
    void setSucursal(const Sucursal *const); // establece a que sucursal
                                             // pertenece
public:
        /* Funciones get */
    int getID(); // obtiene el id del objeto 
    std::string getNombre(); // obtiene el nombre
    std::string getPassword(); // obtiene la contraseña
    Sucursal getSucursal(); // obtiene la sucursal

    /* Funciones operativas (Casos de uso) */
    static Usuario crear(             // crea un objeto
            const std::string *const,
            Sucursal *const,
            const std::string *const,
            BaseDatos *const);
    void cambiarSucursal(             // Cambia de sucursal al objeto
            Sucursal *const,
            BaseDatos *const);
    void cambiarNombre(               // Cambia el nombre y la contraseña
            const std::string *const,
            const std::string *const,
            BaseDatos *const);
    void cambiarPassword(             // Cambia solo la contraseña
            const std::string *const,
            BaseDatos *const);
    void eliminar(BaseDatos *const); // elimina el objeto de la db.

/*---------------------------------------------------------------------------*
 *                                                                           *
 *                           Campos de la clase                              *
 *                                                                           *
 *---------------------------------------------------------------------------*/
private:
    int id; // clave primaria en la base de datos.
    std::string nombre; // Nombre del usuario.
    std::string password; // Contraseña del usuario
    Sucursal sucursal; // Sucursal a la que pertenece
};
}; // Namespace catalogos
#endif //-HAVE_USUARIOS_H
