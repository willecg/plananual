/*
 * plananual: sucursal.cc
 * Sucursal Class: Implementation.
 * Represents an object called Sucursal. That is a branch (Sucursal is branch
 * in spanish) in the cooperative, by this, you can modify the name, delete it
 * from the data base and create a new by the "crear" static function.
 *
 * Copyright (C) 2014 William Ernesto Cárdenas Gomez
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "sucursal.h"
#include <string>

/**\brief Catálogos de usuarios y sucursales.
 *
 * Los catálogos son los elementos que pueden ser manipulados de forma mas o
 * menos regular en la base de datos. En este caso son los usuarios y las
 * sucursales. */
namespace catalogos
{

/*---------------------------------------------------------------------------*
 *                                                                           *
 *                        Constructors & destructors                         *
 *                                                                           *
 *---------------------------------------------------------------------------*/
/**\brief Default constructor.
 *
 * Create the object with trash values */
Sucursal::Sucursal()
{
    setNombre("");
    setID(0);
}

/**\brief Create an object by the id.
 *
 * Create the object reading on db, searching by the id.
 * @param "int id" Id of the object.
 * @param "BaseDatos *conexion" The database connection object */
Sucursal::Sucursal(int id, BaseDatos *conexion)
{
    std::string query =
        "SELECT sucursal FROM public.sucursales WHERE id_sucursal = " +
        std::to_string(id);
    if ( !conexion->ejecutaQuery(query))
        throw conexion->getLastError();
    if (conexion->nRegistros() <= 0) {
        std::string fails(_("There is not branches with this id."));
        throw fails;
    }
    setNombre(conexion->regresaDato(0,0));
    setID(id);
}

/**\brief Create an object by the name.
 *
 * Create the object reading from the bd, searching by the branch name
 * @param "std::string nombre" Branch name
 * @param "BaseDatos * conexion" The database connection object */
Sucursal::Sucursal(std::string nombre, BaseDatos *conexion)
{
    std::string query(
            "SELECT id_sucursal \
            FROM public.sucursales \
            WHERE sucursal = LOWER('" + nombre + "');");
    if (!conexion->ejecutaQuery(query))
        throw conexion->getLastError();
    if (conexion->nRegistros() <= 0) {
        std::string fails(_("There is not branches with this name."));
        throw fails;
    }
    setNombre(nombre);
    setID(std::stoi(conexion->regresaDato(0,0)));
}

/**\brief Destructor.
 *
 * Default destructor.*/
Sucursal::~Sucursal()
{}

/*---------------------------------------------------------------------------*
 *                                                                           *
 *                            set & get functions                            *
 *                                                                           *
 *---------------------------------------------------------------------------*/
/**\brief Set the name.
 *
 * Sets a name for the branch, and this is getted from the data base.
 * @param "std::string nombre" Sets the name of the branch object*/
void Sucursal::setNombre(std::string nombre)
{   
    this->nombre = nombre;//lower_string(&nombre);
}

/**\brief Set the name.
 *
 * Sets the id to the object. This most come from the database.
 * @param "int id" Sets the id of the branch object*/
void Sucursal::setID(int id)
{
    this->id = id;
}

/**\brief Get the ID.
 *
 * Return the current value of the id.
 * @return ID of the branch object.*/
int Sucursal::getID()
{
    return this->id;
}

/**\brief Get the name.
 *
 * Get the name of the branch object.
 * @return The branch object name.*/
std::string Sucursal::getNombre()
{
    return nombre;
}

/*---------------------------------------------------------------------------*
 *                                                                           *
 *                            operation functions                            *
 *                                                                           *
 *---------------------------------------------------------------------------*/
/**\brief Create a new object and database record.
 *
 * Create a new object Sucursal an make a new row in the db.
 * @param "std::string nombre" Branch name.
 * @param "BaseDatos * conexion" Data base connection.
 * @return The new sucursal object.*/
Sucursal Sucursal::crear(std::string nombre, BaseDatos *conexion)
{
    std::string query("SELECT public.nuevasucursal('" + nombre + "');");
    if (!conexion->ejecutaQuery(query))
        throw conexion->getLastError();
    Sucursal suc(nombre, conexion);
    return suc;
}

/**\brief Change the name.
 *
 * Sets a new name to the branch and update this information in db
 * @param "std::string nombre" New name for this branch.
 * @param "BaseDatos *conexion" Database connection.*/
void Sucursal::cambiarNombre(std::string nombre, BaseDatos *conexion)
{
    std::string query("SELECT public.cambiarSucursal('" +
            getNombre() + "','" + nombre + "');");
    if (!conexion->ejecutaQuery(query))
        throw conexion->getLastError();
    setNombre(nombre);
}

/**\brief delete from the database.
 *
 * Delete a branch from the data base and set name to empty string ("") and
 * id to zero (0)
 * @param "BaseDatos * conexion" Data base connection. */
void Sucursal::eliminar(BaseDatos *conexion)
{
    std::string query("SELECT public.borrarSucursal('"+getNombre()+"');");
    if (!conexion->ejecutaQuery(query))
        throw conexion->getLastError();
    setNombre("");
    setID(0);
}

} // Namespace catalogos
