/*
 * plancatalogos: catalogos.h
 * Control de catalogos.
 *
 * Copyright © 2014 William Ernesto Cárdenas Gomez
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef HAVE_CATALOGOS_H
#define HAVE_CATALOGOS_H

/* Gui Includes */
#include <gtkmm/treeview.h>
#include <gtkmm/treestore.h>
#include <gtkmm/treemodel.h>
#include <gtkmm/applicationwindow.h>
#include <gtkmm/button.h>
#include <gtkmm/entry.h>
#include <gtkmm/comboboxtext.h>
#include <gtkmm/builder.h>

#include "../../lib/baseDatos.h"

namespace catalogos{

/**\brief Type of event sended to sigc.
 *
 * When an event in the menu widget is sended, one of this should be setted to
 * decide what to do when click on one button.*/
enum Actions{
    NO_ACTION, /**<No action selected.*/
    ADMIN_PASSWORD,/**<Change the admin password.*/
    CAPTURE_DAYS,/**<Change the capture days (default 5).*/
    USER_NEW,/**<Make a new user*/
    USER_NAME_PASSWORD,/**<Change username*/
    USER_PASSWORD,/**<Change password*/
    USER_BRANCH,/**<Change the user branch.*/
    USER_DELETE,/**<Delete an user.*/
    BRANCH_NEW,/**<Make a new branch.*/
    BRANCH_EDIT,/**<Edit a branch name.*/
    BRANCH_DELETE/**<Delete a branch from database.*/
};

/**\brief Gtk gui control.
 *
 * By this class the interface is controlled. Its members are the widgets
 * that are drawn on screen and some useful stuff, and its functions are the
 * events and the use cases of the proyect.*/
class CatalogoGui {
public:
    /**\brief Constructor
     *
     * Create the Gtk interface. */
    CatalogoGui(db::BaseDatos *con);
    /**\brief Constructor
     * 
     * Delete the object. */
    virtual ~CatalogoGui();
    
    /**\brief Open a window.
     * 
     * Create a new window and prints on screen.
     * \param "int argc" Argc from main function.
     * \param "char **argv" Argv from main function. */
    int open_window(int argc, char **argv);
    
    /**\brief Load/Update a tree view
     * 
     * Updates the value of a tree with the model CatalogosTreeModel.
     * 
     * \param "Gtk::TreeView *arbol" the treeview to update.
     * \param "db::BaseDatos *const conn" Data base connection.*/
    void load_tree();
 
    /**\brief Load/Update the combo box
     * 
     * Updates the value of the combo box using branches.
     * 
     * \param "Gtk::TreeView *arbol" the treeview to update.
     * \param "db::BaseDatos *const conn" Data base connection.*/
    void load_combo();  
/*---------------------------------------------------------------------------*
 *                                                                           *
 *                          Event Gtk Gui Functions                          *
 *                                                                           *
 *---------------------------------------------------------------------------*/
    /*-----------------------    File Menu events    ------------------------*/
    /**\brief Change the database password for the user administrador_plan.
     *
     * This is called from the a_archivo_password. action*/   
    void file_admin_password_action();
    
    /**\brief Set avalibles days for branches.
     *
     * This is called from the a_archivo_dias_captura action. */
    void file_days_action();
    
    /**\brief Quit the application.
     *
     * This is called from a_archivo_salir action. */   
    void file_exit_action();

    /*---------------------     Users Menu events     -----------------------*/
    /**\brief Add a new user to database.
     *
     * This is called from the a_usuario_nuevo.*/
    void user_new_action();

    /**\brief Change une user name on database users and public.users table.
     *
     * This is called from the a_usuario_cambiar_nombre_password action.*/
    void user_change_name_action();

    /**\brief Only change the password.
     *
     * This is called from the a_usuario_cambiar_password action.*/
    void user_change_password_action();

    /**\brief Change the user branch.
     *
     * This is called from the a_usuario_cambiar_sucursal action.*/
    void user_change_branch_action();

    /**\brief Delete an user from data base.
     *
     * This is called from the a_usuario_cambiar_sucursal.*/
    void user_delete_action();

    /*-------------------     Branches Menu events     ----------------------*/
    /**\brief Add a new branch to database.
     *
     * This is called from the a_branch_nuevo.*/
    void branch_new_action();

    /**\brief Change branch name on public.sucursales table.
     *
     * This is called from the a_branch_edit action.*/
    void branch_edit_action();

    /**\brief Delete a branch from db.
     *
     * This is called from the a_branch_eliminar action.*/
    void branch_delete_action();

    /*--------------------     Help menu events    --------------------------*/
    /**\brief About Dialog.
     *
     * Show the about dialog. This is called from the a_about action.*/
    void help_about_action();

    /*-------------------         Buttons events    -------------------------*/
    /**\brief Clean GUI.
     *
     * Set null values to all entries. This is added to an clicked event to
     * b_clean.*/
    void on_clean_clicked();

    /**\brief Clean GUI and block the interface.
     *
     * Set null values to all entries and block all entries. This is added to
     * an clicked event to b_cancel button.*/
    void on_cancel_clicked();

    /**\brief Save changes on data base.
     *
     * When this is called the information is added to the database and the
     * entries are cleaned and blocked. This function is added to an clicked
     * event to b_ok button */
    void on_ok_clicked();

    bool quit(GdkEventAny* event);

    /**\brief Sets the type of action.
     *
     * Set an action from the enumeration Actions.*/
    void set_action(Actions a);

     /**\brief Gets the type of action.
     *
     * Get an action from the enumeration Actions.*/
     Actions get_action();

private:
    /*-----------------------------------------------------------------------*
     *                                                                       *
     *                          Operatives functions                         *
     *                          Case of uses.                                *
     *                                                                       *
     *-----------------------------------------------------------------------*/
    /**\brief Change the admin password.
     *
     * Change the user password of the administrador_plan user.
     * \return true if succes, otherwise false.*/
    bool change_admin_password();
    
    /**\brief change capture days.
     *
     * Set a new number of days avalibles to capture on the data base for
     * normal users.
     * \return true if succes, otherwise false.*/
    bool change_days_captures();
    
    /**\brief Adds a user.
     *
     * Set a new user for the data base.
     * \return true if succes, otherwise false.*/
    bool new_user();
    
    /**\brief Change user name and password.
     *
     * Set a new name and set a new password on database.
     * \return true if succes, otherwise false.*/
    bool change_user_and_password();
    
    /**\brief Like change_user_and_password but only password.
     *
     * Change the password that a user uses to connect to database.
     * \return true if succes, otherwise false.*/
    bool change_user_password();
    
    /**\brief Delete a user.
     *
     * Get information for the gui and delete the selected user on the tree.
     * \return true if succes, otherwise false.*/
    bool delete_user();
    
    /**\brief Change the user branch.
     *
     * Set a new branch for the selected user.
     * \return true if succes, otherwise false.*/
    bool change_user_branch();
    
    /**\brief Adds a new branch.
     *
     * On database adds a branch for statistics and user.
     * \return true if succes, otherwise false.*/
    bool new_branch();
    
    /**\brief Edit branch name.
     *
     * Set a new name for a branch.
     * \return true if succes, otherwise false.*/
    bool edit_branch();
    
    /**\brief Delete a branch.
     *
     * Delete a branch from data base.
     * \return true if succes, otherwise false.*/
    bool delete_branch();

public:
    /**\brief Model of the tree store.
     * 
     * This class represent the model that should be added to the tree.*/
    class CatalogosTreeModel : public Gtk::TreeModelColumnRecord {
    public:
        /**\brief Constructor.*/
        CatalogosTreeModel()
        {
            add(name);
        }
        /* Column. */
        Gtk::TreeModelColumn<Glib::ustring> name;
    }; // end of the class
private:
    /* Widgets */
    Gtk::ApplicationWindow *window;
    Gtk::Button *b_ok;
    Gtk::Button *b_cancel;
    Gtk::Button *b_clean;
    Gtk::Entry *e_name;
    Gtk::Entry *e_password;
    Gtk::Entry *e_retype_password;
    Gtk::ComboBoxText *cb_branches;
    /*Tree stuff*/
    CatalogosTreeModel my_model;
    Gtk::TreeView * arbol;
    Glib::RefPtr<Gtk::TreeStore> tree_store;
    Glib::RefPtr<Gtk::Builder> builder;

    /*Not gui atributes*/
    db::BaseDatos * conn;
    Actions action;
};
} // namespace catalogos
#endif // HAVE_CATALOGOS_H
