/*
 * plancatalogos: catalogos.cc
 * Control de catalogos.
 *
 * Copyright © 2014 William Ernesto Cárdenas Gomez
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <iostream>
#include <glibmm/i18n.h>
/* GUI includes */
#include <gtkmm/application.h>
#include <gtkmm/messagedialog.h>
#include <gtkmm/actiongroup.h>
#include <gtkmm/aboutdialog.h>

#include "catalogos.h"
#include "usuario.h"

/* This fuor macros are used to find the gui during develop and during
 * operation */
/* This two macros are useful only for production environment (When the
 * software is already installed). If you use it when it is not the program
 * is going to be finished */
#define UI_FILE_CATALOGOS PACKAGE_DATA_DIR"/ui/catalogos.glade"
#define UI_FILE_ABOUT PACKAGE_DATA_DIR"/ui/acerca_de.glade"

/* This macros are useful only on the development phase. 
 * If you install this software this will generate a mistake and the program
 * is going to be finished. */
//#define UI_FILE_CATALOGOS "gui/catalogos.glade"
//#define UI_FILE_ABOUT "gui/acerca_de.glade"
namespace catalogos{

/*---------------------------------------------------------------------------*
 *                                                                           *
 *                        Constructor & destructor                           *
 *                                                                           *
 *---------------------------------------------------------------------------*/
/**\brief Constructor.
 *
 * Create the interface, sets the events and prepares the tree about the gui.
 * Also store a db::BaseDatos object reference.
 *
 * \param "db::BaseDatos *con" Database connection.*/
CatalogoGui::CatalogoGui(db::BaseDatos *con) :
    conn(con)
{
    builder = Gtk::Builder::create_from_file(UI_FILE_CATALOGOS);

    /* Get widgets */ 
    builder->get_widget("win_app", window);
    builder->get_widget("arbol", arbol);
    builder->get_widget("e_nombre", e_name);
    builder->get_widget("e_password", e_password);
    builder->get_widget("e_repite_password", e_retype_password);
    builder->get_widget("b_limpiar", b_clean);
    builder->get_widget("b_cancelar", b_cancel);
    builder->get_widget("b_aceptar", b_ok);
    builder->get_widget("cbt_sucursal", cb_branches);

    /* Create actions */
    Glib::RefPtr<Gtk::ActionGroup> ag = Gtk::ActionGroup::create();
    /* File menu */
    Glib::RefPtr<Gtk::Action> action_file_pass =
        Glib::RefPtr<Gtk::Action>::cast_dynamic(
                builder->get_object("a_archivo_password"));
    Glib::RefPtr<Gtk::Action> action_days =
        Glib::RefPtr<Gtk::Action>::cast_dynamic(
                builder->get_object("a_archivo_dias_captura"));
    Glib::RefPtr<Gtk::Action> action_quit =
        Glib::RefPtr<Gtk::Action>::cast_dynamic(
                builder->get_object("a_archivo_salir"));
    /* User menu */
    Glib::RefPtr<Gtk::Action> action_newuser =
        Glib::RefPtr<Gtk::Action>::cast_dynamic(
                builder->get_object("a_usuario_nuevo"));
    Glib::RefPtr<Gtk::Action> action_change_username =
        Glib::RefPtr<Gtk::Action>::cast_dynamic(
                builder->get_object("a_usuario_cambiar_nombre_password"));
    Glib::RefPtr<Gtk::Action> action_change_pass =
        Glib::RefPtr<Gtk::Action>::cast_dynamic(
                builder->get_object("a_usuario_cambiar_password"));
    Glib::RefPtr<Gtk::Action> action_change_userbranch =
        Glib::RefPtr<Gtk::Action>::cast_dynamic(
                builder->get_object("a_usuario_cambiar_sucursal"));
    Glib::RefPtr<Gtk::Action> action_user_delete =
        Glib::RefPtr<Gtk::Action>::cast_dynamic(
                builder->get_object("a_usuario_eliminar"));
    /* Branch menu */
    Glib::RefPtr<Gtk::Action> action_new_branch =
        Glib::RefPtr<Gtk::Action>::cast_dynamic(
                builder->get_object("a_sucursal_nueva"));
    Glib::RefPtr<Gtk::Action> action_change_branchname =
        Glib::RefPtr<Gtk::Action>::cast_dynamic(
                builder->get_object("a_sucursal_cambiar"));
    Glib::RefPtr<Gtk::Action> action_delete_branch =
        Glib::RefPtr<Gtk::Action>::cast_dynamic(
                builder->get_object("a_sucursal_eliminar"));
    /* Help menu */
    Glib::RefPtr<Gtk::Action> action_about =
        Glib::RefPtr<Gtk::Action>::cast_dynamic(
                builder->get_object("a_about"));

    /* Events */
    window->signal_delete_event().connect(
            sigc::mem_fun(*this, &CatalogoGui::quit));
    b_ok->signal_clicked().connect(
            sigc::mem_fun(*this, &CatalogoGui::on_ok_clicked));
    b_cancel->signal_clicked().connect(
            sigc::mem_fun(*this, &CatalogoGui::on_cancel_clicked));
    b_clean->signal_clicked().connect(
            sigc::mem_fun(*this, &CatalogoGui::on_clean_clicked));
    
    /* Adding actions to the action group and adding events*/
    /*File*/
    ag->add(action_file_pass,
            sigc::mem_fun(*this, &CatalogoGui::file_admin_password_action));
    ag->add(action_days,
            sigc::mem_fun(*this, &CatalogoGui::file_days_action));
    ag->add(action_quit,
            sigc::mem_fun(*this, &CatalogoGui::file_exit_action));
    /*Users*/
    ag->add(action_newuser,
            sigc::mem_fun(*this, &CatalogoGui::user_new_action));
    ag->add(action_change_username,
            sigc::mem_fun(*this, &CatalogoGui::user_change_name_action));
    ag->add(action_change_pass,
            sigc::mem_fun(*this, &CatalogoGui::user_change_password_action));
    ag->add(action_change_userbranch,
            sigc::mem_fun(*this, &CatalogoGui::user_change_branch_action));
    ag->add(action_user_delete,
            sigc::mem_fun(*this, &CatalogoGui::user_delete_action));
    /*Branches*/
    ag->add(action_new_branch,
            sigc::mem_fun(*this, &CatalogoGui::branch_new_action));
    ag->add(action_change_branchname,
            sigc::mem_fun(*this, &CatalogoGui::branch_edit_action));
    ag->add(action_delete_branch,
            sigc::mem_fun(*this, &CatalogoGui::branch_delete_action));
    /*Help*/
    ag->add(action_about,
            sigc::mem_fun(*this, &CatalogoGui::help_about_action));
   
    /*Prepares tree*/
    tree_store = Gtk::TreeStore::create(my_model);
    arbol->set_model(tree_store);
    arbol->append_column(_("Branch/User"), my_model.name);
    load_tree();
}

/**\brief Constructor
 *
 * Delete the object. */
CatalogoGui::~CatalogoGui()
{}

/**\brief Open a window.
 *
 * Create a new window and prints on screen.
 * \param "int argc" Argc from main function.
 * \param "char **argv" Argv from main function. */
int CatalogoGui::open_window(int argc, char **argv)
{
    Glib::RefPtr<Gtk::Application> application =
        Gtk::Application::create(argc, argv, "cpaa.plan.catalogos");
    return application->run(*window);
}

/**\brief Load/Update a tree view
 *
 * Updates the value of a tree with the model CatalogosTreeModel.*/
void CatalogoGui::load_tree()
{
    const std::string query_get_branches(
            "SELECT s.sucursal, u.usuario \
            FROM public.sucursales s \
            LEFT OUTER JOIN public.usuarios u \
            ON s.id_sucursal = u.sucursal \
            ORDER BY s.id_sucursal, u.id_usuario;");
    if(!conn->ejecutaQuery(query_get_branches)) {
        Gtk::MessageDialog *dialog = new Gtk::MessageDialog
            (_("When get branches data."), false, Gtk::MESSAGE_WARNING);
        dialog->set_secondary_text(conn->getLastError());
        dialog->run();
        delete dialog;
        return;
    }

    /* Prepare the model, store, iterators and row variables. */
    tree_store->clear();
    Gtk::TreeModel::iterator iter, child_iter;
    Gtk::TreeModel::Row row, child_row;
    std::string suc;
    int j;
    for (int i = 0; i < conn->nRegistros(); i++) {
        iter = tree_store->append();
        row = *iter;
        row[my_model.name] = conn->regresaDato(i, 0);
        suc = conn->regresaDato(i, 0);
        for (j = i;
                j < conn->nRegistros() && suc == conn->regresaDato(j, 0);
                j++) {
            child_iter = tree_store->append(row.children());
            child_row = *child_iter;
            child_row[my_model.name] = conn->regresaDato(j, 1);
        }
        if (j != i)
            i = j - 1;
    }
}

/**\brief Load/Update our combo box
 *
 * Updates the values of out tree model. */
void CatalogoGui::load_combo()
{
    std::string query =
        "SELECT s.sucursal FROM public.sucursales s \
        ORDER BY s.id_sucursal;";
    if (conn->ejecutaQuery(query)) {
        cb_branches->remove_all();
        for (int i = 0; i < conn->nRegistros(); i++)
            cb_branches->append(conn->regresaDato(i,0));
        cb_branches->set_active_text(conn->regresaDato(0,0));
    }
    else {
        Gtk::MessageDialog er(*window, _("Can't load branches."), false,
                Gtk::MESSAGE_ERROR, Gtk::BUTTONS_OK, false);
        er.set_secondary_text(conn->getLastError());
        std::cerr << conn->getLastError() << std::endl;
        er.run();
    }
}

/*---------------------------------------------------------------------------*
 *                                                                           *
 *                          Event Gtk Gui Functions                          *
 *                                                                           *
 *---------------------------------------------------------------------------*/
/*---------------------      File menu events     ---------------------------*/
/**\brief Change the database password for the user administrador_plan.
 *
 * This is called from the a_archivo_password. action*/   
void CatalogoGui::file_admin_password_action()
{
    e_password->set_sensitive(true);
    e_retype_password->set_sensitive(true);
    b_ok->set_sensitive(true);
    b_clean->set_sensitive(true);
    b_cancel->set_sensitive(true);
    set_action(ADMIN_PASSWORD);
}
    
/**\brief Set avalibles days for branches.
 *
 * This is called from the a_archivo_dias_captura action. */
void CatalogoGui::file_days_action()
{
    e_name->set_sensitive(true);
    b_ok->set_sensitive(true);
    b_clean->set_sensitive(true);
    b_cancel->set_sensitive(true);
    set_action(CAPTURE_DAYS);
}

/**\brief Quit the application.
 *
 * This is called from a_archivo_salir action. */
void CatalogoGui::file_exit_action()
{
    Gtk::MessageDialog dialog(*window,
            _("Do you really want to quit?"),
            false,
            Gtk::MESSAGE_QUESTION, Gtk::BUTTONS_YES_NO,
            false);
    if(dialog.run() == Gtk::RESPONSE_YES)
        window->hide();
}

/*-----------------------     Users Menu events     -------------------------*/
/**\brief Add a new user to database.
 *
 * This is called from the a_usuario_nuevo.*/
void CatalogoGui::user_new_action()
{
    on_cancel_clicked();
    e_name->set_sensitive(true);
    e_password->set_sensitive(true);
    e_retype_password->set_sensitive(true);
    cb_branches->set_sensitive(true);
    load_combo();
    b_ok->set_sensitive(true);
    b_clean->set_sensitive(true);
    b_cancel->set_sensitive(true);
    set_action(USER_NEW);
}

/**\brief Change une user name on database users and public.users table.
 *
 * This is called from the a_usuario_cambiar_nombre_password action.*/
void CatalogoGui::user_change_name_action()
{
    on_cancel_clicked();
    e_name->set_sensitive(true);
    e_password->set_sensitive(true);
    e_retype_password->set_sensitive(true);
    b_ok->set_sensitive(true);
    b_clean->set_sensitive(true);
    b_cancel->set_sensitive(true);
    set_action(USER_NAME_PASSWORD);
}

/**\brief Only change the password.
 * 
 * * This is called from the a_usuario_cambiar_password action.*/
void CatalogoGui::user_change_password_action()
{
    on_cancel_clicked();
    e_password->set_sensitive(true);
    e_retype_password->set_sensitive(true);
    b_ok->set_sensitive(true);
    b_clean->set_sensitive(true);
    b_cancel->set_sensitive(true);
    set_action(USER_PASSWORD);
}

/**\brief Change the user branch.
 *
 * This is called from the a_usuario_cambiar_sucursal action.*/
void CatalogoGui::user_change_branch_action()
{
    on_cancel_clicked();
    cb_branches->set_sensitive(true);
    load_combo();
    b_ok->set_sensitive(true);
    b_clean->set_sensitive(true);
    b_cancel->set_sensitive(true);
    set_action(USER_BRANCH);
}

/**\brief Delete an user from data base.
 *
 * This is called from the a_usuario_cambiar_sucursal.*/
void CatalogoGui::user_delete_action()
{
    on_cancel_clicked();
    b_ok->set_sensitive(true);
    b_clean->set_sensitive(true);
    b_cancel->set_sensitive(true);
    set_action(USER_DELETE);
}

/*--------------------     Branches Menu events     ----------------------*/
/**\brief Add a new branch to database.
 *
 * This is called from the a_branch_nuevo.*/
void CatalogoGui::branch_new_action()
{
    on_cancel_clicked();
    e_name->set_sensitive(true);
    b_ok->set_sensitive(true);
    b_clean->set_sensitive(true);
    b_cancel->set_sensitive(true);
    set_action(BRANCH_NEW);
}

/**\brief Change branch name on public.sucursales table.
 *
 * This is called from the a_branch_edit action.*/
void CatalogoGui::branch_edit_action()
{
    on_cancel_clicked();
    e_name->set_sensitive(true);
    b_ok->set_sensitive(true);
    b_clean->set_sensitive(true);
    b_cancel->set_sensitive(true);
    set_action(BRANCH_EDIT);
}

/**\brief Delete a branch from db.
 *
 * This is called from the a_branch_eliminar action.*/
void CatalogoGui::branch_delete_action()
{
    on_cancel_clicked();
    b_ok->set_sensitive(true);
    b_clean->set_sensitive(true);
    b_cancel->set_sensitive(true);
    set_action(BRANCH_DELETE);
}
/*--------------------      Help menu events     ----------------------------*/
/**\brief About Dialog.
 *
 * Show the about dialog.*/
void CatalogoGui::help_about_action()
{
    Glib::RefPtr<Gtk::Builder> about_builder =
        Gtk::Builder::create_from_file(UI_FILE_ABOUT);
    Gtk::AboutDialog *about = 0;
    about_builder->get_widget("aboutdialog", about);
    about->run();
}

/*---------------------        Buttons events        ------------------------*/
/**\brief Clean GUI.
 *
 * Set null values to all entries. This is added to an clicked event to
 * b_clean.*/
void CatalogoGui::on_clean_clicked()
{
    e_name->set_text("");
    e_password->set_text("");
    e_retype_password->set_text("");
}

/**\brief Clean GUI and block the interface.
 *
 * Set null values to all entries and block all entries. This is added to
 * an clicked event to b_cancel button.*/
void CatalogoGui::on_cancel_clicked()
{
    on_clean_clicked();
    e_name->set_sensitive(false);
    e_password->set_sensitive(false);
    e_retype_password->set_sensitive(false);
    b_ok->set_sensitive(false);
    b_cancel->set_sensitive(false);
    b_clean->set_sensitive(false);
    cb_branches->set_sensitive(false);
    cb_branches->remove_all();
    set_action(NO_ACTION);
}

/**\brief Save changes on data base.
 *
 * When this is called the information is added to the database and the
 * entries are cleaned and blocked. This function is added to an clicked
 * event to b_ok button */
void CatalogoGui::on_ok_clicked()
{
    bool succes;
    switch(get_action()) {
    case ADMIN_PASSWORD:
        succes = change_admin_password();
        break;

    case CAPTURE_DAYS:
        succes = change_days_captures();
        
        break;

    case USER_NEW:
        succes = new_user();
        break;

    case USER_NAME_PASSWORD:
        succes = change_user_and_password();
        break;

    case USER_PASSWORD:
        succes = change_user_password();
        break;

    case USER_BRANCH:
        succes = change_user_branch();
        break;

    case USER_DELETE:
        succes = delete_user();
        break;

    case BRANCH_NEW:
        succes = new_branch();
        break;

    case BRANCH_EDIT:
        succes = edit_branch();
        break;

    case BRANCH_DELETE:
        succes = delete_branch();
    }
    if (succes)
        on_cancel_clicked();
}

bool CatalogoGui::quit(GdkEventAny* event)
{
    Gtk::MessageDialog dialog(*window,
            _("Do you really want to quit?"),
            false,
            Gtk::MESSAGE_QUESTION, Gtk::BUTTONS_YES_NO,
            false);
    if(dialog.run() == Gtk::RESPONSE_YES)
        return false;
    else
        return true;
}

/**\brief Sets the type of action.
 *
 * Set an action from the enumeration Actions.*/
void CatalogoGui::set_action(Actions a)
{
    this->action = a;
}

/**\brief Gets the type of action.
 *
 * Get an action from the enumeration Actions.*/
Actions CatalogoGui::get_action()
{
    return action;
}

/*---------------------------------------------------------------------------*
 *                                                                           *
 *                          Operatives functions                             *
 *                          Case of uses.                                    *
 *                                                                           *
 *---------------------------------------------------------------------------*/
/**\brief Change the admin password.
 * 
 * Change the user password of the administrador_plan user.
 * \return true if succes, otherwise false.*/
bool CatalogoGui::change_admin_password()
{
    if (e_password->get_text() == e_retype_password->get_text() &&
            e_password->get_text() != "" ) {
        std::string query(
                "ALTER USER administrador_plan WITH ENCRYPTED PASSWORD '" +
                e_password->get_text().raw() + "';");
        if(conn->ejecutaQuery(query)) {
            Gtk::MessageDialog msg(*window,
                    _("The application is going to close"), false,
                    Gtk::MESSAGE_WARNING);
            msg.set_secondary_text(_("You need to re-login."));
            msg.run();
            file_exit_action();
        }
        else {
            Gtk::MessageDialog error(*window, conn->getLastError());
            error.set_secondary_text(
                    _("The operation is going to be rollback."));
            error.run();
            return false;
        }
    }
    else {
        std::cerr << _("Passwords do not match.") 
            << _("Both passwords need to be the same.") << std::endl;
        Gtk::MessageDialog error(*window, _("Passwords do not match."));
        error.set_secondary_text(_("Both passwords need to be the same."));
        error.run();
        return false;
    }
    return true;
}

/**\brief change capture days.
 *
 * Set a new number of days avalibles to capture on the data base for
 * normal users.
 * \return true if succes, otherwise false.*/
bool CatalogoGui::change_days_captures()
{
    std::string query("UPDATE metas.diascaptura SET dias = " + 
            e_name->get_text().raw() + " ;");
    if(!conn->ejecutaQuery(query)) {
        Gtk::MessageDialog error(*window, conn->getLastError());
        error.set_secondary_text(
                _("The operation is going to be rollback."));
        error.run();
        return false;
    }
    return true;
}

/**\brief Adds a user.
 *
 * Set a new user for the data base.
 * \return true if succes, otherwise false.*/
bool CatalogoGui::new_user()
{
    Glib::ustring name(e_name->get_text());
    Glib::ustring password(e_password->get_text());
    Glib::ustring error("");
    if (name.empty() || password.empty())
        error = _("The name and password must be specified. ");
    if (password != e_retype_password->get_text())
        error += _("Passwords do not match.");
    /* If there are problems on entries. */
    if (!error.empty()) {
        Gtk::MessageDialog e_ms(
                *window, error, false, Gtk::MESSAGE_ERROR);
        e_ms.set_secondary_text(
                _("The operation is going to be rollback."));
        e_ms.run();
        return false;
    }
    /*...But if anything looks good.*/
    try {
        Sucursal *suc = 
            new Sucursal(cb_branches->get_active_text().raw(), conn);
        Usuario user = 
            Usuario::crear(&name.raw(), suc, &password.raw(), conn);
        load_tree();
    }
    catch(std::string message) {
        Gtk::MessageDialog error(*window, message, false, Gtk::MESSAGE_ERROR);
        error.set_secondary_text(_("The operation is going to be rollback."));
        error.run();
        return false;
    }
    catch(std::exception &e) {
        Gtk::MessageDialog error(*window,
                _("The operation is going to be rollback."), false,
                Gtk::MESSAGE_ERROR);
        error.set_secondary_text(e.what());
        error.run();
        return false;
    }
    return true;
}

/**\brief Change user name and password.
 *
 * Set a new name and set a new password on database.
 * \return true if succes, otherwise false.*/
bool CatalogoGui::change_user_and_password()
{
    Glib::RefPtr<Gtk::TreeSelection> sel = arbol->get_selection();
    Gtk::TreeModel::iterator iter = sel->get_selected();
    std::string errors("");
    if(e_password->get_text() == e_retype_password->get_text())
        if(iter) {
            Gtk::TreeModel::Row row = *iter;
            Glib::ustring uuser = row[my_model.name];
            std::string user = uuser.raw();
            try {
                Usuario p_user(&user, conn);
                p_user.cambiarNombre(&e_name->get_text().raw(),
                        &e_password->get_text().raw(), conn);
                load_tree();
            } catch (std::string mistake) {
                errors = mistake;
            }
        }
        else
            errors += _("There is not selected user.");
    else
        errors += _("Passwords do not match.");
    
    if(errors != "") {
        Gtk::MessageDialog dialog_error(
                *window,
                _("Can't change name and password"), false,
                Gtk::MESSAGE_ERROR);
        dialog_error.set_secondary_text(errors);
        dialog_error.run();
        return false;
    }
    return true;
}

/**\brief Like change_user_and_password but only password.
 *
 * Change the password that a user uses to connect to database.
 * \return true if succes, otherwise false.*/
bool CatalogoGui::change_user_password()
{
    Glib::RefPtr<Gtk::TreeSelection> sel = arbol->get_selection();
    Gtk::TreeModel::iterator iter = sel->get_selected();
    std::string errors("");
    if(e_password->get_text() == e_retype_password->get_text())
        if(iter)
        {
            Gtk::TreeModel::Row row = *iter;
            Glib::ustring user = row[my_model.name];
            try {
                Usuario usr(&user.raw(), conn);
              //  usr.cambiarPassword(&e_password->get_text().raw(), conn);
            }
            catch (std::string mistakes) {
                errors = mistakes;
            }
        }
        else
            errors += _("There is not selected user.");
    else
        errors += _("Passwords do not match.");
    if(errors != "") {
        Gtk::MessageDialog dialog_error(*window,
                _("Can't change password"), false, Gtk::MESSAGE_ERROR);
        dialog_error.set_secondary_text(errors);
        dialog_error.run();
        return false;
    }
    return true;
}

/**\brief Delete a user.
 *
 * Get information for the gui and delete the selected user on the tree.
 * \return true if succes, otherwise false.*/
bool CatalogoGui::delete_user()
{
    Glib::RefPtr<Gtk::TreeSelection> sel = arbol->get_selection();
    Gtk::TreeModel::iterator iter = sel->get_selected();
    std::string errors("");
    if(iter) {
        Gtk::TreeModel::Row row = *iter;
        Glib::ustring user = row[my_model.name];
        try {
            Usuario usr(&user.raw(), conn);
            if (usr.getNombre().length()) {
                usr.eliminar(conn);
                load_tree();
            }
            else
                errors = _("There is not users with this name");
        }
        catch (std::string mistakes) {
            errors = mistakes;
        }
    }
    else
        errors += _("There is not selected user.");
    if(errors != "") {
        Gtk::MessageDialog dialog_error(*window,
                _("Can't delete user."), false, Gtk::MESSAGE_ERROR);
        dialog_error.set_secondary_text(errors);
        dialog_error.run();
        return false;
    }
    return true;
}

/**\brief Change the user branch.
 *
 * Set a new branch for the selected user.
 * \return true if succes, otherwise false.*/
bool CatalogoGui::change_user_branch()
{
    Glib::RefPtr<Gtk::TreeSelection> sel = arbol->get_selection();
    Gtk::TreeModel::iterator iter = sel->get_selected();
    std::string errors("");
    if(iter) {
        Gtk::TreeModel::Row row = *iter;
        Glib::ustring user = row[my_model.name];
        try {
            Usuario usr(&user.raw(), conn);
            usr.cambiarSucursal(
                    new Sucursal(cb_branches->get_active_text().raw(), conn),
                    conn);
        }
        catch(std::string mistake) {
            errors = mistake;
        }
    }
    else
        errors += _("There is not selected user.");
    if(errors != "") {
        Gtk::MessageDialog dialog_error(*window,
                _("Can't change user branch"), false, Gtk::MESSAGE_ERROR);
        dialog_error.set_secondary_text(errors);
        dialog_error.run();
        return false;
    }
    load_tree();
    return true;
}

/**\brief Adds a new branch.
 *
 * On database adds a branch for statistics and user.
 * \return true if succes, otherwise false.*/
bool CatalogoGui::new_branch()
{
    Glib::ustring name(e_name->get_text());
    Glib::ustring error("");
    if (name.empty())
        error = _("The name must be specified. ");
    /* If there are problems on entries. */
    if (!error.empty()) {
        Gtk::MessageDialog e_ms(
                *window, error, false, Gtk::MESSAGE_ERROR);
        e_ms.set_secondary_text(
                _("The operation is going to be rollback."));
        e_ms.run();
        return false;
    }
    /*...But if anything looks good.*/
    try {
        Sucursal suc = Sucursal::crear(name.raw(), conn);
        load_tree();
    }
    catch(std::string message) {
        Gtk::MessageDialog error(*window, message, false, Gtk::MESSAGE_ERROR);
        error.set_secondary_text(_("The operation is going to be rollback."));
        error.run();
        return false;
    }
    catch(std::exception &e) {
        Gtk::MessageDialog error(*window,
                _("The operation is going to be rollback."), false,
                Gtk::MESSAGE_ERROR);
        error.set_secondary_text(e.what());
        error.run();
        return false;
    }
    return true;
}

/**\brief Edit branch name.
 *
 * Set a new name for a branch.* \return true if succes, otherwise false.*/
bool CatalogoGui::edit_branch()
{
    Glib::RefPtr<Gtk::TreeSelection> sel = arbol->get_selection();
    Gtk::TreeModel::iterator iter = sel->get_selected();
    std::string errors("");
    if(iter)
    {
        Gtk::TreeModel::Row row = *iter;
        Glib::ustring branch = row[my_model.name];
        try {
            Sucursal suc(branch.raw(), conn);
            suc.cambiarNombre(e_name->get_text().raw(), conn);
            load_tree();
        }
        catch (std::string mistakes) {
            errors = mistakes;
        }
    }
    else
        errors += _("There is not selected branch.");
    if(errors != "") {
        Gtk::MessageDialog dialog_error(*window,
                _("Can't change branch name."), false, Gtk::MESSAGE_ERROR);
        dialog_error.set_secondary_text(errors);
        dialog_error.run();
        return false;
    }
    return true;
}

/**\brief Delete a branch.
 *
 * Delete a branch from data base.
 * \return true if succes, otherwise false.*/
bool CatalogoGui::delete_branch()
{
    Glib::RefPtr<Gtk::TreeSelection> sel = arbol->get_selection();
    Gtk::TreeModel::iterator iter = sel->get_selected();
    std::string errors("");
    if(iter) {
        Gtk::TreeModel::Row row = *iter;
        Glib::ustring branch_name = row[my_model.name];
        try {
            Sucursal branch(branch_name.raw(), conn);
            if (branch.getNombre().length()) {
                branch.eliminar(conn);
                load_tree();
            }
            else
                errors = _("There is not branches with this name.");
        }
        catch (std::string mistakes) {
            errors = mistakes;
        }
    }
    else
        errors += _("There is not selected branch.");
    if(errors != "") {
        Gtk::MessageDialog dialog_error(*window,
                _("Can't delete branch."), false, Gtk::MESSAGE_ERROR);
        dialog_error.set_secondary_text(errors);
        dialog_error.run();
        return false;
    }
    return true;
}
} // namespace catalogos end.
