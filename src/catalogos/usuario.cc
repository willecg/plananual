/*
 * plananual: usuario.cc
 * Usuario Class: Implementation.
 * Represents an object called Usuario. That is a user, by this, you can
 * modify the name, delete it from the data base and create a new by the
 * "crear" static function.
 *
 * Copyright (C) 2014 William Ernesto Cárdenas Gomez
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif //-HAVE_CONFIG_H

#include "usuario.h"

/**\brief Catálogos de usuarios y sucursales.
 *
 * Los catálogos son los elementos que pueden ser manipulados de forma mas o
 * menos regular en la base de datos. En este caso son los usuarios y las
 * sucursales. */
namespace catalogos
{
/*---------------------------------------------------------------------------*
 *                                                                           *
 *                      Constructores y destructor                           *
 *                                                                           *
 *---------------------------------------------------------------------------*/
/**\briefConstructor default.
 *
 * PELIGRO Los valores iniciales son basura y no existe un
 * correpondiente en la base de datos */
Usuario::Usuario()
{
    std::string nula("");
    setID(0);
    setNombre(&nula);
    setPassword(&nula);
    Sucursal suc;
    setSucursal(&suc);
}

/**\brief Constructor estándar con ID.
 *
 * Construye el objeto a partir del id del usuario, busca en la base de datos y
 * llena el resto de los campos con la información recuperada.
 * @param "const int* const id" ID de usuario busqueda a partir de este dato.
 * @param "BaseDatos* const conexion" conexion: Enlace con la base de datos. */
Usuario::Usuario(const int* const id, BaseDatos* const conexion)
{
    std::string query_usuario(
                "SELECT usuario, sucursal \
                FROM public.usuarios \
                WHERE id_usuario = " + std::to_string(*id) + ";");
    if(!conexion->ejecutaQuery(query_usuario))
        throw conexion->getLastError();
    if(conexion->nRegistros() <= 0) {
        std::string fails(_("There is not users with this id."));
        throw fails;
    }
    
    // se obtienen los datos a pasar
    Sucursal suc(stoi(conexion->regresaDato(0, 1)), conexion); 
    std::string nm = conexion->regresaDato(0, 0);
    // captura
    setID(id);
    setNombre(&nm);
    setSucursal(&suc);
}

/**\brief Constructor estándar con nombre.
 *
 * Construye el objeto a partir del nombre del usuario, busca en la base de
 * datos y llena el resto de los campos con la información recuperada.
 * @param "const std::string* const nombre" ID de usuario busqueda a partir de
 * este dato.
 * @param "BaseDatos * conexion" Enlace con la base de datos. */
Usuario::Usuario(
        const std::string* const nombre, BaseDatos* conexion)
{
    std::string query(
"SELECT id_usuario, sucursal \
FROM public.usuarios \
WHERE usuario = '" + *nombre + "';");
    if(!conexion->ejecutaQuery(query))
        throw conexion->getLastError();

    if(conexion->nRegistros() <= 0) {
        std::string fails(_("There is not users with this name."));
        throw fails;
    }
    // se obtienen los datos para capturar
    int us_id = stoi(conexion->regresaDato(0, 0));
    Sucursal suc(stoi(conexion->regresaDato(0, 1)), conexion); 
    // captura
    setID(&us_id);
    setNombre(nombre);
    setSucursal(&suc);
    std::string nula("");
    setPassword(&nula);
}

/**\brief Destructor.
 *
 * Destructor del objeto.*/
Usuario::~Usuario()
{}

/*---------------------------------------------------------------------------*
 *                                                                           *
 *                           Funciones set y get                             *
 *                                                                           *
 *---------------------------------------------------------------------------*/
/**\brief Establece el ID del usuario.
 *
 * Solo la misma clase lo puede aplicar
 * debido a que la información se obtiene de la base de datos.
 * @param "const int *const id" Id a asignar */
void Usuario::setID(const int* const id)
{
    this->id = *id;
}

/**\brief Establece el nombre del usuario.
 *
 * Solo la misma clase lo puede aplicar debido
 * a que la información se obtiene de la base de datos.
 * @param "const std::string *const nombre" nombre a asignar */
void Usuario::setNombre(const std::string* const nombre)
{
    this->nombre = *nombre;
}

/**\briefEstablece el password del usuario.
 *
 * Solo es util cuando se crea el usuario,
 * inmediatamente después su información es desechable ya que queda almacenada
 * en el gestor de base de datos.
 * @param "const std::string *const password" contraseña a asignar */
void Usuario::setPassword(const std::string* const password)
{
    this->password = *password;
}

/**\brief Establece la sucursal del usuario.
 *
 * Solo accesible desde la base de datos ya
 * que se llena de información almacenada en el gestor de base de datos.
 * @param "const Sucursal *const sucursal" objeto de tipo Sucursal a asignar */
void Usuario::setSucursal(const Sucursal* const sucursal)
{
    this->sucursal = *sucursal;
}

/**\brief Obtiene el id del usuario.
 *
 * Obtiene el id que esta almacenado en la base de datos o que se esta
 * trabajando.
 * @return ID del usuario */
int Usuario::getID()
{
    return this->id;
}

/**\brief Obtiene el nombre del usuario.
 *
 * Obtiene el nombre que se almacena en la base de datos o que se esté
 * trabajando en ese momento.
 * @return Nombre del usuario */
std::string Usuario::getNombre()
{
    return this->nombre;
}

/**\brief Obtiene el password del usuario.
 *
 * NO obtiene la información almacenada en la
 * base de datos, por lo que solo es util durante la creación de un nuevo
 * usuario.
 * @return Password del usuario */
std::string Usuario::getPassword()
{
    return this->password;
}

/**\brief Obtiene el objeto sucursal del usuario.
 *
 * Obtiene la sucursal del usuario que esta almacenada en la base de datos.
 * @return Objeto sucursal. */
Sucursal Usuario::getSucursal()
{
    return this->sucursal;
}

/*---------------------------------------------------------------------------*
 *                                                                           *
 *                   Funciones operativas (Casos de uso)                     *
 *                                                                           *
 *---------------------------------------------------------------------------*/
/**\brief Crear un nuevo usuario.
 *
 * Crea una nueva entrada en la tabla de usuarios de la base de datos, por
 * medio de la función de base de datos public.crearUsuario. Si no existen
 * problemas durante la creación del usuario se lee el usuario con los nuevos
 * datos y se regresa como valor de retorno ese objeto Usuario creado. En caso
 * de error se envía una excepción de tipo std::string.
 *
 * @param "const std::string *const nombre" Nombre del nuevo usuario
 * @param "catalogos::Sucursal *const sucursal" Objeto sucursal al que
 * pertenece.
 * @param "const std::string *const password" Contraseña de acceso.
 * @param "BaseDatos *const conexion" Conexión a la base de datos.
 *
 * @return El nuevo usuario creado en la base de datos. */
Usuario Usuario::crear(
            const std::string* const nombre,
            Sucursal* const sucursal,
            const std::string* const password,
            BaseDatos* const conexion)
{
    std::string query("SELECT public.crearUsuario('"
            + *nombre + "', '"
            + *password + "','"
            + sucursal->getNombre()+"');");
    if(!conexion->ejecutaQuery(query))
        throw conexion->getLastError();
    return Usuario(nombre, conexion);
}

/**\brief Modifica la sucursal en la base de datos.
 *
 * Modifica un registro para la sucursal en la base de datos por medio
 * de la función de base de datos public.cambiarDeSucursal. En caso de que el
 * cambio sea exitoso se modifica el campo sucursal del objeto tipo Usuario
 * por el nuevo valor del objeto tipo Sucursal. En caso de error se genera una
 * excepción de tipo std::string.
 *
 * @param "Sucursal *const sucursal" Nueva sucursal a asignar al usuario.
 * @param "BaseDatos *const conexion" Conexión con la base de datos. */
void Usuario::cambiarSucursal(
            Sucursal* const sucursal,
            BaseDatos* const conexion)
{
    std::string query("SELECT public.cambiarDeSucursal('"
            + getNombre() + "','"
            + sucursal->getNombre() + "');");
    if(!conexion->ejecutaQuery(query))
        throw conexion->getLastError();
    setSucursal(sucursal);
}

/**\breif Cambia nombre y contraseña del usuario en la base de datos.
 *
 * Establece nuevos nombre y contraseña en la base de datos por medio
 * de la función de base de datos public.modificarNombreUsuario. En caso de
 * que el cambio sea exitoso se modifica el campo usuario del objeto, y la
 * contraseña. En caso de error se genera una excepción de tipo std::string.
 *
 * @param "const std::string *const nombre" Nuevo nombre del usuario.
 * @param "const std::string *const password" Nueva contraseña del usuario.
 * @param "BaseDatos *const conexion" Conexión con la base de datos. */
void Usuario::cambiarNombre(
            const std::string* const nombre,
            const std::string* const password,
            BaseDatos* const conexion)
{
    std::string query("SELECT public.modificarNombreUsuario('"
            + getNombre() + "','"
            + *nombre + "','"
            + *password + "');");
    if(!conexion->ejecutaQuery(query))
        throw conexion->getLastError();
    setNombre(nombre);
    setPassword(password);
}

/**\brief Cambio de contraseña.
 *
 * Establece nueva contraseña en la base de datos por medio de la función de
 * base de datos public.cambiarPassword. En caso de que el cambio sea exitoso 
 * se modifica la contraseña del usuario del objeto. En caso de error se
 * genera una excepción de tipo std::string.
 *
 * @param "const std::string *const password" Nueva contraseña del usuario.
 * @param "BaseDatos *const conexion" Conexión con la base de datos. */
void Usuario::cambiarPassword(
            const std::string* const password,
            BaseDatos* const conexion)
{
    std::string query("SELECT public.cambiarPassword('"
            + getNombre() + "','"
            + *password + "');");
    if(!conexion->ejecutaQuery(query))
        throw conexion->getLastError();
    setPassword(password);
}

/**\brief Borrar al usuario de la base de datos.
 *
 * Elimina el usuario de las tablas de la base de datos y de los permisos para
 * entrar nuevamente a la base de datos. En caso de error se genera una
 * excepción de tipo std::string. Si todo funciona correctamente se se vuelven
 * a agregar datos basura a los miembros del objeto.
 *
 * @param "BaseDatos *const conexion" Conexión con la base de datos. */
void Usuario::eliminar(BaseDatos* const conexion)
{
    std::string query("SELECT public.borrarUsuario('" + getNombre() + "');");
    if(!conexion->ejecutaQuery(query))
        throw conexion->getLastError();
    const std::string cadena_vacia("");
    const int cero = 0;
    setPassword(&cadena_vacia);
    setNombre(&cadena_vacia);
    Sucursal suc;
    setSucursal(&suc);
    setID(&cero);
}

} // Namespace catalogos
