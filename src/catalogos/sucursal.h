/*
 * plananual: sucursal.h
 * Sucursal Class: Declaration.
 * Represents an object called Sucursal. That is a branch (Sucursal is branch
 * in spanish) in the cooperative, by this, you can modify the name, delete it
 * from the data base and create a new by the "crear" static function.
 *
 * Copyright (C) 2014 William Ernesto Cárdenas Gomez
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef HAVE_SUCURSAL_H
#define HAVE_SUCURSAL_H

#include <string>

#include "../../lib/baseDatos.h"
using namespace db;

namespace catalogos
{
/**\brief Manipulación de objetos Sucursal.
 *
 * Por medio de los objetos de tipo sucursal se pueden crear, eliminar o
 * modificar los registros de sucursales en la base de datos. */
class Sucursal{
/*---------------------------------------------------------------------------*
 *                                                                           *
 *                          Class's functions                                *
 *                                                                           *
 *---------------------------------------------------------------------------*/
public:
    /* constructors */
    Sucursal(); // Default constructor WARNING use carefully.
    Sucursal(const int, BaseDatos *); // Constructor by the branch id
    Sucursal(std::string, BaseDatos *); // Constructor by en the name
    virtual ~Sucursal(); // Destructor

private:
    /* set & get functions */
    void setNombre(std::string); // set name to the object
    void setID(int);             // set branch object ID

public:
    int getID(); // get the id 
    std::string getNombre(); // get the name

    /* Operations */
    static Sucursal crear(const std::string, BaseDatos *); // Create an
                                                    // entrance in db

    void cambiarNombre(std::string, BaseDatos*); // this change the branch
                                                  // name
    void eliminar(BaseDatos *); // delete the object from de db and
                                // destroys itself.
    
/*-----------------------------------------------------------------------*
 *                                                                       *
 *                      Class's atributes                                *
 *                                                                       *
 *-----------------------------------------------------------------------*/
private:
    int id; // in the data base is the primary key of the row
    std::string nombre; // name of the branch
}; // class

}; // Namespace catalogos
#endif //-HAVE_SUCURSAL_H
