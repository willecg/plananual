/*
 * plancatalogos: catalogos.cc
 * Control de catalogos.
 *
 * Copyright © 2014 William Ernesto Cárdenas Gomez
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <iostream>
#include <glibmm/i18n.h>

#include "catalogos.h"
#include "../plan.h"
#include "../../lib/baseDatos.h"

int main(int argc, char *argv[])
{
    /* Internationalization */
    bindtextdomain(GETTEXT_PACKAGE, PROGRAMNAME_LOCALEDIR);
    bind_textdomain_codeset(GETTEXT_PACKAGE, "UTF-8");
    textdomain(GETTEXT_PACKAGE);
  
    std::string usuario, password;
    plan::connection_data data = plan::load_struct();
    if(plan::perform_login(new login::Login(plan::log_in, argc, argv), &data)){
        int error;
        db::BaseDatos *con = new db::BaseDatos
            (data.user, data.password, data.host, data.data_base, data.port);
        catalogos::CatalogoGui cat(con);
        try {
            error = cat.open_window(argc, argv);
        }
        catch (Glib::Error &e) {
            std::cerr << e.what() << std::endl;
        }
       return error;
    }
    
    return 0;
} // fin de main
