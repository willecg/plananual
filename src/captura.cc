// captura.cc
//
// Copyright (C) 2014 - William Ernesto Cárdenas Gómez
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <iostream>
#include <glibmm/i18n.h>
#include <gtkmm/builder.h>
#include <gtkmm/application.h>

#include <glibmm/fileutils.h>
#include <glibmm/markup.h>

#include "window_capture.h"
#include "plan.h"
#include "catalogos/usuario.h"
#include "../lib/baseDatos.h"

#define UI_CAPTURE PACKAGE_DATA_DIR"/ui/captura.glade"
//#define UI_CAPTURE "gui/captura.glade"

int main(int argc, char *argv[])
{
    /* Internationalization */
    bindtextdomain(GETTEXT_PACKAGE, PROGRAMNAME_LOCALEDIR);
    bind_textdomain_codeset(GETTEXT_PACKAGE, "UTF-8");
    textdomain(GETTEXT_PACKAGE);

    int error = 0;
    std::string user, password;
    plan::connection_data data = plan::load_struct();
    if(plan::perform_login(new login::Login(plan::log_in, argc, argv), &data)){
        db::BaseDatos *conn = new db::BaseDatos
            (data.user, data.password, data.host, data.data_base, data.port);
        Glib::RefPtr<Gtk::Application> app =
            Gtk::Application::create(argc, argv, "cpaa.plan.captura");
        Glib::RefPtr<Gtk::Builder> builder = Gtk::Builder::create();
        try{
            builder->add_from_file(UI_CAPTURE);
        }
        catch(const Glib::FileError& e){
            std::cerr << _("File error: ") << e.what() << std::endl;
            ++error;
        }
        catch(const Glib::MarkupError& e){
            std::cerr << _("Markup error: ") << e.what() << std::endl;
            ++error;
        }
        catch(const Gtk::BuilderError& e){
            std::cerr << _("Builder error: ") << e.what() << std::endl;
            ++error;
        }
        if(!error){
            capture::WindowCapture *win = 0;
            builder->get_widget_derived("main_win", win);
            win->set_user(new catalogos::Usuario(&data.user, conn));
            win->set_conn(conn);
            win->prepare();
            app->run(*win);
        }
    }
    else
        ++error;

    return error;
}
