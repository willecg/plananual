/*
 * planadmin: administracion.cc
 * Combo box that store branches name.
 *
 * Copyright © 2014 William Ernesto Cárdenas Gomez
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "combo_branches.h"

/**\brief Namespace admin for the admin operations.
 *
 * Declares the gui for the admin operations of the plan proyect. May be on
 * follow versions will be integrated in plan namespace.*/ 
namespace admin{
/**\brief Constructor for get_widget_derived.
 * 
 * Only creates the object but does not put information in it.
 * This is only usable by a Gtk::Builder object with its method
 * get_widget_derived.
 * \param "BaseObjectType *cobject"
 * \param "const Glib::RefPtr<Gtk::Builder> builder" Gtk::Builder object.*/
ComboBoxBranches::ComboBoxBranches(BaseObjectType *cobject,
            const Glib::RefPtr<Gtk::Builder> builder):
    Gtk::ComboBoxText(cobject)
{
    set_status(false);
}

/**Destructor.*/
ComboBoxBranches::~ComboBoxBranches()
{}

/**\brief Set the status.
 *
 * If there are database connection status is going tu be true. Elsewere
 * false.
 * \param "bool status_value" True for connection false for no connection*/
void ComboBoxBranches::set_status(bool status_value)
{
    status = status_value;
}

/**\brief Get the status.
 *
 * If there are database connection status is going tu be true. Elsewere
 * false.
 * \return status.*/
bool ComboBoxBranches::get_status()
{
    return status;
}

/**\brief Set the branch_list and the branch_index.
 *
 * Uses the connection to read all branches and stores it on the vectos.
 * \param "db::BaseDatos *conn" Connection to database.
 * \return True if success elsewere false.*/
bool ComboBoxBranches::set_branches(db::BaseDatos *conn)
{
    set_status(false);
    if(conn->ejecutaQuery(query_branches)) {
        for(int i = 0; i < conn->nRegistros(); i++) {
            branch_list.push_back(conn->regresaDato(i, 1));
            branch_index.push_back(conn->regresaDato(i, 0));
        }
        set_status(true);
    }
    return get_status();
}

/**\brief Fill the combo box.
 * 
 * With information stored on branch_list.*/
void ComboBoxBranches::fill_combo()
{
    for(size_t i = 0; i < branch_list.size(); i++)
        append(branch_list.at(i));
    set_active_text(branch_list.at(0));
}
} // Namespace end
