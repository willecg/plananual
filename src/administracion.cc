/*
 * planadmin: administracion.cc
 * Control de las estadisticas.
 *
 * Copyright © 2014 William Ernesto Cárdenas Gomez
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <iostream>
#include <glibmm/i18n.h>

#include "../lib/baseDatos.h"
#include "plan.h"
#include "administracion_gui.h"

int main(int argc, char *argv[])
{
    /* Internationalization */
    bindtextdomain(GETTEXT_PACKAGE, PROGRAMNAME_LOCALEDIR);
    bind_textdomain_codeset(GETTEXT_PACKAGE, "UTF-8");
    textdomain(GETTEXT_PACKAGE);

    int error = 0;
    std::string user, password;
    plan::connection_data data = plan::load_struct();

    if(perform_login(new login::Login(plan::log_in, argc, argv), &data)) {
        db::BaseDatos *conn = new db::BaseDatos
            (data.user, data.password, data.host, data.data_base, data.port);
        admin::Admin_gui gui(conn);
        try{
            error = gui.open_window(argc, argv);
        }
        catch(Glib::Error &e){
            std::cerr << e.what() << std::endl;
        }
    }
    else {
        ++error;
    }
    return error;
}
