/*
 * plananual: estadistica_reales.h
 * Definición de la clase "EstadisticaReales"
 * Clase que representa las estadisticas generadas y seguidas por las
 * sucursales durante su operación cada año. Por medio de los objetos de esta
 * clase se pueden crear nuevos registros en la base de datos, borrarlo y
 * cambiar la información. Así como registrar las operaciones anuales.
 * Solo se relaciona con las operaciones que tienen que ver con las operaciones
 * con números reales.
 *
 * Copyright © 2014 William Ernesto Cárdenas Gomez
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef HAVE_ESTADISTICA_REALES_H
#define HAVE_ESTADISTICA_REALES_H

#include "estadistica.h"

namespace estadisticas
{
/**\brief Estadisticas con números reales.
 *
 * Clase encargada de realizar las operaciones de captura, eliminación y
 * modificaciones de los registros en la base de datos que tienen que ser sobre
 * numeros reales. Las operaciones de esta clase se realizan sobre el esquema
 * metas de la base de datos.*/
class EstadisticaReales : public Estadistica {
/*---------------------------------------------------------------------------*
 *                                                                           *
 *                      Funciones miembro (Operaciones)                      *
 *                                                                           *
 *---------------------------------------------------------------------------*/
    /* Constructores y destructores */
public:
    EstadisticaReales(); // Constructor default. Valores inexistentes
    EstadisticaReales( // Constructor de trabajo.
            catalogos::Sucursal* const, const int, const int,
            const TipoCuenta, BaseDatos *const);
    virtual ~EstadisticaReales(); // Destructor

    /* Funciones get y set */
private:
    void setPresupuesto(const double);
    void setRealizado(const double);
public:
    double getPresupuesto();
    double getRealizado();

    /* Funciones operativas */
    static EstadisticaReales crear(           // Genera un nuevo objeto y crea
            catalogos::Sucursal *const, // una entrada en la base.
            const int, const int, const double,
            const TipoCuenta, BaseDatos *const);
    void modificarPresupuesto
        (const double, BaseDatos* const);//Modifica la captura en el presupuest
    void modificarRealizado
        (const double, BaseDatos* const); // Modifica lo realizado en el mes.

/*---------------------------------------------------------------------------*
 *                                                                           *
 *                                  Atributos                                *
 *                                                                           *
 *---------------------------------------------------------------------------*/
private:
    double presupuesto; // Presupuesto mensual.
    double realizado; // avance en el presupuesto del mes.
};
}
#endif // HAVE_ESTADISTICAS_REALES
