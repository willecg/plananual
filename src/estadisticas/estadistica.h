/*
 * plananual: estadistica.h
 * Definición de la clase "Estadistica"
 * Clase que representa las estadisticas generadas y seguidas por las
 * sucursales durante su operación cada año. Por medio de los objetos de esta
 * clase se pueden crear nuevos registros en la base de datos, borrarlo y
 * cambiar la información. Así como registrar las operaciones anuales.
 *
 * Copyright © 2014 William Ernesto Cárdenas Gomez
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef HAVE_ESTADISTICAS_H
#define HAVE_ESTADISTICAS_H

#include "../catalogos/sucursal.h"
#include "../../lib/baseDatos.h"
using namespace db;

/* Este espacio de nombres almacena información referente a las estadisticas de
 * los avances de la sucursal*/
namespace estadisticas
{

/**\brief Enumeración TipoCuenta.
 *
 * Por esta se determinan sobre que tabla buscar en la base de datos. */
enum TipoCuenta{
    SOCIOS = 0,           /**<Cuenta de socios adultos, enteros */
    MENORES = 1,          /**<Tipo de cuenta para menores, enteros. */
    AHORROSOCIOS = 2,     /**<El ahorro de socios, decimales.*/
    AHORROMENORES = 3,    /**<El ahorro de menores ahorradores, decimales. */
    DEPOSITOALAVISTA = 4, /**<Ahorro en depósitos a la vista, decimales. */
    PLAZOFIJO = 5,        /**<Ahorro en plazos fijos, decimales. */
    PRESTAMO = 6,         /**<Cantidad otorgada en préstamos, decimales. */
    MOROSIDAD = 7         /**<Porcentaje de morosidad, decimales */
};

/**\brief Tipo de dato especial.
 *
 * Es una cadena constante para los nombres de las cuentas.*/
typedef std::string str_cuenta;

/**\brief Cadenas default.
 *
 * Son las cadenas que se manejan como default para cada una de los tipos de
 * cuenta */
/* Son las cadenas que se manejan como default para cada una de los tipos de
 * cuenta */
const std::string cadenas_cuentas[] =
    {_("Partners"),         _("Children"),              _("Saving partners"),
     _("Saving childrens"), _("Demand deposit"),        _("Fixed deposit"),
     _("Loans"),            _("% of delay on payment")
    };


/**\brief Nombres de las tablas.
 *
 * Nombres de las tablas en la base de datos */
/* Son las cadenas que se manejan como default para cada una de los tipos de
 * cuenta */
const str_cuenta tablas[] = {
    "metas.socios", "metas.menores", "metas.ahorrosocios",
    "metas.ahorromenores", "metas.depositoalavista", "metas.plazofijo",
    "metas.prestamo", "metas.morosidad" };

/* Funciones que realizan intercambios entre TipoCuenta y str_cuenta */
str_cuenta cuenta_cadena(const TipoCuenta); // TipoCuenta a str_cadena
TipoCuenta cadena_cuenta(const str_cuenta); //de str_cadena a TipoCuenta
int cuenta_tabla(const TipoCuenta); // Obtiene el índice de la tabla.

/**\brief Estadisticas.
 *
 * Clase encargada de manipular la información de la base de datos referente a
 * la operación de la aplicación. Por medio de esta clase se heredan
 * directamente las cuentas que trabajaran con la información contenida en la
 * base de datos, dependiendo del tipo de cuenta que se maneje será la tabla
 * que se trabaje también */
/* Es heredable, no esta diseñada para que opere directamente las entradas en
 * la base de datos. */
class Estadistica {
/*---------------------------------------------------------------------------*
 *                                                                           *
 *                           Funciones miembro                               *
 *                                                                           *
 *---------------------------------------------------------------------------*/
public:
    /* Constructores y destructores */
    Estadistica(); // Constructor default datos basura Solo sirve como auxiliar
    Estadistica( // constructor principal
            catalogos::Sucursal* const, 
            const int,
            const int,
            const TipoCuenta, 
            BaseDatos* const);
    virtual ~Estadistica(); // Destructor
    
    /* Funciones set y get */
protected:
    void setSucursal(catalogos::Sucursal* const); // establece sucursal
    void setMes(const int); // establece mes
    void setAnyo(const int); // establece año
public:
    void setTipoCuenta(const TipoCuenta); // Establece el tipo de cuenta

    catalogos::Sucursal getSucursal(); // obtiene la sucursal
    int getMes(); // obtiene el mes
    int getAnyo(); // obtiene el año
    TipoCuenta getTipoCuenta(); // obtiene el Tipo de cuenta

    /* Funciones operativas */
    void deleteEstadistica(BaseDatos* const db);
    void modificarSucursal( // Cambia la sucursal a la que pertenece
            catalogos::Sucursal* const, // esta estadistica
            BaseDatos* const);
    void modificarAnyo(const int, BaseDatos* const); // cambia el año
    void modificarMes(const int, BaseDatos* const); // cambia el mes
/*---------------------------------------------------------------------------*
 *                                                                           *
 *                          Atributos de la clase                            *
 *                                                                           *
 *---------------------------------------------------------------------------*/
private:
    catalogos::Sucursal sucursal; // Sucursal a la que pertenece la estadistica
    int anyo; // Año correspondiente a la captura de la estadistica
    int mes; // Mes correspondiente a la captura de la estadistica
    TipoCuenta tipoCuenta; // El tipo de cuenta que se esta manejando
};
}
#endif // HAVE_ESTADISTICAS_h
