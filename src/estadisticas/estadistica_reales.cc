/*
 * plananual: estadistica_reales.cc
 * Implementación de la clase "EstadisticaReales"
 * Clase que representa las estadisticas generadas y seguidas por las
 * sucursales durante su operación cada año. Por medio de los objetos de esta
 * clase se pueden crear nuevos registros en la base de datos, borrarlo y
 * cambiar la información. Así como registrar las operaciones anuales.
 * Solo se relaciona con las operaciones que tienen que ver con las operaciones
 * con números reales.
 *
 * Copyright © 2014 William Ernesto Cárdenas Gomez
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif // HAVE_CONFIG_H

#include "estadistica_reales.h"
#include <iostream>
namespace estadisticas
{
/*---------------------------------------------------------------------------*
 *                                                                           *
 *                       Constructores y destructores                        *
 *                                                                           *
 *---------------------------------------------------------------------------*/
/**\brief Constructor default.
 *
 * Prepara un objeto con valores basura, no es muy útil ya que la información
 * almacenada en los miembros no tiene par en la base de datos.*/
EstadisticaReales::EstadisticaReales() :
    Estadistica()
{
    setPresupuesto(0);
    setRealizado(0);
}

/**\brief Constructor especifico.
 *
 * Este inicializa los atributos con información obtenida desde la base de
 * datos. Por lo tanto es el mecanismo mas adecuado para realizar la mayoría de
 * operaciones de este tipo de objetos.
 *
 * @param "catalogos::Sucursal * const sucursal" Apuntador a la sucursal.
 * @param "const int anyo" Año de la estadistica a leer.
 * @param "const int mes" Mes de la estadistica a leer.
 * @param "TipoCuenta tipoCuenta" Tipo de cuenta o tabla a leer.
 * @param "BaseDatos * conexion" Conexión a la base de datos.*/
EstadisticaReales::EstadisticaReales(
        catalogos::Sucursal * const sucursal,
        const int anyo,
        const int mes,
        const TipoCuenta tipoCuenta,
        BaseDatos * conexion) :
    Estadistica(sucursal, anyo, mes, tipoCuenta, conexion)
{
    std::string consulta(
            "SELECT presupuesto, realizado FROM " +
            std::string(tablas[cuenta_tabla(tipoCuenta)]) +
            " WHERE sucursal = " + std::to_string(sucursal->getID()) +
            " AND mes = " + std::to_string(mes) + 
            " AND anyo = " + std::to_string(anyo) + ";");
    if(!conexion->ejecutaQuery(consulta))
        throw conexion->getLastError();
    if(conexion->nRegistros() <= 0)
        throw _("There are not statistics avalibles.");
    
    setPresupuesto((conexion->regresaDato(0, 0) != "" ?
                std::stod(conexion->regresaDato(0, 0)): 0.0));
    setRealizado((conexion->regresaDato(0, 1) != "" ?
                std::stod(conexion->regresaDato(0, 1)): 0.0));
}

/**\brief Destructor.*/
EstadisticaReales::~EstadisticaReales()
{}

/**\breif Establece el presupuesto.
 *
 * Dicho dato debe ser leído de la base de datos, o en caso de la construcción
 * default del objeto debe tener valor cero. 
 * @param "const double presupuesto" Presupuesto mensual. */
void EstadisticaReales::setPresupuesto(const double presupuesto)
{
    this->presupuesto = presupuesto;
}

/**\brief Establece lo que se realizó en el mes.
 *
 * Dicho dato debe ser leído de la base de datos, o en caso de la construcción
 * default del objeto debe tener valor cero. 
 * @param "const double realizado" Ingreso obtenido en el mes. */
void EstadisticaReales::setRealizado(const double realizado)
{
    this->realizado = realizado;
}

/**\brief Obtiene el presupuesto.
 *
 * Lee el presupuesto que se esta operando en ese momento.
 * @return Presupuesto del mes.*/
double EstadisticaReales::getPresupuesto()
{
    return presupuesto;
}

/**\brief Obtiene lo realizado.
 *
 * Lee lo realizado que se esta operando en ese momento.
 * @return Realizado durante del mes.*/
double EstadisticaReales::getRealizado()
{
    return realizado;
}

/*---------------------------------------------------------------------------*
 *                                                                           *
 *                           Funciones operativas                            *
 *                                                                           *
 *---------------------------------------------------------------------------*/
/**\brief Crea una estadistica.
 *
 * Crea una nueva entrada en la base de datos sobre las tablas que manejan
 * enteros. En este caso la tabla metas.socios y la tabla metas.menores.
 *
 * @param "catalogos::Sucursal * const sucursal" Apuntador a la sucursal.
 * @param "const int anyo" Año de la nueva estadistica.
 * @param "const int mes" Mes de la nueva estadistica.
 * @param "const double presupuesto" Presupuesto para esta nueva estadistica.
 * @param "const TipoCuenta tipoCuenta" Indica el tipo de cuenta que se va a
 * afectar(Para determinar la tabla que se este objeto va a manejar).
 * @oaram "BaseDatos * const conexion" Conexión a la base de datos.
 *
 * @return Un nuevo objeto de tipo EstadisticaReales. */
EstadisticaReales EstadisticaReales::crear(
            catalogos::Sucursal *const sucursal,
            const int anyo, const int mes, const double presupuesto,
            const TipoCuenta tipoCuenta, BaseDatos *const conexion)
{
    std::string consulta = "SELECT metas.agregarregistro2('" + 
        sucursal->getNombre() + "', " + 
        std::to_string(presupuesto) + ", " + 
        std::to_string(anyo) + ", " +
        std::to_string(mes) + ", " + 
        std::to_string(cuenta_tabla(tipoCuenta)-2) + ");";
    if (!conexion->ejecutaQuery(consulta))
        throw conexion->getLastError();
    EstadisticaReales nuevo(sucursal, anyo, mes, tipoCuenta, conexion);
    return nuevo;
}

/**\brief Modifica lo realizado.
 *
 * Modifica la tabla correspondiente, de forma que se actualicen los valores en
 * los campos ingresos y retiros de la tabla por medio de la función de base de
 * datos captura_socios. Si hay algún problema se genera una excepción de tipo
 * std::string.
 *
 * @param "const double realizado" Nuevo número de realizado.
 * @param "BaseDatos * const conexion" Conexión a la base de datos. */
void EstadisticaReales::modificarRealizado(
        const double realizado, BaseDatos * const conexion)
{
    std::string consulta = 
        "SELECT metas.capturar2('" + 
        getSucursal().getNombre() + "', " + 
        std::to_string(realizado) + ", " +
        std::to_string(getAnyo()) + ", " + 
        std::to_string(getMes()) + ", " + 
        std::to_string(cuenta_tabla(getTipoCuenta())-2) + ");";
    if (!conexion->ejecutaQuery(consulta))
        throw conexion->getLastError();
    setRealizado(realizado);
}

/**\brief Modifica el presupuesto.
 *
 * Cambia el valor original del presupuesto de la clase, y al mismo tiempo
 * cambia el valor del presupuesto en la tabla correspondiente en la base de
 * datos. En caso de que exista algún problema se genera una exepción de tipo
 * std::string.
 *
 * @param "const double presupuesto" Nuevo valor de presupuesto.
 * @param "BaseDatos * const conexion" Conexión a la base de datos. */
void EstadisticaReales::modificarPresupuesto(
        const double presupuesto, BaseDatos *const conexion)
{
    std::string consulta =
        "SELECT metas.modificar_presupuesto2('" + 
        getSucursal().getNombre() + "', " + 
        std::to_string(presupuesto) + ", " + 
        std::to_string(getAnyo()) + ", " + 
        std::to_string(getMes()) + ", " + 
        std::to_string(cuenta_tabla(getTipoCuenta())-2) + ");";
    if (!conexion->ejecutaQuery(consulta))
        throw conexion->getLastError();
    setPresupuesto(presupuesto);
}

}
