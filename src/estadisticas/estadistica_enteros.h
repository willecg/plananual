/*
 * plananual: estadistica_enteros.h
 * Definición de la clase "EstadisticaEnteros"
 * Clase que representa las estadisticas generadas y seguidas por las
 * sucursales durante su operación cada año. Por medio de los objetos de esta
 * clase se pueden crear nuevos registros en la base de datos, borrarlo y
 * cambiar la información. Así como registrar las operaciones anuales. Solo se
 * relacionan con las operaciones con números enteros.
 *
 * Copyright © 2014 William Ernesto Cárdenas Gomez
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef HAVE_ESTADISTICA_ENTEROS_H
#define HAVE_ESTADISTICA_ENTEROS_H

#include "estadistica.h"

namespace estadisticas
{
/**\brief Estadisticas con números enteros.
 *
 * Clase encargada de realizar las operaciones de captura, eliminación y
 * modificaciones de los registros en la base de datos que tienen que ser sobre
 * numeros reales. Las operaciones de esta clase se realizan sobre el esquema
 * metas de la base de datos.*/
class EstadisticaEnteros : public Estadistica {
/*---------------------------------------------------------------------------*
 *                                                                           *
 *                      Funciones miembro (Operaciones)                      *
 *                                                                           *
 *---------------------------------------------------------------------------*/
    /* Constructores y destructores */
public:
    EstadisticaEnteros(); // Constructor default. Valores inexistentes
    EstadisticaEnteros( // Constructor de trabajo.
            catalogos::Sucursal* const, const int,
            const int, const TipoCuenta, BaseDatos *const);
    virtual ~EstadisticaEnteros(); // Destructor

    /* Funciones get y set */
private:
    void setPresupuesto(const int);
    void setIngresos(const int);
    void setRetiros(const int);
public:
    int getPresupuesto();
    int getIngresos();
    int getRetiros();
    int getRealizado();

    /* Funciones operativas */
    static EstadisticaEnteros crear(          // Genera un nuevo objeto y crea
            catalogos::Sucursal *const, // una entrada en la base.
            const int, const int, const int,
            const TipoCuenta, BaseDatos *const);
    void modificarRealizado(
            const int, const int, BaseDatos *const); // Modifica realizado.
    void modificarPresupuesto(
            const int, BaseDatos *const);// Modificar presupuesto.
 
/*---------------------------------------------------------------------------*
 *                                                                           *
 *                                  Atributos                                *
 *                                                                           *
 *---------------------------------------------------------------------------*/
private:
    int presupuesto; // Presupuesto mensual
    int ingresos; // Ingresos por mes
    int retiros; // Retiros por mes
};

}
#endif
