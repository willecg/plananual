/*
 * plananual: estadistica.cc
 * Implementación de la clase "Estadistica"
 * Clase que representa las estadisticas generadas y seguidas por las
 * sucursales durante su operación cada año. Por medio de los objetos de esta
 * clase se pueden crear nuevos registros en la base de datos, borrarlo y
 * cambiar la información. Así como registrar las operaciones anuales.
 *
 * Copyright © 2014 William Ernesto Cárdenas Gomez
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif // HAVE_CONFIG_H

#include "estadistica.h"

/**\brief Clase Estadistica y sus auxiliares.
 *
 * Este espacio de nombres almacena información referente a las estadisticas de
 * los avances de la sucursal, se basa principalmente en la clase Estadisticas,
 * funciones y enumeraciones que auxilian a esta en sus operaciones. */
namespace estadisticas
{
/*****************************************************************************
 *                                                                           *
 *                               Utilerías                                   *
 *                                                                           *
 *****************************************************************************/
/**\brief Convierte de TipoCuenta a str_cuenta.
 *
 * Recibe un dato de tipo TipoCuenta y regresa su cadena correspondiente.
 * @param "const TipoCuenta cuenta" Que tipo de cuenta se va a buscar.
 * @return El valor correspondiente en forma de cadena de texto. */
std::string cuenta_cadena(const TipoCuenta cuenta)
{
    switch(cuenta) {
    case SOCIOS:
        return cadenas_cuentas[0];
    case MENORES:
        return cadenas_cuentas[1]; 
    case AHORROSOCIOS:
        return cadenas_cuentas[2]; 
    case AHORROMENORES:
        return cadenas_cuentas[3]; 
    case DEPOSITOALAVISTA:
        return cadenas_cuentas[4]; 
    case PLAZOFIJO:
        return cadenas_cuentas[5]; 
    case PRESTAMO:
        return cadenas_cuentas[6]; 
    case MOROSIDAD:
        return cadenas_cuentas[7]; 
    }
    return ""; // Si no coincide alguna regresa una cadena vacía.
}

/**\brief Convierte de str_cuenta a Tipocuenta.
 *
 * Recibe una cadena de tipo str_cuenta y lo convierte a la enumeración
 * TipoCuenta, y regresa su correspondiente en dicho tipo de dato. 
 * @param "const str_cuenta cadena" La cadena de cuenta.
 * @return el tipo de cuenta */
TipoCuenta cadena_cuenta(const str_cuenta cadena)
{
    /* Se busca la cadena correcta */
    int i;
    for (i = 0; i < 8; i++)
        if (cadena.compare(cadenas_cuentas[i]))
            break;
    
    switch(i) {
    case 0:
        return SOCIOS;
    case 1:
        return MENORES;
    case 2:
        return AHORROSOCIOS;
    case 3:
        return AHORROMENORES;
    case 4:
        return DEPOSITOALAVISTA;
    case 5:
        return PLAZOFIJO;
    case 6:
        return PRESTAMO;
    case 7:
        return MOROSIDAD;
    }
    static const std::string error(_("String not valid."));
    throw error;
}

/**\brief Lee TipoCuenta para obtener la tabla de 
 *
 * Recibe un dato de tipo TipoCuenta y regresa el índice de la tabla.
 * @param "const TipoCuenta cuenta" Que tipo de cuenta se va a buscar.
 * @return El índice correspondiente. */
int cuenta_tabla(const TipoCuenta cuenta)
{
    switch(cuenta) {
    case SOCIOS:
        return 0;
    case MENORES:
        return 1; 
    case AHORROSOCIOS:
        return 2; 
    case AHORROMENORES:
        return 3; 
    case DEPOSITOALAVISTA:
        return 4; 
    case PLAZOFIJO:
        return 5; 
    case PRESTAMO:
        return 6; 
    case MOROSIDAD:
        return 7; 
    }
    return -1; // Si no coincide alguna opción válida
}

/*****************************************************************************
 *                                                                           *
 *                          Funciones de la clase                            *
 *                          estadisticas::Estadistica                        *
 *                                                                           *
 *****************************************************************************/
/*---------------------------------------------------------------------------*
 *                                                                           *
 *                       Constructores y destructor                          *
 *                                                                           *
 *---------------------------------------------------------------------------*/
/**\brief Constructor default.
 *
 * No apto para uso, los valores que se asignan al crear
 * un objeto con este constructor no tienen correspondiente en la base de
 * datos. */
Estadistica::Estadistica()
{
    catalogos::Sucursal suc;
    setSucursal(&suc);
    setAnyo(0);
    setMes(0);
    setTipoCuenta(SOCIOS);
}

/**\brief Constructor para uso normal.
 *
 * Lee el registro de la base de datos buscando
 * las coincidencias en la base de datos y si encuentra algo, carga los
 * atributos del objeto, en caso contrario se genera una excepción de tipo
 * std::string.
 * @param "catalogos::Sucursal *sucursal" Apuntador a un objeto sucursal.
 * @param "const int anyo" Año del registro en la base de datos.
 * @param "const int mes" mes en que se registró en la base de datos.
 * @param "const TipoCuenta tipoCuenta" A que tabla se va a referir en la bd. 
 * @param "BaseDatos* const conexion" Conexión a la base de datos. */
Estadistica::Estadistica(
        catalogos::Sucursal *const sucursal,
        const int anyo,
        const int mes,
        const TipoCuenta tipoCuenta,
        BaseDatos* const conexion)
{   
    std::string query(
            "SELECT sucursal, anyo, mes FROM " +
            tablas[cuenta_tabla(tipoCuenta)] +
            " WHERE sucursal = " + std::to_string(sucursal->getID()) +
            " AND anyo = " + std::to_string(anyo) +
            " AND mes = " + std::to_string(mes) + ";");

    if(!conexion->ejecutaQuery(query))
        throw conexion->getLastError();
    if(conexion->nRegistros() <= 0)
        throw _("There is not statistic avalible.");

    setSucursal(sucursal);
    setAnyo(anyo);
    setMes(mes);;
    setTipoCuenta(tipoCuenta);
}

/**\brief Destructor de la clase 
 *
 * Función virtual que destruye la existencia de la clase */
Estadistica::~Estadistica()
{}

/*---------------------------------------------------------------------------*
 *                                                                           *
 *                      Funciones set y get                                  *
 *                                                                           *
 *---------------------------------------------------------------------------*/
/* Funciones set */////////////////////////////////////////////////////////////
/**\brief Establece objeto sucursal
 const*
 * Utiliza un apuntador para establecer dicho campo en la clase. dicha
 * información se obtiene de la base de datos.
 * @param const catalogos::Sucursal *sucursal: */
void Estadistica::setSucursal(catalogos::Sucursal *const sucursal)
{
    this->sucursal = *sucursal;
}

/**\brief Establece el mes a capturar.
 *
 * Se utiliza para determinar en que mes se desea capturar o se capturó el
 * registro. dicha información se obtiene de la base de datos.
 * @param "const int mes" Mes del calendario como entero 1 = enero. */
void Estadistica::setMes(const int mes)
{
    this->mes = mes;
}

/**\brief Establece el año.
 *
 * Se utiliza para establecer el año del registro que se esta leyendo o que se
 * va a crear.
 * @param "const int anyo" Año calendarico. */
void Estadistica::setAnyo(const int anyo)
{
    this->anyo = anyo;
}

/**\brief Estrablece el tipo de cuenta.
 *
 * Determina el tipo de cuenta de donde se desea leer o capturar en la base de
 * datos, por medio de este dato se determina que tabla trabajar.
 * @param "const tipoCuenta" Tipo de cuenta a manipular. */
void Estadistica::setTipoCuenta(const TipoCuenta tipoCuenta)
{
    this->tipoCuenta = tipoCuenta;
}

/* Funciones get */
/**\brief Obtiene la sucursal del objeto.
 *
 * Nos entrega un objeto sucursal, el cual representa a la sucursal a la cual
 * pertenece el registro que se esta leyendo o trabajando.
 * @return Copia del objeto sucursal. */
catalogos::Sucursal Estadistica::getSucursal()
{
    return this->sucursal;
}

/**\brief Obtiene el mes.
 *
 * El número de mes que se trabaja o se maneja en la base de datos.
 * @return El valor del atributo mes. */
int Estadistica::getMes()
{
    return this->mes;
}

/**\brief Obtiene el año.
 *
 * Determina que año tiene el registro en la base de datos o el que se esta
 * manejando.
 * @return El valor del atributo año. */
int Estadistica::getAnyo()
{
    return this->anyo;
}

/**\brief Obtiene el tipo de cuenta.
 *
 * Se determina que tipo de cuenta se debe trabajar o bien. A que tabla se
 * deben dirigir las operaciones de la base de datos.
 * @return TipoCuenta: El tipo de cuenta que se tiene. */
TipoCuenta Estadistica::getTipoCuenta()
{
    return this->tipoCuenta;
}

/**\brief Delete the statistic from the data base.
 *
 * The information must have to exists.
 * @param "BaseDatos *const conexion" Conexion a la base de datos.*/
void Estadistica::deleteEstadistica(BaseDatos* const db)
{
    std::string query(
            "SELECT metas.borrar_registro('" +
            getSucursal().getNombre() + "'," + 
            std::to_string(getAnyo()) + " ," + 
            std::to_string(getMes()) + " ," +
            std::to_string(cuenta_tabla(getTipoCuenta())) + " )");
    if(!db->ejecutaQuery(query))
        throw db->getLastError();
}
/**\brief Modifica la sucursal que se esta trabajando.
 *
 * Cambia el nombre de la sucursal en la que esta trabajando el registro de
 * la base de datos.
 * @param "catalogos::Sucursal *const sucursal" La nueva sucursal.
 * @param "BaseDatos *const conexion" Conexion a la base de datos.*/
void Estadistica::modificarSucursal(
        catalogos::Sucursal *const sucursal, BaseDatos *const conexion)
{ 
    std::string query(
            "SELECT metas.modificar_sucursal('" +
            sucursal->getNombre() + "', '" + 
            getSucursal().getNombre() + "', " + 
            std::to_string(getAnyo()) + ", " +
            std::to_string(getMes()) + ", " +
            std::to_string(cuenta_tabla(getTipoCuenta())) + ");");

    if (!conexion->ejecutaQuery(query))
        throw conexion->getLastError();

    setSucursal(sucursal);
}

/**\brief Modifica año del registro que se esta trabajando.
 *
 * Cambia el año del registro en la que esta trabajando el registro de
 * la base de datos.
 * @param "const int anyo" La nueva sucursal.
 * @param "BaseDatos *const conexion" Conexion a la base de datos.*/
void Estadistica::modificarAnyo(const int anyo, BaseDatos *const conexion)
{  
    std::string query(
            "SELECT metas.modificar_anyo('" +
            sucursal.getNombre() + "', " + 
            std::to_string(getAnyo()) + ", " + 
            std::to_string(anyo) + ", " +
            std::to_string(getMes()) + ", " +
            std::to_string(cuenta_tabla(getTipoCuenta())) + ");");
    if (!conexion->ejecutaQuery(query))
        throw conexion->getLastError();
    setAnyo(anyo);
}

/**\brief Modifica el mes del registro que se esta trabajando.
 *
 * Cambia el mes del registro en la que esta trabajando el registro de
 * la base de datos.
 * @param "const int mes" La nueva sucursal.
 * @param "BaseDatos *const conexion" Conexion a la base de datos.*/
void Estadistica::modificarMes(const int mes, BaseDatos *const conexion)
{ 
    std::string query(
            "SELECT metas.modificar_mes('" +
            sucursal.getNombre() + "', " + 
            std::to_string(getAnyo()) + ", " + 
            std::to_string(mes) + ", " +
            std::to_string(getMes()) + ", " +
            std::to_string(cuenta_tabla(getTipoCuenta())) + ");");
    if (!conexion->ejecutaQuery(query))
        throw conexion->getLastError();
    setAnyo(anyo);
}

} // Fin del namespace
