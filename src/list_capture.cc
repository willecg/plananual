/*
 * planadmin: list_capture.c
 * Control a ListStore for the capture of the news records.
 *
 * Copyright © 2014 William Ernesto Cárdenas Gomez
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "list_capture.h"
#include "catalogos/sucursal.h"
#include <iostream>
extern const int MINMONTH; 
extern const int MAXMONTH;

/**\brief Namespace admin for the admin operations.
 *
 * Declares the gui for the admin operations of the plan proyect. May be on
 * follow versions will be integrated in plan namespace.*/
namespace admin{
/*---------------------------------------------------------------------------*
 *                                                                           *
 *                        Functions miscelaneus                              *
 *                                                                           *
 *---------------------------------------------------------------------------*/
/** \brief Create an empy array for the statistics.
 *
 * Get an array of estadisticas::Estadisticas objects and fill it with empty
 * references to them.
 * \param "estadisticas::Estadistica * stats[][]" Matrix of references of
 * stats.*/
void create_empty_statistics(estadisticas::Estadistica* stats[][STATISTICSNUM])
{
    for(int i = MINMONTH; i < MAXMONTH; i++)
        for(int j = static_cast<int>(estadisticas::SOCIOS);
                j <= static_cast<int>(estadisticas::MOROSIDAD);
                j++)
            if(j <= 1)
                stats[i][j] = new estadisticas::EstadisticaEnteros();
            else
                stats[i][j] = new estadisticas::EstadisticaEnteros();
}

/** \brief Delete an matrix of statistics.
 *
 * Clear the memory form the matrix of estadisticas::Estadistica.
 * \param "estadisticas::Estadistica * stats[][]" Matrix of references of
 * stats.*/
void delete_statistics(estadisticas::Estadistica * stats[][STATISTICSNUM])
{
    for(int i = MINMONTH; i < MAXMONTH; i++)
        for(int j = static_cast<int>(estadisticas::SOCIOS);
                j <= static_cast<int>(estadisticas::MOROSIDAD);
                j++)
            delete stats[i][j];
}

/*****************************************************************************
 *                                                                           *
 *                            Classes Functions                              *
 *                                                                           *
 *****************************************************************************/
/*---------------------------------------------------------------------------*
 *                                                                           *
 *                    Constructor & Destructor                               *
 *                                                                           *
 *---------------------------------------------------------------------------*/
/**\brief Default constructor.
 *
 * Set the default model for the list store. */
ListCapture::ListCapture()
{
    set_list();
}

/**\brief Constructor with default treeview pointer.
 *
 * Set the default model for the list store and prepares the treeview.
 * \param "Gtk::TreeView *tree" Widget Gtk::TreeView to work with.*/
ListCapture::ListCapture(Gtk::TreeView *treeView, int year)
{
    set_list();
    set_treeview(treeView);
    set_year(year);
}

/**Destructor*/
ListCapture::~ListCapture()
{
    // TODO
}

/*---------------------------------------------------------------------------*
 *                                                                           *
 *                       Set Get member functions                            *
 *                                                                           *
 *---------------------------------------------------------------------------*/
/*---------------------------Set Functions-----------------------------------*/
/**\brief Constructor with default treeview pointer.
 *
 * Set the TreeView at the member of this class. Also set it model and add
 * the columns.
 * \param "Gtk::TreeView *tree" Widget Gtk::TreeView to work with. */
void ListCapture::set_treeview(Gtk::TreeView *tree)
{
    tree_view = tree;
    tree_view->set_model(list);
    tree_view->append_column(_("Branch"), model.branch);
    tree_view->append_column_editable(_("Partners"), model.partners);
    tree_view->append_column_editable(_("Children"), model.children);
    tree_view->append_column_numeric_editable(
            _("Saving partners"), model.saving_partners, "%0.2f");
    tree_view->append_column_numeric_editable(
            _("Saving children"), model.saving_children, "%0.2f");
    tree_view->append_column_numeric_editable(
            _("Deposit on demand"), model.deposit, "%0.2f");
    tree_view->append_column_numeric_editable(
            _("Fixed deposit"), model.fixed_deposit, "%0.2f");
    tree_view->append_column_numeric_editable(
            _("loan"), model.loan, "%0.2f");
    tree_view->append_column_numeric_editable(
            _("% on default"), model.defaulting, "%0.2f");
}

/**\brief Creates the Gtk::ListStore and set its model.
 *
 * There is no way to set another model.*/
void ListCapture::set_list()
{
    list = Gtk::ListStore::create(model);
}

/**\Set the year.
 *
 * The year should be the next of today year.
 * \param "const int year" The year to use to capture.*/
void ListCapture::set_year(const int year)
{
    this->year = year;
}

/*---------------------------Get Functions-----------------------------------*/
/**\brief Gets the ListStore model from the treeview.
 *
 * You should set the year first.*/
Glib::RefPtr<Gtk::ListStore> ListCapture::get_list()
{
    return list;
}

/**\brief Get the year.
 *
 * Obtain the year to capture.
 * \return the year as int. */
int ListCapture::get_year()
{
    return year;
}

/**\brief Get the year.
 *
 * Obtain the year to capture.
 * \return the year as std::string. */
std::string ListCapture::get_year_str()
{
    return std::to_string(year);
}
/*---------------------------------------------------------------------------*
 *                                                                           *
 *                       Operational member functions                        *
 *                                                                           *
 *---------------------------------------------------------------------------*/
/**\brief Fill the table with empty information.
 *
 * Prepare the table with empty information. This should be use for create
 * new information in the database. (estadisticas::Estadisticas
 * information.)
 * \param "db::BaseDatos *conn" pointer to the database connection.
 * \return true if success, elsewere false.*/
bool ListCapture::prepare_to_insert(db::BaseDatos *conn)
{
    /* Prepare zone. */
    list->clear();
    const std::string query("SELECT sucursal FROM public.sucursales;");
    bool flag;
    if(flag = conn->ejecutaQuery(query)){
        Gtk::TreeModel::Row row;
        /* Fill of the structure. */
        for(int i = 0; i < conn->nRegistros(); i++){
            row = *(list->append());
            row[model.branch] = conn->regresaDato(i, 0);
        }
        saving = ADD;
    }
    return flag;
}

/**\brief Fill the table with stored information.
 *
 * This should be use for update information that already exist in the
 * database (estadisticas::Estadisticas information).
 * \param "db::BaseDatos *conn" pointer to the database connection.
 * \return true if success, elsewere false.*/
bool ListCapture::prepare_to_update(db::BaseDatos *conn)
{
    list->clear();
    bool flag;
    Gtk::TreeModel::Row row;
    int acum_integer;
    double acum_real;
    if(flag = conn->ejecutaQuery("SELECT sucursal FROM public.sucursales;")){
        std::vector<std::string> branches;
        for(int i = 0; i < conn->nRegistros(); i++)
            branches.push_back(conn->regresaDato(i, 0));
        /* For branches*/
        for(std::vector<std::string>::iterator iter = branches.begin();
                iter != branches.end(); iter++){
            row = *(list->append());
            row[model.branch] = *iter;
            /* Iter the acounts*/
            for(int j = static_cast<int>(estadisticas::SOCIOS);
                    j <= static_cast<int>(estadisticas::MOROSIDAD);
                    j++){
                estadisticas::TipoCuenta acount =
                    static_cast<estadisticas::TipoCuenta>(j);
                acum_integer = 0;
                acum_real = 0.0;
                for(int month = MINMONTH; month < MAXMONTH; month++)
                    if(j <= 1){
                        stats[month][j] =
                            new estadisticas::EstadisticaEnteros(
                                    new catalogos::Sucursal(*iter,conn),
                                    year,
                                    month,
                                    acount,
                                    conn);
                        acum_integer +=
                            static_cast<estadisticas::EstadisticaEnteros*>(
                                    stats[month][j])->getPresupuesto();
                        
                                                        
                    }
                    else{
                        stats[month][j] =
                            new estadisticas::EstadisticaReales(
                                    new catalogos::Sucursal(*iter,conn),
                                    year,
                                    month,
                                    acount,
                                    conn);
                        acum_real +=
                                static_cast<estadisticas::EstadisticaReales*>(
                                    stats[month][j])->getPresupuesto();
                    } // If..else
                if(j <= 1)
                    set_row_data(row, acount, acum_integer);
                else
                    set_row_data(row, acount, acum_real);
            } // For. Iter the acounts
            delete_statistics(stats);
        } // For. Branches
        saving = UPDATE;
    } // if
    return flag;
}

/**\brief Destroy the information stored in the table with the year.
 *
 * The use of this operation is only for destroy all the information of a
 * sigle year. The only year that can be destroyed will be is what is
 * establish in the year member field.
 * \param "db::BaseDatos *conn" pointer to the database connection.
 * \return true if success, elsewere false.*/
bool ListCapture::delete_info(db::BaseDatos *conn)
{
    list->clear();
    bool flag;
    if(flag = conn->ejecutaQuery("SELECT sucursal FROM public.sucursales;")){
        std::vector<std::string> branches;
        for(int i = 0; i < conn->nRegistros(); i++)
            branches.push_back(conn->regresaDato(i, 0));
        /* For branches*/
        for(std::vector<std::string>::iterator iter = branches.begin();
                iter != branches.end(); iter++){
            /* Iter the acounts*/
            for(int j = static_cast<int>(estadisticas::SOCIOS);
                    j <= static_cast<int>(estadisticas::MOROSIDAD);
                    j++){
                estadisticas::TipoCuenta acount =
                    static_cast<estadisticas::TipoCuenta>(j);
                for(int month = MINMONTH; month < MAXMONTH; month++){
                    if(j <= 1)
                        stats[month][j] =
                            new estadisticas::EstadisticaEnteros(
                                    new catalogos::Sucursal(*iter,conn),
                                    year,
                                    month,
                                    acount,
                                    conn);
                    else
                        stats[month][j] =
                            new estadisticas::EstadisticaReales(
                                    new catalogos::Sucursal(*iter,conn),
                                    year,
                                    month,
                                    acount,
                                    conn);
                    stats[month][j]->deleteEstadistica(conn);
                } // Inner for.
            } // For. Iter the acounts
        } // For. Branches
        delete_statistics(stats);
        saving = NONE;
    } // if
    return flag;


}
/**\brief Update the database conn with new information.
 *
 * The information is obtained from the list store.*/
void ListCapture::save_information(db::BaseDatos *conn)
{
    switch(saving){
    case ADD:
        insert(conn);
        break;
    case UPDATE:
        update(conn);
    }
    saving = NONE;
    list->clear();
}

/** Insert in the data base with the treeview information. */
void ListCapture::insert(db::BaseDatos *conn)
{
    /*Initialization*/
    typedef Gtk::TreeModel::Children type_children;
    type_children children = list->children();
    Gtk::TreeModel::Row row;
    create_empty_statistics(stats);
    /*check every single row and sets the values on the database.*/
    for(type_children::iterator iter = children.begin();
            iter != children.end(); iter++){
        row = *iter;
        for(int i = MINMONTH; i < MAXMONTH; i++){
            for(int j = static_cast<int>(estadisticas::SOCIOS);
                    j <= static_cast<int>(estadisticas::MOROSIDAD);
                    j++){
                estadisticas::TipoCuenta acount = 
                    static_cast<estadisticas::TipoCuenta>(j);
                if(j <= 1){
                    int final_insert;
                    /* Tree cases.
                     * 1.- The number is divisible with 12, so, the only I have to do is
                     *     make this division, The result is stored.
                     * 2.- The number is not divisible with 12, so, uses the division per 12
                     *     then the surplus is added to the last or the first month.
                     * 3.- The number is not divisible with 12, so, uses the division per 11
                     *     Then the surplus is added in the 12th month.
                     * 
                     * I will use initially the 2th case. The first is by default.*/

                    final_insert = get_row_data(row, acount) / 12;
                    if(static_cast<int>(get_row_data(row, acount)) % 12 != 0 && i == 12)
                        final_insert += static_cast<int>(get_row_data(row, acount)) % 12;

                    *stats[i][j] = estadisticas::EstadisticaEnteros::crear(
                            new catalogos::Sucursal(row[model.branch],conn),
                            year,
                            i,
                            final_insert,
                            acount,
                            conn);
                }
                else
                    *stats[i][j] = estadisticas::EstadisticaReales::crear(
                            new catalogos::Sucursal(row[model.branch],conn),
                            year,
                            i,
                            get_row_data(row, acount)/12.00,
                            acount,
                            conn);
            } // For. Iterate the acounts, any row.
        } // For. Iterate the months.    
    } // For. Iterate in the list store.
    delete_statistics(stats);
}

/** Update the data base with the treeview information. */
void ListCapture::update(db::BaseDatos *conn)
{
    /*Initialization*/
    typedef Gtk::TreeModel::Children type_children;
    type_children children = list->children();
    Gtk::TreeModel::Row row;
    /*check every single row and sets the values on the database.*/
    for(type_children::iterator iter = children.begin();
            iter != children.end(); iter++){
        row = *iter;
        for(int i = MINMONTH; i < MAXMONTH; i++){
            for(int j = static_cast<int>(estadisticas::SOCIOS);
                    j <= static_cast<int>(estadisticas::MOROSIDAD);
                    j++){
                estadisticas::TipoCuenta acount = 
                    static_cast<estadisticas::TipoCuenta>(j);
                if(j <= 1){
                    int final_insert;
                    /* Tree cases.
                     * 1.- The number is divisible with 12, so, the only I have to do is
                     *     make this division, The result is stored.
                     * 2.- The number is not divisible with 12, so, uses the division per 12
                     *     then the surplus is added to the last or the first month.
                     * 3.- The number is not divisible with 12, so, uses the division per 11
                     *     Then the surplus is added in the 12th month.
                     * 
                     * I will use initially the 2th case. The first is by default.*/

                    final_insert = get_row_data(row, acount) / 12;
                    if(static_cast<int>(get_row_data(row, acount)) % 12 != 0 && i == 12)
                        final_insert += static_cast<int>(get_row_data(row, acount)) % 12;
                    stats[i][j] = new estadisticas::EstadisticaEnteros(
                            new catalogos::Sucursal(row[model.branch], conn),
                            year, i , acount, conn);
                    static_cast<estadisticas::EstadisticaEnteros*>(
                            stats[i][j])->modificarPresupuesto(
                                final_insert, conn);
                }
                else{
                    stats[i][j] = new estadisticas::EstadisticaReales(
                            new catalogos::Sucursal(row[model.branch], conn),
                            year ,i , acount, conn);
                    static_cast<estadisticas::EstadisticaReales*>(
                            stats[i][j])->modificarPresupuesto(
                                get_row_data(row, acount)/12.00, conn);
                }
            } // For. Iterate the acounts, any row.
        } // For. Iterate the months.
    } // For. Iterate in the list store.
    delete_statistics(stats);
}

/**\brief Get row data.
 *
 * Recieve a TipoCuenta param and return the value that the row have.
 * \param "Gtk::TreeModel::Row row" Row to read.
 * \param "estadisticas::TipoCuenta data" Column to affect.
 * \return te value stored in the table.*/
double ListCapture::get_row_data(
        Gtk::TreeModel::Row row,estadisticas::TipoCuenta data)
{
    double value;
    switch(data){
    case estadisticas::SOCIOS:
        value = row[model.partners];
        break;
    case estadisticas::MENORES:
        value = row[model.children];
        break;
    case estadisticas::AHORROSOCIOS:
        value = row[model.saving_partners];
        break;
    case estadisticas::AHORROMENORES:
        value = row[model.saving_children];
        break;
    case estadisticas::DEPOSITOALAVISTA:
        value = row[model.deposit];
        break;
    case estadisticas::PLAZOFIJO:
        value = row[model.fixed_deposit];
        break;
    case estadisticas::PRESTAMO:
        value = row[model.loan];
        break;
    case estadisticas::MOROSIDAD:
        value = row[model.defaulting];
        break;
    default:
        throw _("Bad name of TipoTabla. See documentation.");
    }
    return value;
}

/**\brief Set row data.
 *
 * Set avalue for the especified row and in the especify TipoCuenta.
 * \param "Gtk::TreeModel::Row row" Row to update.
 * \param "estadisticas::TipoCuenta data" Column to affect.
 * \param "double value" value to set.*/
void ListCapture::set_row_data(
        Gtk::TreeModel::Row row, estadisticas::TipoCuenta data, double value){
    switch(data){
    case estadisticas::SOCIOS:
        row[model.partners] = value;
        break;
    case estadisticas::MENORES:
        row[model.children] = value;
        break;
    case estadisticas::AHORROSOCIOS:
        row[model.saving_partners] = value;
        break;
    case estadisticas::AHORROMENORES:
        row[model.saving_children] = value;
        break;
    case estadisticas::DEPOSITOALAVISTA:
        row[model.deposit] = value;
        break;
    case estadisticas::PLAZOFIJO:
        row[model.fixed_deposit] = value;
        break;
    case estadisticas::PRESTAMO:
        row[model.loan] = value;
        break;
    case estadisticas::MOROSIDAD:
        row[model.defaulting] = value;
        break;
    default:
        throw _("Bad name of TipoTabla. See documentation.");
    }
}

/**\brief Clear the liststore ref.
 *
 * Clear the data on the liststore. */
void ListCapture::clear()
{
    list->clear(); // So exhausted |-(
}
} // End of the admin namespace
