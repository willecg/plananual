/*
 * planadmin: administracion.cc
 * Combo box that store branches name.
 *
 * Copyright © 2014 William Ernesto Cárdenas Gomez
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef COMBO_BRANCHES_H
#define COMBO_BRANCHES_H

#include <vector>

#include <gtkmm/comboboxtext.h>
#include <gtkmm/builder.h>

#include "../lib/baseDatos.h"

/**\brief Namespace admin for the admin operations.
 *
 * Declares the gui for the admin operations of the plan proyect. May be on
 * follow versions will be integrated in plan namespace.*/ 
namespace admin{
/**\brief Query to obtain branches
 *
 * This is the query that reads from database and obtain an list of branches
 * and its id's*/
const std::string query_branches(
        "SELECT id_sucursal, sucursal \
        FROM public.sucursales ORDER BY id_sucursal");

/**\brief Is a simbolic Gtk::ComboBoxText.
 *
 * Fills the comboboxtext and get the information selected */
class ComboBoxBranches : public Gtk::ComboBoxText{
public:
/*---------------------------------------------------------------------------*
 *                                                                           *
 *                           Constructors and destructors                    *
 *                                                                           *
 *---------------------------------------------------------------------------*/
    /**\brief Constructor for get_widget_derived.
     * 
     * Only creates the object but does not put information in it.
     * This is only usable by a Gtk::Builder object with its method
     * get_widget_derived.
     * \param "BaseObjectType *cobject"
     * \param "const Glib::RefPtr<Gtk::Builder> builder" Gtk::Builder object.*/
    ComboBoxBranches(BaseObjectType *cobject,
            const Glib::RefPtr<Gtk::Builder> builder);

    /**Destructor.*/
    ~ComboBoxBranches();

private:
/*---------------------------------------------------------------------------*
 *                                                                           *
 *                           Set get and miscellaneous                       *
 *                                                                           *
 *---------------------------------------------------------------------------*/
    /**\brief Set the status.
     *
     * If there are database connection status is going tu be true. Elsewere
     * false.
     * \param "bool status_value" True for connection false for no connection*/
    void set_status(bool status_value); 
public:
    /**\brief Get the status.
     *
     * If there are database connection status is going tu be true. Elsewere
     * false.
     * \return status.*/
    bool get_status();

    /**\brief Set the branch_list and the branch_index.
     *
     * Uses the connection to read all branches and stores it on the vectos.
     * \param "db::BaseDatos *conn" Connection to database.
     * \return True if success elsewere false.*/
    bool set_branches(db::BaseDatos *conn);

    /*Utilities*/
    /**\brief Fill the combo box.
     * 
     * With information stored on branch_list.*/
    void fill_combo();
private:
    std::vector<std::string> branch_list; // 
    std::vector<std::string> branch_index;
    bool status; // true connected to database, false disconected
};
} // Namespace end.
#endif // COMBO_BRANCHES_H
