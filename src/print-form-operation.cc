/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 4; tab-width: 4 -*-  */
/*
 * print-form-operation.cc
 * Copyright (C) 2014 Pc0147 William <pc0147@pc0147>
 *
 * plananual-1.2 is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * plananual-1.2 is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <glibmm/i18n.h>
#include "print-form-operation.h"
#include <gtkmm/messagedialog.h>
#include <tree_model_plan.h>
#include <iomanip>

/**\brief Common operations.
 *
 * This is a group of common functions to diferent programs on this package.*/
namespace plan{
/*---------------------------------------------------------------------------*
 *                                                                           *
 *                Constructors & Destructors functions                       *
 *                                                                           *
 *---------------------------------------------------------------------------*/
/**\brief Default constructor.
 *
 * \see "static Glib::RefPtr<PrintFormOperation> create()"*/
PrintFormOperation::PrintFormOperation()
{}

/**\breif Destructor*/
PrintFormOperation::~PrintFormOperation()
{
}

/*---------------------------------------------------------------------------*
 *                                                                           *
 *                     Set - Get Functions                                   *
 *                                                                           *
 *---------------------------------------------------------------------------*/
/**\brief Set the listStore reference.
 * 
 * Set the correct value for a list.
 * \param "Glib::RefPtr<Gtk::ListStore> list" ListStore with the info.*/
void PrintFormOperation::set_list(Glib::RefPtr<Gtk::ListStore> list)
{
   this->list = list;
}

/**\brief Set the branch name.
 * 
 * Set the correct value for the branch.
 * \param "const std::string branch_name" Branch name.*/
void PrintFormOperation::set_branch(const Glib::ustring& branch_name)
{
   branch = branch_name;
}

/**\brief Set the year.
 * 
 * Set the year to print.
 * \param "const int year" Year to print.*/
void PrintFormOperation::set_year(const int year)
{
   this->year = year;
}

/*---------------------------------------------------------------------------*
 *                                                                           *
 *                        Operational functions                              *
 *                                                                           *
 *---------------------------------------------------------------------------*/
/**\brief Virtual from the Gtk::PrintOperation.
 *
 * Prepares the print operation.*/
void PrintFormOperation::on_begin_print(
            const Glib::RefPtr<Gtk::PrintContext>& print_context)
{
    m_refLayout = print_context->create_pango_layout();
    Pango::FontDescription font("mono 9");
    m_refLayout->set_font_description(font);
    const double width = print_context->get_width();
    const double height = print_context->get_height();
    m_refLayout->set_width(static_cast<int>(width * Pango::SCALE));

    /*Prepare text to print*/
    Glib::ustring header_a = _("<tt><u>Month               News       Leave        Real    Budgeted       Total</u></tt>\n");
    Glib::ustring header_b = _("<tt><u>Month           Realized    Budgeted       Total</u></tt>\n");
    Glib::ustring out_text;
    out_text += _("<b>Branch:</b> ") + branch + " - ";
    out_text += _("<b>Year:</b> ") + std::to_string(year) + "\n\n";

    /*Make the report in out_text*/
    out_text += _("<big>Partners</big>\n"); // Header of the table
    out_text += header_a;
    /* Data from the tree */
    TreeModelPlan model;
    typedef Gtk::TreeModel::Children type_children;
    type_children children = list->children();
    Gtk::TreeModel::Row row;
    Glib::ustring u_month;
    for(type_children::iterator iter = children.begin();
        iter != children.end(); ++iter)
    {
        row = *iter;
        u_month = row[model.month];
        out_text += Glib::ustring::format(std::left, std::setw(12), u_month.raw());
        out_text += Glib::ustring::format(std::right, std::setw(12), row[model.partners_in]);
        out_text += Glib::ustring::format(std::right, std::setw(12), row[model.partners_out]);
        out_text += Glib::ustring::format(std::right, std::setw(12), row[model.partners_realized]);
        out_text += Glib::ustring::format(std::right, std::setw(12), row[model.partners_budgeted]);
        out_text += Glib::ustring::format(std::right, std::setw(12), row[model.partners_total]) + "\n";
    }

    out_text += _("\n\n<big>Children</big>\n"); // Header of the table
    out_text += header_a;
    /* Data from the tree */
    for(type_children::iterator iter = children.begin();
        iter != children.end(); ++iter)
    {
        row = *iter;
        u_month = row[model.month];
        out_text += Glib::ustring::format(std::left, std::setw(12), u_month.raw());
        out_text += Glib::ustring::format(std::right, std::setw(12), row[model.children_in]);
        out_text += Glib::ustring::format(std::right, std::setw(12), row[model.children_out]);
        out_text += Glib::ustring::format(std::right, std::setw(12), row[model.children_realized]);
        out_text += Glib::ustring::format(std::right, std::setw(12), row[model.children_budgeted]);
        out_text += Glib::ustring::format(std::right, std::setw(12), row[model.children_total]) + "\n";
    }

    out_text += _("\n\n<big>Partner saving</big>\n"); // Header of the table
    out_text += header_b;
    /* Data from the tree */
    for(type_children::iterator iter = children.begin();
        iter != children.end(); ++iter)
    {
        row = *iter;
        u_month = row[model.month];
        out_text += Glib::ustring::format(std::left, std::setw(12), u_month.raw());
        out_text += Glib::ustring::format(std::right, std::setw(12), row[model.partners_saving_realized]);
        out_text += Glib::ustring::format(std::right, std::setw(12), row[model.partners_saving_budgeted]);
        out_text += Glib::ustring::format(std::right, std::setw(12), row[model.partners_saving_total]) + "\n";
    }

    out_text += _("\n\n<big>Children saving</big>\n"); // Header of the table
    out_text += header_b;
    /* Data from the tree */
    for(type_children::iterator iter = children.begin();
        iter != children.end(); ++iter)
    {
        row = *iter;
        u_month = row[model.month];
        out_text += Glib::ustring::format(std::left, std::setw(12), u_month.raw());
        out_text += Glib::ustring::format(std::right, std::setw(12), std::setprecision(2), row[model.children_saving_realized]);
        out_text += Glib::ustring::format(std::right, std::setw(12), std::setprecision(2), row[model.children_saving_budgeted]);
        out_text += Glib::ustring::format(std::right, std::setw(12), std::setprecision(2), row[model.children_saving_total]) + "\n";
    }

    out_text += _("\n\n\n\n\n<big>Deposit on demand</big>\n"); // Header of the table
    out_text += header_b;
    /* Data from the tree */
    for(type_children::iterator iter = children.begin();
        iter != children.end(); ++iter)
    {
        row = *iter;
        u_month = row[model.month];
        out_text += Glib::ustring::format(std::left, std::setw(12), u_month.raw());
        out_text += Glib::ustring::format(std::right, std::setw(12), row[model.deposit_realized]);
        out_text += Glib::ustring::format(std::right, std::setw(12), row[model.deposit_budgeted]);
        out_text += Glib::ustring::format(std::right, std::setw(12), row[model.deposit_total]) + "\n";
    }

    out_text += _("\n\n<big>Fixed deposit</big>\n"); // Header of the table
    out_text += header_b;
    /* Data from the tree */
    for(type_children::iterator iter = children.begin();
        iter != children.end(); ++iter)
    {
        row = *iter;
        u_month = row[model.month];
        out_text += Glib::ustring::format(std::left, std::setw(12), u_month.raw());
        out_text += Glib::ustring::format(std::right, std::setw(12), row[model.fixed_realized]);
        out_text += Glib::ustring::format(std::right, std::setw(12), row[model.fixed_budgeted]);
        out_text += Glib::ustring::format(std::right, std::setw(12), row[model.fixed_total]) + "\n";
    }

    
    out_text += _("\n\n<big>Loan</big>\n"); // Header of the table
    out_text += header_b;
    /* Data from the tree */
    for(type_children::iterator iter = children.begin();
        iter != children.end(); ++iter)
    {
        row = *iter;
        u_month = row[model.month];
        out_text += Glib::ustring::format(std::left, std::setw(12), u_month.raw());
        out_text += Glib::ustring::format(std::right, std::setw(12), row[model.loan_realized]);
        out_text += Glib::ustring::format(std::right, std::setw(12), row[model.loan_budgeted]);
        out_text += Glib::ustring::format(std::right, std::setw(12), row[model.loan_total]) + "\n";
    }

    
    out_text += _("\n\n<big>% of delay on payment</big>\n"); // Header of the table
    out_text += header_b;
    /* Data from the tree */
    for(type_children::iterator iter = children.begin();
        iter != children.end(); ++iter)
    {
        row = *iter;
        u_month = row[model.month];
        out_text += Glib::ustring::format(std::left, std::setw(12), u_month.raw());
        out_text += Glib::ustring::format(std::right, std::setw(12), row[model.delay_realized]);
        out_text += Glib::ustring::format(std::right, std::setw(12), row[model.delay_budgeted]);
        out_text += Glib::ustring::format(std::right, std::setw(12), row[model.delay_total]) + "\n";
    }

    m_refLayout->set_markup(out_text);

    const int line_count = m_refLayout->get_line_count();

    Glib::RefPtr<Pango::LayoutLine> layout_line;
    double page_height = 0;

    for (int line = 0; line < line_count; ++line)
    {
        Pango::Rectangle ink_rect, logical_rect;

        layout_line = m_refLayout->get_line(line);
        layout_line->get_extents(ink_rect, logical_rect);

        const double line_height = logical_rect.get_height() / 1024.0;

        if (page_height + line_height > height)
        {
            m_PageBreaks.push_back(line);
            page_height = 0;
        }

        page_height += line_height;
    }

    set_n_pages(m_PageBreaks.size() + 1);
}

/**\brief Virtual from the Gtk::PrintOperation.
 *
 * Makes the print on a cairo context.*/
void PrintFormOperation::on_draw_page(
            const Glib::RefPtr<Gtk::PrintContext>& print_context, int page_nr)
{
    //Decide which lines we need to print in order to print the specified page:
  int start_page_line = 0;
  int end_page_line = 0;

  if(page_nr == 0)
  {
    start_page_line = 0;
  }
  else
  {
    start_page_line = m_PageBreaks[page_nr - 1];
  }

  if(page_nr < static_cast<int>(m_PageBreaks.size()))
  {
    end_page_line = m_PageBreaks[page_nr];
  }
  else
  {
    end_page_line = m_refLayout->get_line_count();
  }

  //Get a Cairo Context, which is used as a drawing board:
  Cairo::RefPtr<Cairo::Context> cairo_ctx = print_context->get_cairo_context();

  //We'll use black letters:
  cairo_ctx->set_source_rgb(0, 0, 0);

  //Render Pango LayoutLines over the Cairo context:
  Pango::LayoutIter iter = m_refLayout->get_iter();

  double start_pos = 0;
  int line_index = 0;

  do
  {
    if(line_index >= start_page_line)
    {
      Glib::RefPtr<Pango::LayoutLine> layout_line = iter.get_line();
      Pango::Rectangle logical_rect = iter.get_line_logical_extents();
      int baseline = iter.get_baseline();

      if (line_index == start_page_line)
      {
        start_pos = logical_rect.get_y() / 1024.0;
      }

      cairo_ctx->move_to(logical_rect.get_x() / 1024.0,
        baseline / 1024.0 - start_pos);

      layout_line->show_in_cairo_context(cairo_ctx);
    }

    line_index++;
  }
  while(line_index < end_page_line && iter.next_line());
}

/*---------------------------------------------------------------------------*
 *                                                                           *
 *                     Static memeber functions                              *
 *                                                                           *
 *---------------------------------------------------------------------------*/
/**\brief Create a reference to the PrintFormOperation.
 * 
 * This is the correct way to obtain it.
 * \return The reference to a new PrintFormOperation object.*/ 
Glib::RefPtr<PrintFormOperation> PrintFormOperation::create()
{
    return Glib::RefPtr<PrintFormOperation>(new PrintFormOperation());
}
} // Namespace end.