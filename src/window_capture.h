/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 4; tab-width: 4 -*-  */
/*
 * window_capture.h
 * Copyright (C) 2014 William Ernesto Cárdenas Gómez <sistemas.will@alianzacpa.com.mx>
 *
 * plananual-1.2 is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * plananual-1.2 is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _WINDOW_CAPTURE_H_
#define _WINDOW_CAPTURE_H_

#include <gtkmm/applicationwindow.h>
#include <gtkmm/action.h>
#include <gtkmm/treeview.h>
#include <gtkmm/builder.h>
#include <gtkmm/spinbutton.h>
#include "text-view-statistics.h"
#include "catalogos/usuario.h"
#include "../lib/baseDatos.h"
#include "list_reports.h"
#include "print-form-operation.h"

/**\brief Namespace to capture the branch advance.
 * 
 * This namespace is used to the plancaptura program.*/
namespace capture{

/**\brief Date.
 *
 * Simple model for the date*/
struct date{
     int day;/**<Day*/
     int month;/**<Month*/
     int year;/**<Year*/
};

/**\brief Interface to make the capture.
 *
 * This class contains the necesary to capture the advance for a month of the
 * branch, and also get reports to see the advance of the same branch.*/
class WindowCapture: public Gtk::ApplicationWindow 
{
public:
/*---------------------------------------------------------------------------*
 *                                                                           *
 *                       Constructor & Destructor                            *
 *                                                                           *
 *---------------------------------------------------------------------------*/
    /**\brief Constructor.
     * 
     * Prepares the interface. This is designed to be used by the
     * get_derived_widget method form Gtk::Builder.
     * \param "BaseObjectType cobject" Wraper of a C object.
     * \param "const Glib::RefPtr<Gtk::Builder>& builder" Reference to a
     * builder object.*/
    WindowCapture(BaseObjectType* cobject,
                  const Glib::RefPtr<Gtk::Builder>& builder);
    /**Destructor*/
    virtual ~WindowCapture();

/*---------------------------------------------------------------------------*
 *                                                                           *
 *                      Member functions miscelanius                         *
 *                                                                           *
 *---------------------------------------------------------------------------*/
private:
    /**\brief Load the events.
     * 
     * This method call the events for the constructor.*/
    void load_events();
    
public:
    /**\brief Prepares the tree.
     *
     * This function should be called after build the object and before of the
     * function set_conn to have all the functionallity of the application.
     * \param "admin::editable_columns columns=admin::UNEDITABLE" columns to 
     * show.
     * \throws char* If is not set conn.*/
    void prepare(admin::editable_columns columns=admin::UNEDITABLE);
/*---------------------------------------------------------------------------*
 *                                                                           *
 *                             Events functions                              *
 *                                                                           *
 *---------------------------------------------------------------------------*/
     /**\brief Exit from the window.
      * 
      * This function is called when the user clic on the close window button.
      * \param "GdkEventAny *event" Event called.
      * \return false to close the window, true if not.*/
    bool on_exit_window_clic(GdkEventAny *event);

    /**\brief Exit from the window.
     * 
     * This function is called when the user clic on the action_quit action. */
    void on_action_exit();

    /**\brief Show the about dialog.
     * 
     * This function is called from the event of the action_about action.*/
    void on_action_about();

    /**\brief Show the print dialog.*/
    void on_action_print();

    /**\brief Show the preview dialog.*/
    void on_action_print_preview();

    /**\brief Config the page to print. */
    void on_action_page_setup();

    /**\brief Print to a pdf file.*/
    void on_action_export_pdf();

    /**\breif Saves the information captured.
     * 
     * Read from the spin-buttons and store the new information in the database
     * for this load the information by estadisticas::Estadisticas classes and
     * then uses their methods for update the information.*/
    void on_action_save();

/*---------------------------------------------------------------------------*
 *                                                                           *
 *                       Set & get functions                                 *
 *                                                                           *
 *---------------------------------------------------------------------------*/
    /**\brief Set the user.
     *
     * Also set the title in the window.
     * \param "catalogos::Usuario * user" User to be setted. */
    void set_user(catalogos::Usuario *user)
    {
        this->user = user;
        Glib::ustring title = _("Plan | Branch: ");
        title += user->getSucursal().getNombre() + _(" | Username: ");
        title += user->getNombre();
        set_title(title);
    }

    /**\brief Set the database connection.
     * \param "db::BaseDatos * conn" Database connection pointer.*/
    void set_conn(db::BaseDatos * conn)
    {
        this->conn = conn;
    }

    /**\brief Call the print dialog.
     * 
     * Function to call the dialog to print or to see the preview.
     * \param "Gtk::PrintOperationAction print_action" Flag to show the dialog. */
    void print_or_preview(Gtk::PrintOperationAction print_action);
    
    /*--------------------------Print events---------------------------------*/
    /**\brief This event is called when the print process present a change.
     * 
     * When there is a change this function is called.  
     * \param "const Glib::RefPtr<plan::PrintFormOperation>& operation"*/ 
    void on_print_operation_status_changed(
            const Glib::RefPtr<plan::PrintFormOperation>& operation);

    /**\brief This event is called when the print process is done.
     * 
     * When the print proccess is done this function is called.  
     * \param "const Glib::RefPtr<plan::PrintFormOperation>& operation"*/
    void on_printoperation_done(
            Gtk::PrintOperationResult result,
            const Glib::RefPtr<plan::PrintFormOperation>& operation);
private:
    /**\brief Set the date from the database. */
    void set_date();

    /**\brief Get the date generated by the set_date function.
     * \return the date setted by the set_date function.*/
    date get_date()
    {
        return m_date;
    }

    /**\brief Get the user.*/
    catalogos::Usuario * get_user()
    {
        return user;
    }

    /**\brief Get the database connection */
    db::BaseDatos * get_conn()
    {
        return conn;
    }
/*---------------------------------------------------------------------------*
 *                                                                           *
 *                       Member atributes                                    *
 *                                                                           *
 *---------------------------------------------------------------------------*/
protected:
    catalogos::Usuario *user;
    db::BaseDatos *conn;
    date m_date;;
    /* Widgets */
    TextViewStatistics *text_view;
    admin::ListReports *tree;
    Glib::RefPtr<Gtk::Action> action_about;
    Glib::RefPtr<Gtk::Action> action_print;
    Glib::RefPtr<Gtk::Action> action_print_preview;
    Glib::RefPtr<Gtk::Action> action_page_setup;
    Glib::RefPtr<Gtk::Action> action_export_pdf;
    Glib::RefPtr<Gtk::Action> action_quit;
    Glib::RefPtr<Gtk::Action> action_save;
    Glib::RefPtr<Gtk::Builder> builder;
    Gtk::SpinButton * spinarray[10];
    /* Print objects */
    Glib::RefPtr<Gtk::PageSetup> refPageSetup;
    Glib::RefPtr<Gtk::PrintSettings> refPrintSettings;
};

} // End of the namespace 
#endif // _WINDOW_CAPTURE_H_

