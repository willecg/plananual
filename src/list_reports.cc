/*
 * plananual: list_reports.cc
 * Widget for the creation of reports.
 *
 * Copyright © YEAR William Ernesto Cárdenas Gomez
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**\brief Namespace admin for the admin operations.
 *
 * Declares the gui for the admin operations of the plan proyect. May be on
 * follow versions will be integrated in plan namespace.*/
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "list_reports.h"
#include <iostream>
/**\brief Widget that shows information to make the reports.
 *
 * This inherits from the Gtk::TreeView class, have its own class model, and
 * its information is prepared to make a coincidence with the database
 * information.*/
namespace admin{

/*---------------------------------------------------------------------------*
 *                                                                           *
 *                Class List Reports Constructor & Destructor                *
 *                                                                           *
 *---------------------------------------------------------------------------*/
ListReports::ListReports(
        BaseObjectType *cobject, const Glib::RefPtr<Gtk::Builder> builder):
    Gtk::TreeView(cobject),
    builder(builder)
{
    list = Gtk::ListStore::create(model);
    set_model(list);
}

ListReports::~ListReports()
{}

/**\brief Gets the ListStore model from the treeview.
 *
 * You should set the year first.*/
Glib::RefPtr<Gtk::ListStore> ListReports::get_list()
{
    return list;
}

/**\brief Shows the columns.
 *
 * Show in the screen the columns corresponding to the acounts.
 * \param "const int column" or columns to show editables.*/
void ListReports::set_columns(editable_columns columns)
{
    remove_all_columns();
    append_column(_("Month"), model.month);
    switch(columns){
    case UNEDITABLE:
        append_column(_("Partners in"), model.partners_in);
        append_column(_("Partners out"), model.partners_out);
        append_column(_("Partners realized"), model.partners_realized);
        append_column(_("Partners budgeted"), model.partners_budgeted);
        append_column(_("Partners total"), model.partners_total);
        append_column(_("Children in"), model.children_in);
        append_column(_("Children out"), model.children_out);
        append_column(_("Children realized"), model.children_realized);
        append_column(_("Children budgeted"), model.children_budgeted);
        append_column(_("Children total"), model.children_total);
        append_column_numeric(_("Partner saving realized"),
                      model.partners_saving_realized, "%0.2f");
        append_column_numeric(_("Partner saving budgeted"),
                      model.partners_saving_budgeted, "%0.2f");
        append_column_numeric(_("Partner saving total"),
                      model.partners_saving_total, "%0.2f");
        append_column_numeric(_("Children saving realized"),
                      model.children_saving_realized, "%0.2f");
        append_column_numeric(_("Children saving budgeted"),
                      model.children_saving_budgeted, "%0.2f");
        append_column_numeric(_("Children saving total"),
                      model.children_saving_total, "%0.2f");
        append_column_numeric(_("Deposit realized"),
                      model.deposit_realized, "%0.2f");
        append_column_numeric(_("Deposit budgeted"),
                      model.deposit_budgeted, "%0.2f");
        append_column_numeric(_("Deposit total"),
                      model.deposit_total, "%0.2f");
        append_column_numeric(_("Fixed deposit realized"),
                      model.fixed_realized, "%0.2f");
        append_column_numeric(_("Fixed deposit budgeted"),
                      model.fixed_budgeted, "%0.2f");
        append_column_numeric(_("Fixed deposit total"),
                      model.fixed_total, "%0.2f");
        append_column_numeric(_("Loan realized"),
                      model.loan_realized, "%0.2f");
        append_column_numeric(_("Loan budgeted"),
                      model.loan_budgeted, "%0.2f");
        append_column_numeric(_("Loan total"), model.loan_total, "%0.2f");
        append_column_numeric(_("Defaulting realized"),
                      model.delay_realized, "%0.2f");
        append_column_numeric(_("Defaulting budgeted"),
                      model.delay_budgeted, "%0.2f");
        append_column_numeric(_("Defaulting total"),
                      model.delay_total, "%0.2f");
        break;
    case EDITABLE:
        append_column_editable(_("Partners in"), model.partners_in);
        append_column_editable(_("Partners out"), model.partners_out);
        append_column(_("Partners realized"), model.partners_realized);
        append_column(_("Partners budgeted"), model.partners_budgeted);
        append_column(_("Partners total"), model.partners_total);
        append_column_editable(_("Children in"), model.children_in);
        append_column_editable(_("Children out"), model.children_out);
        append_column(_("Children realized"), model.children_realized);
        append_column(_("Children budgeted"), model.children_budgeted);
        append_column(_("Children total"), model.children_total);
        append_column_numeric_editable(_("Partner saving realized"),
                      model.partners_saving_realized, "%0.2f");
        append_column_numeric(_("Partner saving budgeted"),
                      model.partners_saving_budgeted, "%0.2f");
        append_column_numeric(_("Partner saving total"),
                      model.partners_saving_total, "%0.2f");
        append_column_numeric_editable(_("Children saving realized"),
                      model.children_saving_realized, "%0.2f");
        append_column_numeric(_("Children saving budgeted"),
                      model.children_saving_budgeted, "%0.2f");
        append_column_numeric(_("Children saving total"),
                      model.children_saving_total, "%0.2f");
        append_column_numeric_editable(_("Deposit realized"),
                      model.deposit_realized, "%0.2f");
        append_column_numeric(_("Deposit budgeted"),
                      model.deposit_budgeted, "%0.2f");
        append_column_numeric(_("Deposit total"),
                      model.deposit_total, "%0.2f");
        append_column_numeric_editable(_("Fixed deposit realized"),
                      model.fixed_realized, "%0.2f");
        append_column_numeric(_("Fixed deposit budgeted"),
                      model.fixed_budgeted, "%0.2f");
        append_column_numeric(_("Fixed deposit total"),
                      model.fixed_total, "%0.2f");
        append_column_numeric_editable(_("Loan realized"),
                      model.loan_realized, "%0.2f");
        append_column_numeric(_("Loan budgeted"),
                      model.loan_budgeted, "%0.2f");
        append_column_numeric(_("Loan total"), model.loan_total, "%0.2f");
        append_column_numeric_editable(_("Defaulting realized"),
                      model.delay_realized, "%0.2f");
        append_column_numeric(_("Defaulting budgeted"),
                      model.delay_budgeted, "%0.2f");
        append_column_numeric(_("Defaulting total"),
                      model.delay_total, "%0.2f");
        break;
    }
}   

/**\brief Fill the list with the information.
 *
 * The data come from the database.
 * \param "const int month" number of the month 0 for all.
 * \param "const int year" Year for the statistic.
 * \param "db::BaseDatos *conn" Database connection.*/
void ListReports::fill_list_with_db_statistics
    (const int year, const std::string branch, db::BaseDatos *conn)
{
    list->clear();
    Gtk::TreeModel::iterator iter; 
    Gtk::TreeModel::Row row;
    int i_realized, i_budgeted, i_total, in, out;
    double d_realized, d_budgeted, d_total;
    for(int month = MINMONTH; month < MAXMONTH; month++){
        
        for(int j = static_cast<int>(estadisticas::SOCIOS);
             j <= static_cast<int>(estadisticas::MOROSIDAD);
             j++){
            estadisticas::TipoCuenta acount =
                    static_cast<estadisticas::TipoCuenta>(j);

            if(j <= 1){
                stats[month][j] =
                        new estadisticas::EstadisticaEnteros(
                                new catalogos::Sucursal(branch,conn),
                                year,
                                month,
                                acount,
                                conn);
                in = static_cast<estadisticas::EstadisticaEnteros*>(
                                stats[month][j])->getIngresos();
                out = static_cast<estadisticas::EstadisticaEnteros*>(
                                stats[month][j])->getRetiros();
                i_realized = static_cast<estadisticas::EstadisticaEnteros*>(
                                stats[month][j])->getRealizado();
                i_budgeted = static_cast<estadisticas::EstadisticaEnteros*>(
                                stats[month][j])->getPresupuesto();
                i_total = static_cast<estadisticas::EstadisticaEnteros*>(
                                stats[month][j])->getRealizado() -
                          static_cast<estadisticas::EstadisticaEnteros*>(
                                stats[month][j])->getPresupuesto();
            }
            else
            {
                stats[month][j] =
                        new estadisticas::EstadisticaReales(
                                new catalogos::Sucursal(branch,conn),
                                year,
                                month,
                                acount,
                                conn);
                d_realized = static_cast<estadisticas::EstadisticaReales*>(
                                stats[month][j])->getRealizado();
                d_budgeted = static_cast<estadisticas::EstadisticaReales*>(
                                stats[month][j])->getPresupuesto();
                d_total = static_cast<estadisticas::EstadisticaReales*>(
                                stats[month][j])->getRealizado() -
                          static_cast<estadisticas::EstadisticaReales*>(
                                stats[month][j])->getPresupuesto();
            }
            if(j == 0){
                iter = list->append();
                row = *iter;
                row[model.month] = month_name(month);
            }
            /*Load all data in the record so many to write.*/
            switch(acount){
            case estadisticas::SOCIOS:
                row[model.partners_in] = in;
                row[model.partners_out] = out;
                row[model.partners_realized] = i_realized;
                row[model.partners_budgeted] = i_budgeted;
                row[model.partners_total] = i_total;
                break;
            case estadisticas::MENORES:
                row[model.children_in] = in;
                row[model.children_out] = out;
                row[model.children_realized] = i_realized;
                row[model.children_budgeted] = i_budgeted;
                row[model.children_total] = i_total;
                break;
            case estadisticas::AHORROSOCIOS:
                row[model.partners_saving_realized] = d_realized;
                row[model.partners_saving_budgeted] = d_budgeted;
                row[model.partners_saving_total] = d_total;
                break;
            case estadisticas::AHORROMENORES:
                row[model.children_saving_realized] = d_realized;
                row[model.children_saving_budgeted] = d_budgeted;
                row[model.children_saving_total] = d_total;
                break;
            case estadisticas::DEPOSITOALAVISTA:
                row[model.deposit_realized] = d_realized;
                row[model.deposit_budgeted] = d_budgeted;
                row[model.deposit_total] = d_total;
                break;
            case estadisticas::PLAZOFIJO:
                row[model.fixed_realized] = d_realized;
                row[model.fixed_budgeted] = d_budgeted;
                row[model.fixed_total] = d_total;
                break;
            case estadisticas::PRESTAMO:
                row[model.loan_realized] = d_realized;
                row[model.loan_budgeted] = d_budgeted;
                row[model.loan_total] = d_total;
                break;
            case estadisticas::MOROSIDAD:
                row[model.delay_realized] = d_realized;
                row[model.delay_budgeted] = d_budgeted;
                row[model.delay_total] = d_total;
                break;
            default:
                throw _("Bad acount\nReport this bug.");
            } // Switch
        } // For. Iter acounts
    } // For. Iter the months
    
} // End of the function 
} // namespace end
