/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 4; tab-width: 4 -*-  */
/*
 * print-form-operation.h
 * Copyright (C) 2014 Pc0147 William <pc0147@pc0147>
 *
 * plananual-1.2 is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * plananual-1.2 is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _PRINT_FORM_OPERATION_H_
#define _PRINT_FORM_OPERATION_H_

#include <gtkmm/printoperation.h>
#include <gtkmm/liststore.h>
#include <pangomm.h>
#include <vector>

/**\brief Common operations.
 *
 * This is a group of common functions to diferent programs on this package.*/
namespace plan{
/**\brief Print the reports.
 * This class is the way to print by the Gtk interface print. */
class PrintFormOperation : public Gtk::PrintOperation 
{
public:
    /**\brief Create a reference to the PrintFormOperation.
     * 
     * This is the correct way to obtain it.
     * \return The reference to a new PrintFormOperation object.*/ 
    static Glib::RefPtr<PrintFormOperation> create();

    /**\breif Destructor*/
    virtual ~PrintFormOperation();

    /**\brief Set the listStore reference.
     * 
     * Set the correct value for a list.
     * \param "Glib::RefPtr<Gtk::ListStore> list" ListStore with the info.*/
    void set_list(Glib::RefPtr<Gtk::ListStore> list);

    /**\brief Set the branch name.
     * 
     * Set the correct value for the branch.
     * \param "const std::string branch_name" Branch name.*/
    void set_branch(const Glib::ustring& branch_name);

    /**\brief Set the year.
     * 
     * Set the year to print.
     * \param "const int year" Year to print.*/
    void set_year(const int year);
   
protected:
    /**\brief Default constructor.
     *
     * \see "static Glib::RefPtr<PrintFormOperation> create()"*/
    PrintFormOperation();

    /**\brief Virtual from the Gtk::PrintOperation.
     *
     * Prepares the print operation.*/
    virtual void on_begin_print(const Glib::RefPtr<Gtk::PrintContext>& context);

    /**\brief Virtual from the Gtk::PrintOperation.
     *
     * Makes the print on a cairo context.*/
    virtual void on_draw_page(
            const Glib::RefPtr<Gtk::PrintContext>& context, int page_nr);

    /*elements*/
    Glib::RefPtr<Gtk::ListStore> list;
    std::string branch;
    int year;

    Glib::RefPtr<Pango::Layout> m_refLayout;
    std::vector<int> m_PageBreaks; // line numbers where a page break occurs
};
} // Namespace end
#endif // _PRINT_FORM_OPERATION_H_

