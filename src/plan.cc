/*
 * plananual: catalogos.h
 * Control de catalogos.
 *
 * Copyright © 2014 William Ernesto Cárdenas Gomez
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <iostream>

#include "plan.h"
#include "../lib/baseDatos.h"

/* The porpose of the following two macros is to get the configuration data for
 * the database connection. */
#define CFG_FILE SYS_CONF_DIR"/plan/plan.xml" // Production environment
//#define CFG_FILE "etc/plan.xml"          // Development environment

/**\brief Common operations.
 *
 * This is a group of common functions to diferent programs on this package.*/
namespace plan{

/**\brief information about connection operation for log_in function.
 *
 * Store information needed by the db::BaseDatos constructor. To perform a
 * database connection */
connection_data conn_info;

/**\brief Function that perform a database test of login operation. 
 *
 * When the operation is succesfull this returns a true value. But, if the
 * operation isn't good then the third param is loaded with a message with
 * error. This is needed by the login::Login Class.
 *
 * @param "const std::string *const usr" User name.
 * @param "const std::string *const pass" User password
 * @param "std::std::string *errores" Set it with the mistakes generated during
 * the operacion.
 *
 * @return True if the operation is successful, false otherwise. */
bool log_in(
        const std::string *const usr,
        const std::string *const pass,
        std::string *errores)
{
    db::BaseDatos bd(*usr, *pass,
            conn_info.host, conn_info.data_base, conn_info.port);
    bool retorno;
    if (bd.ejecutaQuery("SELECT 1;"))
        return true;
    *errores = bd.getLastError();
    return false;
}

/**\brief this function make a login operation.
 *
 * This function call the login library and get the result of the login
 * operation. If the operation is good, then it set a global connection_data
 * variable.
 *
 * \param "login::Login *l" Login class, Perform a GTK login operation.
 * \param "connection_data * const conn_data" Store connection parameters.
 * \return If operation successful True. Otherwise False. */
bool perform_login(login::Login *l, connection_data *const conn_data)
{
    conn_info = *conn_data;
    l->open_window();
    if(l->get_errores() == "--EXITO--") {
        conn_data->user = l->get_user();
        conn_data->password = l->get_password();
        l->close_window();
        delete l;
        return true;
    }
    delete l;
    return false;
}

/**\brief Return the values to set the network connection to db.
 *
 * Search in some places and when find the planconfig.ini file read from this
 * the parameters to entablish a data base session. If the file isn't found an
 * exception is generated.
 *
 * \return A filled connection_data. */
connection_data load_struct()
{
    std::locale::global(std::locale(""));
    connection_data info;
    info.host = return_data("hostname", CFG_FILE).raw();
    info.data_base = return_data("dbname", CFG_FILE).raw();
    info.port = return_data("port", CFG_FILE).raw();
    if (info.host == "")
        info.host = "ERROR";
    return info;
}

/**\brief Get a node indicated by the info param.
 *
 * A node and data is received. Then it searches the xml tree until the search
 * data is found or reaches the end of the xml structure. 
 *
 * \param "xmlpp:Node * node" Root node. Store the xml structure.
 * \param "Glib::ustring info" Node name to search.
 *
 * \return The first child of the node founded or NULL if search fails.*/
xmlpp::Node * get_node_baseDatos(xmlpp::Node *node, Glib::ustring info)
{
    if (node->get_name() == info) {
        return *node->get_children().begin();
    }

    xmlpp::Node::NodeList list = node->get_children();
    for(xmlpp::Node::NodeList::iterator iter = list.begin();
            iter != list.end(); ++iter) {
        xmlpp::Node *tmp_node = get_node_baseDatos(*iter, info);
        if (tmp_node != NULL)
            return tmp_node;
    }
    return NULL;
}

/**\brief Searches a value on a xml file.
 * 
 * If the value is found then its content is returned otherwise an empty
 * Glib::ustring is returned.
 *
 * \param "Glib::ustring key" Key to search.
 * \param "Glib::ustring file" File path.
 *
 * \return Empty Glib::ustring if the key ist'n found otherwise the value of
 * this key. */
Glib::ustring return_data(Glib::ustring key, Glib::ustring file)
{
    Glib::ustring data = "";
    try {
        xmlpp::DomParser parser;
        //parser.set_validate(true);
        parser.set_throw_messages(true);
        parser.parse_file(file);
        xmlpp::Node *pNode = parser.get_document()->get_root_node();
        xmlpp::Node *db_node = get_node_baseDatos(pNode, key);
    
        if (db_node != NULL) {
            xmlpp::ContentNode *cont =
                dynamic_cast<xmlpp::ContentNode*>(db_node);
            xmlpp::TextNode *text =
                dynamic_cast<xmlpp::TextNode*>(db_node);
            xmlpp::CommentNode *comm =
                dynamic_cast<xmlpp::CommentNode*>(db_node);
            if (cont)
                data = cont->get_content();
            else if (text)
                data = text->get_content();
            else if (comm)
                data = comm->get_content();
        }
    }
    catch(const std::exception& e) {
        std::cerr << _("Exception caught: ") << e.what() << std::endl;
        data = "";
    }
    return data;
}

} // namespace end. plan.
