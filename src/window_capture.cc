
/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 4; tab-width: 4 -*-  */
/*
 * window_capture.cc
 * Copyright (C) 2014 William Ernesto Cárdenas Gómez <sistemas.will@alianzacpa.com.mx>
 *
 * plananual-1.2 is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * plananual-1.2 is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <gtkmm/actiongroup.h>
#include <gtkmm/messagedialog.h>
#include <gtkmm/aboutdialog.h>
#include <gtkmm/filechooserdialog.h>
#include <iostream>

#include "window_capture.h"
#include "estadisticas/estadistica.h"
#include "estadisticas/estadistica_reales.h"
#include "estadisticas/estadistica_enteros.h"

#define UI_ABOUT PACKAGE_DATA_DIR"/ui/acerca_de.glade"
/* This macros is for the development environment */
//#define UI_ABOUT "gui/acerca_de.glade"

/**\brief Namespace to capture the branch advance.
 * 
 * This namespace is used to the plancaptura program.*/
namespace capture{
/*---------------------------------------------------------------------------*
 *                                                                           *
 *                      Constructor & Destructor                             *
 *                                                                           *
 *---------------------------------------------------------------------------*/
/**\brief Constructor.
 * 
 * Prepares the interface. This is designed to be used by the
 * get_derived_widget method form Gtk::Builder.
 * \param "BaseObjectType cobject" Wraper of a C object.
 * \param "const Glib::RefPtr<Gtk::Builder>& builder" Reference to a
 * builder object.*/
WindowCapture::WindowCapture(
    BaseObjectType* cobject, const Glib::RefPtr<Gtk::Builder>& builder):
         Gtk::ApplicationWindow(cobject),
         builder(builder),
         tree(0),
         user(0),
         text_view(0)
{
    builder->get_widget_derived ("listview", tree);
    builder->get_widget_derived("textviewestadisticas", text_view);
    builder->get_widget("sb_partners_in", spinarray[0]);
    builder->get_widget("sb_partners_out", spinarray[1]);
    builder->get_widget("sb_children_in", spinarray[2]);
    builder->get_widget("sb_children_out", spinarray[3]);
    builder->get_widget("sb_partner_saving", spinarray[4]);
    builder->get_widget("sb_children_saving", spinarray[5]);
    builder->get_widget("sb_deposit_demand", spinarray[6]);
    builder->get_widget("sb_fixed", spinarray[7]);
    builder->get_widget("sb_loan", spinarray[8]);
    builder->get_widget("sb_defaulting", spinarray[9]);

    /* Actions */
    action_about = Glib::RefPtr<Gtk::Action>::cast_dynamic(
            builder->get_object("action_about"));
    action_print = Glib::RefPtr<Gtk::Action>::cast_dynamic(
            builder->get_object("action_print"));
    action_print_preview = Glib::RefPtr<Gtk::Action>::cast_dynamic(
            builder->get_object("action_print_preview"));
    action_page_setup = Glib::RefPtr<Gtk::Action>::cast_dynamic(
            builder->get_object("action_page_setup"));
    action_export_pdf = Glib::RefPtr<Gtk::Action>::cast_dynamic(
            builder->get_object("action_export_pdf"));
    action_quit = Glib::RefPtr<Gtk::Action>::cast_dynamic(
            builder->get_object("action_quit"));
    action_save = Glib::RefPtr<Gtk::Action>::cast_dynamic(
            builder->get_object("action_save"));

    load_events();
}

/**Destructor*/
WindowCapture::~WindowCapture()
{
    delete(user);
}

/*---------------------------------------------------------------------------*
 *                                                                           *
 *                      Member functions miscelanius                         *
 *                                                                           *
 *---------------------------------------------------------------------------*/
/**\brief Load the events.
 * 
 * This method call the events for the constructor.*/
void WindowCapture::load_events()
{
    Glib::RefPtr<Gtk::ActionGroup> action_group = Gtk::ActionGroup::create();
    action_group->add(
        action_about, sigc::mem_fun(*this, &WindowCapture::on_action_about));
    action_group->add(
        action_print, sigc::mem_fun(*this, &WindowCapture::on_action_print));
    action_group->add(
        action_print_preview, sigc::mem_fun(*this, 
                                    &WindowCapture::on_action_print_preview));
    action_group->add(
        action_page_setup, sigc::mem_fun(*this,
                                    &WindowCapture::on_action_page_setup));
    action_group->add(
        action_export_pdf, sigc::mem_fun(*this,
                                    &WindowCapture::on_action_export_pdf));
    action_group->add(
        action_quit, sigc::mem_fun(*this, &WindowCapture::on_action_exit));
    action_group->add(
        action_save, sigc::mem_fun(*this, &WindowCapture::on_action_save));
    
    signal_delete_event().connect(
        sigc::mem_fun(*this, &WindowCapture::on_exit_window_clic));
}

/**\brief Prepares the tree.
 *
 * This function should be called after build the object and before of the
 * function set_conn to have all the functionallity of the application.
 * \param "admin::editable_columns columns=admin::UNEDITABLE" columns to 
 * show.
 * \throws char* If is not set conn.*/
void WindowCapture::prepare(admin::editable_columns columns)
{
    Gtk::MessageDialog *error_dialog = 
        new Gtk::MessageDialog(*this, _("Start up error"), false,
                               Gtk::MESSAGE_ERROR);
            tree->set_columns(columns);
        set_date();
        text_view->set_branch (user->getSucursal().getNombre());
        text_view->set_year(get_date().year);
        text_view->set_text(get_date().month, get_date().day, conn);
    try{
        tree->fill_list_with_db_statistics(
                m_date.year, user->getSucursal().getNombre(), conn);
    }
    catch(char *ex){
        error_dialog->set_secondary_text(ex);
        error_dialog->run();
    }
    delete(error_dialog);
}

/**\brief Set the date from the database. */
void WindowCapture::set_date()
{
    conn->ejecutaQuery("SELECT date_part('year', CURRENT_DATE),\
    date_part('month', CURRENT_DATE), date_part('day', CURRENT_DATE)");
    m_date.year = std::stoi(conn->regresaDato(0,0));
    m_date.month = std::stoi(conn->regresaDato(0,1));
    m_date.day = std::stoi(conn->regresaDato(0,2));
}

/*---------------------------------------------------------------------------*
 *                                                                           *
 *                             Events functions                              *
 *                                                                           *
 *---------------------------------------------------------------------------*/
/**\brief Exit from the window.
 * 
 * This function is called when the user clic on the close window button.
 * \param "GdkEventAny *event" Event called.
 * \return false to close the window, true if not.*/
bool WindowCapture::on_exit_window_clic(GdkEventAny *event)
{
     Gtk::MessageDialog dialog(*this, _("Dou you really want to close it?"),
                             false, Gtk::MESSAGE_QUESTION,
                             Gtk::BUTTONS_OK_CANCEL);
     return (dialog.run() == Gtk::RESPONSE_OK? false: true);
}

/**\brief Exit from the window.
 * 
 * This function is called when the user clic on the action_quit action. */
void WindowCapture::on_action_exit()
{
    Gtk::MessageDialog dialog(*this, _("Dou you really want to close it?"),
                             false, Gtk::MESSAGE_QUESTION,
                             Gtk::BUTTONS_OK_CANCEL);
     if(dialog.run() == Gtk::RESPONSE_OK)
        hide();
}

/**\brief Show the about dialog.
 * 
 * This function is called from the event of the action_about action.*/
void WindowCapture::on_action_about()
{
    builder->add_from_file(UI_ABOUT);
    Gtk::AboutDialog * dialog = 0;
    builder->get_widget("aboutdialog", dialog);
    dialog->set_transient_for(*this);
    dialog->run();
}

/**\brief Show the print dialog.*/
void WindowCapture::on_action_print()
{
    print_or_preview(Gtk::PRINT_OPERATION_ACTION_PRINT_DIALOG);
}

/**\brief Show the preview dialog.*/
void WindowCapture::on_action_print_preview()
{
    print_or_preview(Gtk::PRINT_OPERATION_ACTION_PREVIEW);
}

/**\brief Config the page to print. */
void WindowCapture::on_action_page_setup()
{
    //Show the page setup dialog, asking it to start with the existing settings
    Glib::RefPtr<Gtk::PageSetup> new_page_setup =
        Gtk::run_page_setup_dialog(
                *this, refPageSetup, refPrintSettings);
    //Save the chosen page setup dialog for use when printing, previewing, or
    //showing the page setup dialog again:
    refPageSetup = new_page_setup;
}

/**\brief Print to a pdf file.*/
void WindowCapture::on_action_export_pdf()
{
     print_or_preview (Gtk::PRINT_OPERATION_ACTION_EXPORT);
}

/**\breif Saves the information captured.
 * 
 * Read from the spin-buttons and store the new information in the database
 * for this load the information by estadisticas::Estadisticas classes and
 * then uses their methods for update the information.*/
void WindowCapture::on_action_save()
{
    Gtk::MessageDialog message(
        *this,
        _("Are you really sure?"),
        false,
        Gtk::MESSAGE_QUESTION,
        Gtk::BUTTONS_YES_NO);
    if(message.run() == Gtk::RESPONSE_YES)
    {
        try
        {
            estadisticas::Estadistica * stat;
            catalogos::Sucursal branch = get_user()->getSucursal();
            int month = get_date().month, year = get_date().year;
            typedef estadisticas::EstadisticaEnteros Integers;
            typedef estadisticas::EstadisticaReales Reals;
            for(int i = static_cast<int>(estadisticas::SOCIOS);
                    i <= static_cast<int>(estadisticas::MOROSIDAD); i++)
            {
                if(i <= static_cast<int>(estadisticas::MENORES))
                {
                    int ing = (i == estadisticas::SOCIOS ? i: 2 );
                    if(spinarray[ing]->get_value_as_int() != 0 ||
                       spinarray[ing + 1]->get_value_as_int() != 0)
                    {
                        stat = new Integers(
                                   &branch, year, month,
                                   static_cast<estadisticas::TipoCuenta>(i), conn);
                        static_cast<Integers*>(stat)->modificarRealizado(
                            spinarray[ing]->get_value_as_int(),
                            spinarray[ing + 1]->get_value_as_int(),
                            conn);
                    } //inner if
                }
                else
                {
                    if(spinarray[i+2]->get_value() != 0)
                    {
                        stat = new Reals(
                                   &branch, year, month,
                                   static_cast<estadisticas::TipoCuenta>(i), conn);
                        static_cast<Reals*>(stat)->modificarRealizado(
                            spinarray[i+2]->get_value(), conn);
                    } // Inner if
                } // most extern If...else 
            } // For
        text_view->set_text(get_date().month, get_date().day, conn);
        tree->fill_list_with_db_statistics(
            m_date.year, user->getSucursal().getNombre(), conn);
        }
        catch(char *ex)
        {
            Gtk::MessageDialog(*this, ex, false, Gtk::MESSAGE_ERROR);
        }
        catch(std::string ex)
        {
            Gtk::MessageDialog(*this, ex, false, Gtk::MESSAGE_ERROR);  
        }
        catch(std::exception &ex){
            Gtk::MessageDialog(*this, ex.what(), false, Gtk::MESSAGE_ERROR);
        }
    } // End of external if
}

/**\brief Call the print dialog.
 * 
 * Function to call the dialog to print or to see the preview.
 * \param "Gtk::PrintOperationAction print_action" Flag to show the dialog. */
void WindowCapture::print_or_preview(Gtk::PrintOperationAction print_action)
{
    Glib::RefPtr<plan::PrintFormOperation> print = 
        plan::PrintFormOperation::create();
    print->set_list(tree->get_list());
    print->set_year(get_date().year);
    print->set_branch(get_user()->getSucursal().getNombre());

    print->set_track_print_status();
    print->set_default_page_setup(refPageSetup);
    print->set_print_settings(refPrintSettings);

    print->signal_done().connect(sigc::bind(sigc::mem_fun(*this,
        &WindowCapture::on_printoperation_done), print));
    
    int choose_dialog_button = Gtk::RESPONSE_OK;
    if(print_action == Gtk::PRINT_OPERATION_ACTION_EXPORT){
        Gtk::FileChooserDialog filechooser(_("Select a file please."),
                Gtk::FILE_CHOOSER_ACTION_SAVE);
        filechooser.set_transient_for(*this);
        filechooser.add_button(_("Save"), Gtk::RESPONSE_OK);
        filechooser.add_button(_("Cancel"), Gtk::RESPONSE_CANCEL);

        Glib::RefPtr<Gtk::FileFilter> file_filter = Gtk::FileFilter::create();
        file_filter->set_name(_("Pdf file"));
        file_filter->add_mime_type(".pdf");
        filechooser.add_filter(file_filter);
        
        if((choose_dialog_button = filechooser.run()) == Gtk::RESPONSE_OK)
            print->set_export_filename(filechooser.get_filename());
            
    }
    if(choose_dialog_button == Gtk::RESPONSE_OK)
        try
        {
            print->run(print_action, *this);
        }
        catch (const Gtk::PrintError& ex)
        {
            //See documentation for exact Gtk::PrintError error codes.
            Gtk::MessageDialog msg(
                *this,
                "An error occurred while trying to run a print operation:",
                false,
                Gtk::MESSAGE_ERROR);
            msg.set_secondary_text(ex.what());
            msg.run();
        }
}

/**\brief This event is called when the print process present a change.
 * 
 * When there is a change this function is called.  
 * \param "const Glib::RefPtr<plan::PrintFormOperation>& operation"*/
void WindowCapture::on_print_operation_status_changed(
            const Glib::RefPtr<plan::PrintFormOperation>& operation)
{
    Glib::ustring status_msg;
    if (operation->is_finished())
        status_msg = _("Print job completed.");
    else
        //You could also use get_status().
        status_msg = operation->get_status_string();

    Gtk::MessageDialog msg_status(*this, status_msg);
    msg_status.run();
}

/**\brief This event is called when the print process is done.
 * 
 * When the print proccess is done this function is called.  
 * \param "const Glib::RefPtr<plan::PrintFormOperation>& operation"*/
void WindowCapture::on_printoperation_done(
            Gtk::PrintOperationResult result,
            const Glib::RefPtr<plan::PrintFormOperation>& operation)
{
    //Printing is "done" when the print data is spooled.
    if (result == Gtk::PRINT_OPERATION_RESULT_ERROR){
        Gtk::MessageDialog err_dialog(
                *this, "Error printing form", false, Gtk::MESSAGE_ERROR,
                Gtk::BUTTONS_OK, true);
        err_dialog.run();
    }
    else
        if(result == Gtk::PRINT_OPERATION_RESULT_APPLY){
            //Update PrintSettings with the ones used in this PrintOperation:
            refPrintSettings = operation->get_print_settings();
        }

    if (!operation->is_finished()){
        //We will connect to the status-changed signal to track status
        //and update a status bar. In addition, you can, for example,
        //keep a list of active print operations, or provide a progress dialog.
        operation->signal_status_changed().connect(
            sigc::bind(
                sigc::mem_fun(
                    *this,
                    &WindowCapture::on_print_operation_status_changed),
                operation));
    }
}
} // Namespace end

