/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 4; tab-width: 4 -*-  */
/*
 * text-view-statistics.cc
 * Copyright (C) 2014 William E. Cardenas Gomez <sistemas.will@alianzacpa.com.mx>
 *
 * plananual-1.2 is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * plananual-1.2 is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "text-view-statistics.h"
#include <iostream>
#include <iomanip>

/**\brief Namespace to capture the branch advance.
 * 
 * This namespace is used to the plancaptura program.*/
namespace capture {
/**\brief Default constructor.
 *
 * This should only used by the function get_derived_widget of the 
 * object Gtk::Builder.
 * \param "BaseObjectType * cobject" Reference to the object.
 * \param "Glib::RefPtr<Gtk::Builder> builder" Smart pointer to the builder
 *        object.*/
TextViewStatistics::TextViewStatistics(BaseObjectType *cobject,
                                       Glib::RefPtr<Gtk::Builder> builder) :
    TextView(cobject),
    branch(""),
    year(0)
{
    ref_tag_default = Gtk::TextBuffer::Tag::create();
    ref_tag_default->property_font() = "Mono 8";
    ref_tag_header = Gtk::TextBuffer::Tag::create ();
    ref_tag_header->property_font() = "Sans Bold 10";
    ref_tag_header2 = Gtk::TextBuffer::Tag::create ();
    ref_tag_header2->property_font() = "Sans 10";
    ref_tag_table_header = Gtk::TextBuffer::Tag::create ();
    ref_tag_table_header->property_font() = "Mono Bold 8";

    ref_tag_table = Gtk::TextBuffer::TagTable::create();
    ref_tag_table->add(ref_tag_default);
    ref_tag_table->add(ref_tag_header);
    ref_tag_table->add(ref_tag_header2);
    ref_tag_table->add(ref_tag_table_header);

    buffer = Gtk::TextBuffer::create(ref_tag_table);
}

/**Destructor.*/
TextViewStatistics::~TextViewStatistics()
{
    // TODO: Add implementation here
}

/**\brief Make a query for the database.
 *
 * Makes a particular query for fill the data in the start of this application.
 * \param "estadisticas::TipoCuenta acount" Type of acount.
 * \param "Glib::ustring branch" Branch to get the information.
 * \param "int year" year to get information.
 * \return An Glib::ustring with the query. */
Glib::ustring 
TextViewStatistics::get_query(
        estadisticas::TipoCuenta acount,
        Glib::ustring branch,
        int year)
{
    if(year == 0 || branch == "")
        return "";
    
    Glib::ustring query = ( static_cast<int>(acount) <= 1 ?
            "SELECT sum(presupuesto), (sum(ingreso) - sum(retiro)) FROM ":
            "SELECT sum(presupuesto), sum(realizado) FROM ");
    query += estadisticas::tablas[static_cast<int>(acount)];
    query += " c INNER JOIN public.sucursales s ON s.id_sucursal = c.sucursal "
             "WHERE s.sucursal = '" + branch + "' AND anyo = ";
    query.append(std::to_string(year)); 
    return query;
}

/**\brief Set the text on the Gtk::TextBuffer.
 *
 * Prepares the Gtk::TextBuffer, iter on all the kinds of acounts, and show
 * the information on the textview.*/
void TextViewStatistics::set_text(int month, int day, db::BaseDatos * conn)
{
    buffer->set_text("");
    buffer->insert_with_tag(buffer->end(), _("Branch: "), ref_tag_header);
    buffer->insert_with_tag(buffer->end(), get_branch(), ref_tag_header2);
    buffer->insert_with_tag(buffer->end(), _("\nDate: "), ref_tag_header);
    buffer->insert_with_tag(
                         buffer->end(),
                         std::to_string(day) +
                         "/" + std::to_string(month) +
                         "/" + std::to_string(get_year()) + "\n\n",
                         ref_tag_header2);

    Glib::ustring text = 
        Glib::ustring::format(std::left, std::setw(21), _("Acount"));
    text += Glib::ustring::format(std::right, std::setw(12), _("Advance"));
    text += Glib::ustring::format(std::right, std::setw(12), _("Budget"));
    text += "\n";
    buffer->insert_with_tag(buffer->end(), text, ref_tag_table_header);
    
    
    try{
        const std::string cadenas_cuentas[] =
            {_("Partners"), _("Children"), _("Saving partners"),
            _("Saving childrens"), _("Demand deposit"), _("Fixed deposit"),
            _("Loans"), _("% of delay on payment")
        };
        for(int ac = estadisticas::SOCIOS;
            ac <= estadisticas::MOROSIDAD; ac++ ){
            conn->ejecutaQuery(
                      get_query(static_cast<estadisticas::TipoCuenta>(ac),
                              get_branch(), get_year()));
            text = Glib::ustring::format(
                        std::left,
                        std::setw(21),
                        cadenas_cuentas[ac]);
            text += Glib::ustring::format(
                        std::right,
                        std::setw(12),
                        (conn->regresaDato(0,1)=="" ? "0" :
                             conn->regresaDato(0,1)));
            text += Glib::ustring::format(
                        std::right,
                        std::setw(12),
                        conn->regresaDato(0,0)) + "\n";
            std::cout << cadenas_cuentas[ac] << " ";
            buffer->insert_with_tag(buffer->end(), text, ref_tag_default);
        } // For end
        set_buffer (buffer);
    } // Try
    catch(std::string s){
        std::cerr << s;
    }
    catch(char *s){
        std::cerr << s;
    } // Try...catch
} // End of function

} // end of namespace