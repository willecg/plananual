/*
 * plananual: plan.h
 * Control de catalogos.
 *
 * Copyright © 2014 William Ernesto Cárdenas Gomez
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef HAVE_PLAN_H
#define HAVE_PLAN_H

#include "../lib/login.h"
#include <libxml++/libxml++.h>


/**\brief Common operations.
 *
 * This is a group of common functions to diferent programs on this package. */
namespace plan{

/**\brief Information about the database connection.
 *
 * This struct store all database connection information.*/
struct connection_data{
    std::string host; /**<Host name or ip addres for the server */
    std::string data_base; /**<Data base name.*/
    std::string port; /**<Network port.*/
    std::string user; /**<User name*/
    std::string password; /**<User password*/
};

/**\brief Function that perform a database test of login operation. 
 *
 * When the operation is succesfull this returns a true value. But, if the
 * operation isn't good then the third param is loaded with a message with
 * error. This is needed by the login::Login Class.
 *
 * @param "const std::string *const usr" User name.
 * @param "const std::string *const pass" User password
 * @param "std::std::string *errores" Set it with the mistakes generated during
 * the operacion.
 *
 * @return True if the operation is successful, false otherwise.*/
bool log_in(const std::string *const, const std::string *const, std::string*);

/**\brief This function make a login operation.
 *
 * This function call the login library and get the result of the login
 * operation. If the operation is good, then it set a global connection_data
 * variable.
 *
 * \param "login::Login *l" Login class, Perform a GTK login operation.
 * \param "connection_data * const conn_data" Store connection parameters.
 *
 * \return If operation successful True. Otherwise False. */
bool perform_login(login::Login* l, connection_data *const conn_data);

/**\brief Return the values to set the network connection to db.
 *
 * Search in some places and when find the planconfig.ini file read from this
 * the parameters to entablish a data base session. If the file isn't found an
 * exception is generated.
 *
 * \return A filled connection_data. */
connection_data load_struct();

/**\brief Get a node indicated by the info param.
 *
 * A node and data is received. Then it searches the xml tree until the search
 * data is found or reaches the end of the xml structure. 
 *
 * \param "xmlpp:Node * node" Root node. Store the xml structure.
 * \param "Glib::ustring info" Node name to search.
 *
 * \return The first child of the node founded or NULL if search fails.*/
xmlpp::Node * get_node(xmlpp::Node *node, Glib::ustring info);

/**\brief Searches a value on a xml file.
 * 
 * If the value is found then its content is returned otherwise an empty
 * Glib::ustring is returned.
 *
 * \param "Glib::ustring key" Key to search.
 * \param "Glib::ustring file" File path.
 *
 * \return Empty Glib::ustring if the key ist'n found otherwise the value of
 * this key. */
Glib::ustring return_data(Glib::ustring key, Glib::ustring file);

} // Namespace end. plan.

#endif // HAVE_PLAN_H
