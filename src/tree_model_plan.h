#ifndef TREE_MODEL_PLAN
#define TREE_MODEL_PLAN
/*
 * plananual: tree_model_plan.h
 * Set a default model for the treeviews.
 *
 * Copyright © 2014 William Ernesto Cárdenas Gomez
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <gtkmm/treemodelcolumn.h>
namespace plan{
/**\brief Struct with name and title of the column.
 *
 * This can be for helps to set the title of the columns.*/

/**\brief Set a model for Gtk::ListStore.
 *
 * That includes all the options for the boudget.*/
class TreeModelPlan : public Gtk::TreeModelColumnRecord{
public:
    TreeModelPlan()
    {
        add(month);
        add(partners_in);
        add(partners_out);
        add(partners_realized);
        add(partners_budgeted);
        add(partners_total);
        add(children_in);
        add(children_out);
        add(children_realized);
        add(children_budgeted);
        add(children_total);
        add(partners_saving_realized);
        add(partners_saving_budgeted);
        add(partners_saving_total);
        add(children_saving_realized);
        add(children_saving_budgeted);
        add(children_saving_total);
        add(deposit_realized);
        add(deposit_budgeted);
        add(deposit_total);
        add(fixed_realized);
        add(fixed_budgeted);
        add(fixed_total);
        add(loan_realized);
        add(loan_budgeted);
        add(loan_total);
        add(delay_realized);
        add(delay_budgeted);
        add(delay_total);
    }

    /* Columns from the model */
    Gtk::TreeModelColumn<Glib::ustring> month;
    Gtk::TreeModelColumn<int> partners_in;
    Gtk::TreeModelColumn<int> partners_out;
    Gtk::TreeModelColumn<int> partners_realized;
    Gtk::TreeModelColumn<int> partners_budgeted;
    Gtk::TreeModelColumn<int> partners_total;
    Gtk::TreeModelColumn<int> children_in;
    Gtk::TreeModelColumn<int> children_out;
    Gtk::TreeModelColumn<int> children_realized;
    Gtk::TreeModelColumn<int> children_budgeted;
    Gtk::TreeModelColumn<int> children_total;
    Gtk::TreeModelColumn<double> partners_saving_realized;
    Gtk::TreeModelColumn<double> partners_saving_budgeted;
    Gtk::TreeModelColumn<double> partners_saving_total;
    Gtk::TreeModelColumn<double> children_saving_realized;
    Gtk::TreeModelColumn<double> children_saving_budgeted;
    Gtk::TreeModelColumn<double> children_saving_total;
    Gtk::TreeModelColumn<double> deposit_realized;
    Gtk::TreeModelColumn<double> deposit_budgeted;
    Gtk::TreeModelColumn<double> deposit_total;
    Gtk::TreeModelColumn<double> fixed_realized;
    Gtk::TreeModelColumn<double> fixed_budgeted;
    Gtk::TreeModelColumn<double> fixed_total;
    Gtk::TreeModelColumn<double> loan_realized;
    Gtk::TreeModelColumn<double> loan_budgeted;
    Gtk::TreeModelColumn<double> loan_total;
    Gtk::TreeModelColumn<double> delay_realized;
    Gtk::TreeModelColumn<double> delay_budgeted;
    Gtk::TreeModelColumn<double> delay_total;
};
}
#endif
