#ifndef LIST_REPORTS_H
#define LIST_REPORTS_H
/*
 * plananual: list_reports.h
 * Widget for the creation of reports.
 *
 * Copyright © YEAR William Ernesto Cárdenas Gomez
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**\brief Namespace admin for the admin operations.
 *
 * Declares the gui for the admin operations of the plan proyect. May be on
 * follow versions will be integrated in plan namespace.*/
#include <gtkmm/builder.h>
#include <gtkmm/treeview.h>
#include <gtkmm/liststore.h>
#include <gtkmm/treemodelcolumn.h>
#include <gtkmm/comboboxtext.h>

#include "tree_model_plan.h"
#include "estadisticas/estadistica.h"
#include "estadisticas/estadistica_enteros.h"
#include "estadisticas/estadistica_reales.h"
#include "list_capture.h"
#include "../lib/baseDatos.h"
namespace admin{

/**\brief Enum about how to build the treview columns.
  * If set as EDITABLE the user will can write on some columns.*/
enum editable_columns{
     EDITABLE, /**<Allows to write in some columns.*/
     UNEDITABLE/**<No writeable columns.*/
};

/**\brief Return the name of the month from a number.
 * 
 * The avalibles numbers are from 1 to 12.
 * \param "int index" The index of the month 1..12.
 * \return std::string with the name of the month.*/
inline std::string month_name(int index)
{
    std::string months [] = 
      { _("January"), _("February"), _("March"), _("April"), _("May"),
        _("June"), _("July"), _("August"), _("September"), _("October"),
        _("November"), _("December") 
      };
    return (index <= 12 || index >= 1 ? months[index - 1] : "");
}
/*---------------------------------------------------------------------------*
 *                                                                           *
 *                            Class ListReports                              *
 *                                                                           *
 *---------------------------------------------------------------------------*/
/**\brief Widget that shows information to make the reports.
 *
 * This inherits from the Gtk::TreeView class, have its own class model, and
 * its information is prepared to make a coincidence with the database
 * information.*/
class ListReports : public Gtk::TreeView{
    
public:
    /**\brief Constructor.
     *
     * Prepares the interface.*/
    ListReports(BaseObjectType *cobject,
            const Glib::RefPtr<Gtk::Builder> builder);
    /**Destructor*/
    virtual ~ListReports();

    /**\brief Gets the ListStore model from the treeview.
     *
     * You should set the year first.*/
    Glib::RefPtr<Gtk::ListStore> get_list();
    
    /**\brief Shows the columns.
     *
     * Show in the screen the columns corresponding to the acounts.
     * \param "editable_columns columns" or columns to show editables.*/
    void set_columns(editable_columns columns);

     /**\brief Fill the list with the information.
      *
      * The data come from the database.
      * \param "const int month" number of the month 0 for all.
      * \param "const int year" Year for the statistic.
      * \param "db::BaseDatos *conn" Database connection.*/
    void fill_list_with_db_statistics
       (const int year, const std::string branch, db::BaseDatos *conn);

    
private:
    Glib::RefPtr<Gtk::ListStore> list;
    Glib::RefPtr<Gtk::Builder> builder;
    plan::TreeModelPlan model;
    Gtk::ComboBoxText *p_textbox;

    int columns_to_show;
    estadisticas::Estadistica *stats[MAXMONTH][STATISTICSNUM];
};
}
#endif
