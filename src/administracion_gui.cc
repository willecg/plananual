/*
 * planadmin: administracion-gui.cc
 * Gui for the administration.
 *
 * Copyright © 2014 William Ernesto Cárdenas Gomez
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**\brief Control of the admin of the statistics.
 *
 * This namespace store the gui and operation of the plan admin program.*/
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif //-HAVE_CONFIG_H

#include <glibmm/i18n.h>

#include <gtkmm/application.h>
#include <gtkmm/aboutdialog.h>
#include <gtkmm/messagedialog.h>
#include <gtkmm/filechooserdialog.h> 
#include "administracion_gui.h"

/* Get the ui 
 * This two lines are for an production environment. */
#define UI_OPERACIONES PACKAGE_DATA_DIR"/ui/operacion.glade"
#define UI_ABOUT PACKAGE_DATA_DIR"/ui/acerca_de.glade"
/* This two macros are for the development environment */
//#define UI_OPERACIONES "gui/operacion.glade"
//#define UI_ABOUT "gui/acerca_de.glade"
/**\brief Namespace admin for the admin operations.
 *
 * Declares the gui for the admin operations of the plan proyect. May be on
 * follow versions will be integrated in plan namespace.*/ 
namespace admin
{
/*---------------------------------------------------------------------------*
 *                                                                           *
 *                    Constructors and destructors                           *
 *                                                                           *
 *---------------------------------------------------------------------------*/
/**\brief The constructor.
 *
 * Prepares the gui and the events. */
Admin_gui::Admin_gui(db::BaseDatos *conn):
    conn(conn),
    combo_branches(0),
    spinnButton(0),
    treeReports(0)
{
    refPageSetup = Gtk::PageSetup::create();
    refPrintSettings = Gtk::PrintSettings::create();
    builder = Gtk::Builder::create_from_file(UI_OPERACIONES);
    builder->get_widget("main_win", main_window);
    builder->get_widget("b_generar", ok_button);
    builder->get_widget("viewport1", port);
    builder->get_widget_derived("cb_filtro_sucursal", combo_branches);
    builder->get_widget("sb_filtros_year", spinnButton);
    builder->get_widget_derived("listviewInfo", treeReports);
    builder->get_widget("listviewSucursal", treeCapture);
    if(combo_branches->set_branches(conn))
        combo_branches->fill_combo();
    if(conn->ejecutaQuery("SELECT date_part('year', CURRENT_DATE)")){
        
        listCapture.set_treeview(treeCapture);
        listCapture.set_year(std::stoi(conn->regresaDato(0, 0)) + 1);
        load_events();
        load_reports_interface(true);
        treeReports->set_columns(UNEDITABLE);
    }
    else{
        Gtk::MessageDialog *md = new Gtk::MessageDialog(
                _("Can't get the year."),
                false,
                Gtk::MESSAGE_ERROR);
        md->set_secondary_text(conn->getLastError());
        md->run();
        delete md;
    }
}

/**Destructor*/
Admin_gui::~Admin_gui()
{}

/*---------------------------------------------------------------------------*
 *                                                                           *
 *                          Miscelanious operations                          *
 *                                                                           *
 *---------------------------------------------------------------------------*/
/**\brief Shows the window and its widgets.
 *
 * If all was correctly prepared, this function can load the gui to start
 * to work.*/
int Admin_gui::open_window(int argc, char *argv[])
{
    Glib::RefPtr<Gtk::Application> application =
        Gtk::Application::create(argc, argv, "cpaa.plan.admin");
    return application->run(*main_window);
}

/**\brief Help for the constructor.
 *
 * By this all the events are asigned to the right member functions in this
 * class. */
void Admin_gui::load_events()
{
    /* Create group actions */
    Glib::RefPtr<Gtk::ActionGroup> action_group =
        Glib::RefPtr<Gtk::ActionGroup>::cast_dynamic(
                builder->get_object("actions"));
    /* Gets the actions*/
    /*File menu*/
    save_action = Glib::RefPtr<Gtk::Action>::cast_dynamic(
                builder->get_object("guardar"));
    config_page_action = Glib::RefPtr<Gtk::Action>::cast_dynamic(
                builder->get_object("config_page"));
    preview_action = Glib::RefPtr<Gtk::Action>::cast_dynamic(
                builder->get_object("preview"));
    print_action = Glib::RefPtr<Gtk::Action>::cast_dynamic(
                builder->get_object("imprimir"));
    export_pdf_action = Glib::RefPtr<Gtk::Action>::cast_dynamic(
                builder->get_object("pdf"));
    quit_action = Glib::RefPtr<Gtk::Action>::cast_dynamic(
                builder->get_object("salir"));
    /*Edit menu*/
    add_action = Glib::RefPtr<Gtk::Action>::cast_dynamic(
            builder->get_object("editar_add"));
    delete_action = Glib::RefPtr<Gtk::Action>::cast_dynamic(
            builder->get_object("eliminar"));
    edit_action = Glib::RefPtr<Gtk::Action>::cast_dynamic(
            builder->get_object("editar_registro"));
    clear_action = Glib::RefPtr<Gtk::Action>::cast_dynamic(
            builder->get_object("edit_clear"));
    /*Preferences menu*/
    reports_option_action = Glib::RefPtr<Gtk::ToggleAction>::cast_dynamic(
            builder->get_object("muestra_reportes"));
    /*Help*/
    about_action = Glib::RefPtr<Gtk::Action>::cast_dynamic(
            builder->get_object("about"));

    /* Set the event to the action.*/
    /*File menu*/
    action_group->add(
            save_action,
            sigc::mem_fun(*this, &Admin_gui::clic_on_save));
    action_group->add(
            config_page_action,
            sigc::mem_fun(*this, &Admin_gui::clic_on_config_page));
    action_group->add(
            preview_action,
            sigc::mem_fun(*this, &Admin_gui::clic_on_preview));
    action_group->add(
            print_action,
            sigc::mem_fun(*this, &Admin_gui::clic_on_print));
    action_group->add(
            export_pdf_action,
            sigc::mem_fun(*this, &Admin_gui::clic_on_export_pdf));
    action_group->add(
            quit_action,
            sigc::mem_fun(*this, &Admin_gui::clic_on_exit));
    /* Edit menu */
    action_group->add(
            add_action,
            sigc::mem_fun(*this, &Admin_gui::clic_on_add));
    action_group->add(
            delete_action,
            sigc::mem_fun(*this, &Admin_gui::clic_on_delete));
    action_group->add(
            edit_action,
            sigc::mem_fun(*this, &Admin_gui::clic_on_edit));
    action_group->add(
            clear_action,
            sigc::mem_fun(*this, &Admin_gui::clic_on_clear));

    /* Preferences menu */
    action_group->add(
            reports_option_action,
            sigc::mem_fun(*this, &Admin_gui::clic_on_reports));
    /* Help menu */
    action_group->add(
            about_action,
            sigc::mem_fun(*this, &Admin_gui::clic_on_about));

    /* Other events */
    main_window->signal_delete_event().connect(
            sigc::mem_fun(*this, &Admin_gui::on_exit_window));
    ok_button->signal_clicked().connect(
            sigc::mem_fun(*this, &Admin_gui::on_ok_button));
}

/**\brief Set the interface for the reports.
 *
 * This is only able for the report.
 * @param "bool reports" True for the reports interface False for the
 * capture. */
void Admin_gui::load_reports_interface(bool reports)
{
    /*Options to disable.*/
    save_action->set_sensitive(false);
    delete_action->set_sensitive(!reports);
    edit_action->set_sensitive(!reports);
    add_action->set_sensitive(!reports);
    clear_action->set_sensitive(!reports);
    /*Options to enable.*/
    print_action->set_sensitive(reports);
    export_pdf_action->set_sensitive(reports);
    ok_button->set_sensitive(reports);
    port->remove();
    /*Tree view*/
    if(reports)
        port->add(*treeReports);
    else
        port->add(*treeCapture);
}

/**\brief Print or preview dialog.
 *
 * Call the print or the preview dialog.
 *\param "Gtk::PrintOperationAction print_action" Type of dialog*/
void Admin_gui::print_or_preview(Gtk::PrintOperationAction print_action)
{
    Glib::RefPtr<plan::PrintFormOperation> print = 
        plan::PrintFormOperation::create();
    print->set_list(treeReports->get_list());
    print->set_year(spinnButton->get_value());
    print->set_branch(combo_branches->get_active_text());

    print->set_track_print_status();
    print->set_default_page_setup(refPageSetup);
    print->set_print_settings(refPrintSettings);

    print->signal_done().connect(sigc::bind(sigc::mem_fun(*this,
        &Admin_gui::on_printoperation_done), print));
    
    int choose_dialog_button = Gtk::RESPONSE_OK;
    if(print_action == Gtk::PRINT_OPERATION_ACTION_EXPORT){
        Gtk::FileChooserDialog filechooser(_("Select a file please."),
                Gtk::FILE_CHOOSER_ACTION_SAVE);
        filechooser.set_transient_for(*main_window);
        filechooser.add_button(_("Save"), Gtk::RESPONSE_OK);
        filechooser.add_button(_("Cancel"), Gtk::RESPONSE_CANCEL);

        Glib::RefPtr<Gtk::FileFilter> file_filter = Gtk::FileFilter::create();
        file_filter->set_name(_("Pdf file"));
        file_filter->add_mime_type(".pdf");
        filechooser.add_filter(file_filter);
        
        if((choose_dialog_button = filechooser.run()) == Gtk::RESPONSE_OK)
            print->set_export_filename(filechooser.get_filename());
            
    }
    if(choose_dialog_button == Gtk::RESPONSE_OK)
        try
        {
            print->run(print_action, *main_window);
        }
        catch (const Gtk::PrintError& ex)
        {
            //See documentation for exact Gtk::PrintError error codes.
            Gtk::MessageDialog msg(
                *main_window,
                "An error occurred while trying to run a print operation:",
                false,
                Gtk::MESSAGE_ERROR);
            msg.set_secondary_text(ex.what());
            msg.run();
        }
    
}
/*---------------------------------------------------------------------------*
 *                                                                           *
 *                               Events                                      *
 *                                                                           *
 *---------------------------------------------------------------------------*/
/*                            Actions events                                 */
/*                            Menu file events                               */
/**\brief Save what is is the treeview at this time.
 *
 * Call the function of the object listCapture for store the information on the
 * database.*/
void Admin_gui::clic_on_save()
{
    Gtk::MessageDialog dialog(
            *main_window,
            _("Do you really want to save the information"),
            false,
            Gtk::MESSAGE_QUESTION,
            Gtk::BUTTONS_YES_NO,
            false);
    if(dialog.run() == Gtk::RESPONSE_YES){
        std::string message_exception("");
        try{
            listCapture.save_information(conn);
            add_action->set_sensitive(true);
            edit_action->set_sensitive(true);
            delete_action->set_sensitive(true);
            save_action->set_sensitive(false);
        }
        catch(std::exception &e){
            message_exception = e.what();
        }
        catch(std::string e){
            message_exception = e;
        }
        catch(char * e){
            message_exception = e;
        }
        if(message_exception != ""){
            Gtk::MessageDialog me(*main_window,
                    _("Problem with saving data"), false, Gtk::MESSAGE_ERROR);
            me.set_secondary_text(message_exception);
            me.run();
            listCapture.clear();
        } // If there is an exception.
    }// End of if principal.
}

/**\brief Config the page to print. */
void Admin_gui::clic_on_config_page()
{
    //Show the page setup dialog, asking it to start with the existing settings
    Glib::RefPtr<Gtk::PageSetup> new_page_setup =
        Gtk::run_page_setup_dialog(
                *main_window, refPageSetup, refPrintSettings);
    //Save the chosen page setup dialog for use when printing, previewing, or
    //showing the page setup dialog again:
    refPageSetup = new_page_setup;
}

/**\brief Show the preview dialog.*/
void Admin_gui::clic_on_preview()
{
    print_or_preview(Gtk::PRINT_OPERATION_ACTION_PREVIEW);
}

/**\brief Show the print dialog.*/
void Admin_gui::clic_on_print()
{
    print_or_preview(Gtk::PRINT_OPERATION_ACTION_PRINT_DIALOG);
}

/**\brief Print to a pdf file.*/
void Admin_gui::clic_on_export_pdf()
{
    print_or_preview (Gtk::PRINT_OPERATION_ACTION_EXPORT);
}

/**\brief Close the main window.
 *
 * Close the main window and send the TERM signal to the process. */
void Admin_gui::clic_on_exit()
{
    Gtk::MessageDialog dialog(
            *main_window,
            _("Do you really want to quit?"),
            false,
            Gtk::MESSAGE_QUESTION,
            Gtk::BUTTONS_YES_NO,
            false);
    if(dialog.run() == Gtk::RESPONSE_YES)
        main_window->hide();
}

/*                         Menu edit events                                 */
/**\brief Prepares the TreeView with the new year records.
 *
 * With this function the tree view is filled with one row by branch and
 * theirs other columns with empty values */
void Admin_gui::clic_on_add()
{
    Gtk::MessageDialog *exceptions = new Gtk::MessageDialog(
            *main_window, _("Error for a new budget."), false,
            Gtk::MESSAGE_ERROR);
    try{
        if(listCapture.prepare_to_insert(conn)){
            add_action->set_sensitive(false);
            edit_action->set_sensitive(false);
            delete_action->set_sensitive(false);
            save_action->set_sensitive(true);
        }
        else{
            Gtk::MessageDialog error(
                    *main_window,
                    _("Database mistake"),
                    false,
                    Gtk::MESSAGE_ERROR);
            error.set_secondary_text("That is the big mistake");
            error.run();
        } // If..else
    } // try
    catch(std::exception &e){
        exceptions->set_secondary_text(e.what());
        exceptions->run();
        listCapture.clear();
    }
    catch(std::string e){
        exceptions->set_secondary_text(e);
        exceptions->run();
        listCapture.clear();
    }
    catch(char *e){
        exceptions->set_secondary_text(e);
        exceptions->run();
        listCapture.clear();
    }// End of block try..catch
    delete exceptions;
} // function end

/**\brief Prepares the TreeView with the new year records.
 *
 * With this function the tree view is filled with one row by branch and
 * theirs other columns with empty values */
void Admin_gui::clic_on_delete()
{
    Gtk::MessageDialog delete_info(
            *main_window, _("Delete the budget?"), false,
            Gtk::MESSAGE_QUESTION,
            Gtk::BUTTONS_YES_NO);
    delete_info.set_secondary_text(
            _("You will not be able to rollback this operation."));
    if(delete_info.run() == Gtk::RESPONSE_YES){
        delete_info.set_secondary_text(_(
                "If you say yes \
                all information for start the next year will be deleted."));
        if(delete_info.run() == Gtk::RESPONSE_YES){
            Gtk::MessageDialog *exceptions = new Gtk::MessageDialog(
                    *main_window, _("Error for a new budget."), false,
                    Gtk::MESSAGE_ERROR);
            try{
                if(listCapture.delete_info(conn)){
                    add_action->set_sensitive(false);
                    edit_action->set_sensitive(false);
                    delete_action->set_sensitive(false);
                    save_action->set_sensitive(false);
                }
                else{
                    Gtk::MessageDialog error(
                            *main_window,
                            _("Database mistake"),
                            false,
                            Gtk::MESSAGE_ERROR);
                    error.set_secondary_text(_("That is the big mistake"));
                    error.run();
                } // If..else
            } // try
            catch(std::exception &e){
                exceptions->set_secondary_text(e.what());
                exceptions->run();
            }
            catch(std::string e){
                exceptions->set_secondary_text(e);
                exceptions->run();
            }
            catch(char *e){
                exceptions->set_secondary_text(e);
                exceptions->run();
            }// End of block try..catch
            delete exceptions;
        } // If Second if sure.
    } // If first if sure
}

/**\brief Clear the Gtk::TreeView.
 *
 * Clear the Gtk::TreeView. */
void Admin_gui::clic_on_clear()
{
    /*Options to disable.*/
    delete_action->set_sensitive(true);
    edit_action->set_sensitive(true);
    add_action->set_sensitive(true);
    save_action->set_sensitive(false);
    listCapture.clear();
}

/**\brief Prepares the TreeView to edit records.
 *
 * With this function the tree view is filled with one row by branch and
 * theirs other columns with stored values. */
void Admin_gui::clic_on_edit()
{
    Gtk::MessageDialog *exceptions = new Gtk::MessageDialog(
            *main_window, _("Error for a new budget."), false,
            Gtk::MESSAGE_ERROR);
    try{
        if(listCapture.prepare_to_update(conn)){
            add_action->set_sensitive(false);
            edit_action->set_sensitive(false);
            delete_action->set_sensitive(false);
            save_action->set_sensitive(true);
        }
        else{
            Gtk::MessageDialog error(
                    *main_window,
                    _("Database mistake"),
                    false,
                    Gtk::MESSAGE_ERROR);
            error.set_secondary_text(_("That is the big mistake"));
            error.run();
        } // If..else
    } // try
    catch(std::exception &e){
        exceptions->set_secondary_text(e.what());
        exceptions->run();
    }
    catch(std::string e){
        exceptions->set_secondary_text(e);
        exceptions->run();
    }
    catch(char *e){
        exceptions->set_secondary_text(e);
        exceptions->run();
    }// End of block try..catch
    delete exceptions;
}

/**\brief Choose betweeen two interfaces.
 *
 * If is selected is able to create reports, elsewere can create a new
 * capture of the year.*/
void Admin_gui::clic_on_reports()
{
    load_reports_interface(reports_option_action->get_active());
}

/**\brief Show the about dialog.
 *
 * This function is associated with the about action*/
void Admin_gui::clic_on_about()
{
    Glib::RefPtr<Gtk::Builder> about_builder = 
        Gtk::Builder::create_from_file(UI_ABOUT);
    Gtk::AboutDialog *about_dialog = 0;
    about_builder->get_widget("aboutdialog", about_dialog);
    about_dialog->set_transient_for(*main_window);
    about_dialog->run();
}
/*                         Other events.                                     */
/**\brief This function should be associated to the signal_delete_event.
 *
 * Ask if the user want to close the application, if the answer is yes then
 * the application is clossed, elsewere, the application still working.*/
bool Admin_gui::on_exit_window(GdkEventAny *event)
{
    Gtk::MessageDialog dialog(
            *main_window,
            _("Do you really want to quit?"),
            false,
            Gtk::MESSAGE_QUESTION,
            Gtk::BUTTONS_YES_NO,
            false);
    return (dialog.run() == Gtk::RESPONSE_YES? false: true);
}

/**\brief This function is to create an sql query for the statistics.
 *
 * When clic on the ok button.*/
void Admin_gui::on_ok_button()
{
    Gtk::MessageDialog message(
            *main_window, _("Information process load."));
    try{
        treeReports->fill_list_with_db_statistics(
            spinnButton->get_value(), combo_branches->get_active_text(), conn);
       message.set_secondary_text(_("Success"));
    }
    catch(std::string ex){
        message.set_secondary_text(ex);
    }
    catch(char *ex){
        message.set_secondary_text(ex);
    }
    message.run();
}

/**\brief This event is called when the print process present a change.
 * 
 * When there is a change this function is called.  
 * \param "const Glib::RefPtr<plan::PrintFormOperation>& operation"*/
void Admin_gui::on_print_operation_status_changed(
            const Glib::RefPtr<plan::PrintFormOperation>& operation)
{
    Glib::ustring status_msg;
    if (operation->is_finished())
        status_msg = _("Print job completed.");
    else
        //You could also use get_status().
        status_msg = operation->get_status_string();

    Gtk::MessageDialog msg_status(*main_window, status_msg);
    msg_status.run();
}

/**\brief This event is called when the print process is done.
 * 
 * When the print proccess is done this function is called.  
 * \param "const Glib::RefPtr<plan::PrintFormOperation>& operation"*/
void Admin_gui::on_printoperation_done(
            Gtk::PrintOperationResult result,
            const Glib::RefPtr<plan::PrintFormOperation>& operation)
{
    //Printing is "done" when the print data is spooled.
    if (result == Gtk::PRINT_OPERATION_RESULT_ERROR){
        Gtk::MessageDialog err_dialog(
                *main_window, "Error printing form", false, Gtk::MESSAGE_ERROR,
                Gtk::BUTTONS_OK, true);
        err_dialog.run();
    }
    else
        if(result == Gtk::PRINT_OPERATION_RESULT_APPLY){
            //Update PrintSettings with the ones used in this PrintOperation:
            refPrintSettings = operation->get_print_settings();
        }

    if (! operation->is_finished()){
        //We will connect to the status-changed signal to track status
        //and update a status bar. In addition, you can, for example,
        //keep a list of active print operations, or provide a progress dialog.
        operation->signal_status_changed().connect(
            sigc::bind(
                sigc::mem_fun(
                    *this,
                    &Admin_gui::on_print_operation_status_changed),
                operation));
    }
}

} // End of the namespace
