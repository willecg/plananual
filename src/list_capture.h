/*
 * planadmin: list_capture.h
 * Control a ListStore for the capture of the news records.
 *
 * Copyright © 2014 William Ernesto Cárdenas Gomez
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef LIST_CAPTURE_H
#define LIST_CAPTURE_H

#include <gtkmm/treeview.h>
#include <gtkmm/liststore.h>
#include <gtkmm/treemodelcolumn.h>
#include "../lib/baseDatos.h"
#include "estadisticas/estadistica_enteros.h"
#include "estadisticas/estadistica_reales.h"
#include "catalogos/sucursal.h"

/**\brief Namespace admin for the admin operations.
 *
 * Declares the gui for the admin operations of the plan proyect. May be on
 * follow versions will be integrated in plan namespace.*/
namespace admin{

const int MINMONTH = 1; /**<Base number of the months 1 is january.*/
const int MAXMONTH = 13;/**<Number of element for an array for months zero is
                          unused.*/
const int STATISTICSNUM = 9;

/*---------------------------------------------------------------------------*
 *                                                                           *
 *                        Functions miscelaneus                              *
 *                                                                           *
 *---------------------------------------------------------------------------*/
/** \brief Create an empy array for the statistics.
 *
 * Get an array of estadisticas::Estadisticas objects and fill it with empty
 * references to them.
 * \param "estadisticas::Estadistica * stats[][]" Matrix of references of
 * stats.*/
void create_empty_statistics(
        estadisticas::Estadistica * stats[][STATISTICSNUM]);
/** \brief Delete an matrix of statistics.
 *
 * Clear the memory form the matrix of estadisticas::Estadistica.
 * \param "estadisticas::Estadistica * stats[][]" Matrix of references of
 * stats.*/
void delete_statistics(estadisticas::Estadistica * stats[][STATISTICSNUM]);

/**\brief Operation to execute on data base.
 *
 * When the user want to add news record to the database the option most be
 * setted to ADD, but if the user want to update then the operation must be
 * UPDATE*/
enum saving_type{
    ADD, /**<Used to indicate that the operation must be an INSERT.*/
    UPDATE, /**<Used to indicate that the operation must be an UPDATE.*/
    DELETE, /**<Used to indicate that the operation must be a DELETE.*/
    NONE /**<Not indicated operation.*/
};

/**\brief Widget for the capture of budgets.
 *
 * This widget control a Gtk::ListStore and drives the operations on the data
 * base by the Estadistica objects.*/
class ListCapture{
public:
/*---------------------------------------------------------------------------*
 *                                                                           *
 *                        Constructor & Destructor                           *
 *                                                                           *
 *---------------------------------------------------------------------------*/
    /**\brief Default constructor.
     *
     * Set the default model for the list store. */
    ListCapture();
    /**\brief Constructor with default treeview pointer.
     *
     * Set the default model for the list store and prepares the treeview.
     * \param "Gtk::TreeView *treeView" Widget Gtk::TreeView to work with.*/
    ListCapture(Gtk::TreeView *treeView, int year);
    /**Destructor*/
    virtual ~ListCapture();
/*---------------------------------------------------------------------------*
 *                                                                           *
 *                   Set Get member functions                                *
 *                                                                           *
 *---------------------------------------------------------------------------*/
/*                 Set member functions                                      */
    /**\brief Set the TreeView pointer Widget.
     *
     * Set the TreeView at the member of this class. Also set it model and add
     * the columns.
     * \param "Gtk::TreeView *tree" Widget Gtk::TreeView to work with.*/
    void set_treeview(Gtk::TreeView *tree);
    /**\brief Creates the Gtk::ListStore and set its model.
     *
     * There is no way to set another model.*/
    void set_list();
    /**\Set the year.
     *
     * The year should be the next of today year.
     * \param "const int year" The year to use to capture.*/
    void set_year(int year);

/*                    Get member functions.                                  */
    /**\brief Gets the ListStore model from the treeview.
     *
     * You should set the year first.*/
    Glib::RefPtr<Gtk::ListStore> get_list();

    /**\brief Get the year.
     *
     * Obtain the year to capture.
     * \return the year as int. */
    int get_year();
    
    /**\brief Get the year.
     *
     * Obtain the year to capture.
     * \return the year as std::string. */
    std::string get_year_str();
/*---------------------------------------------------------------------------*
 *                                                                           *
 *                         Operational functions.                            *
 *                                                                           *
 *---------------------------------------------------------------------------*/
    /**\brief Fill the table with empty information.
     *
     * Prepare the table with empty information. This should be use for create
     * new information in the database. (estadisticas::Estadisticas
     * information.)
     * \param "db::BaseDatos *conn" pointer to the database connection.
     * \return true if success, elsewere false.*/
    bool prepare_to_insert(db::BaseDatos *conn);
    /**\brief Fill the table with stored information.
     *
     * This should be use for update information that already exist in the
     * database (estadisticas::Estadisticas information).
     * \param "db::BaseDatos *conn" pointer to the database connection.
     * \return true if success, elsewere false.*/
    bool prepare_to_update(db::BaseDatos *conn);
    /**\brief Destroy the information stored in the table with the year.
     *
     * The use of this operation is only for destroy all the information of a
     * sigle year. The only year that can be destroyed will be is what is
     * establish in the year member field.
     * \param "db::BaseDatos *conn" pointer to the database connection.
     * \return true if success, elsewere false.*/
    bool delete_info(db::BaseDatos *conn);
    /**\brief Update the database conn with new information.
     *
     * The information is obtained from the list store.*/
    void save_information(db::BaseDatos *conn);
private:
    /** Insert in the data base with the treeview information. */
    void insert(db::BaseDatos *conn);
    /** Update the data base with the treeview information. */
    void update(db::BaseDatos *conn);
    

/*---------------------------------------------------------------------------*
 *                                                                           *
 *                            Widgets operations.                            *
 *                                                                           *
 *---------------------------------------------------------------------------*/
public:
    /**\brief Get row data.
     *
     * Recieve a TipoCuenta param and return the value that the row have.
     * \param "Gtk::TreeModel::Row row" Row to read.
     * \param "estadisticas::TipoCuenta data" Column to affect.
     * \return te value stored in the table.*/
    double get_row_data(
            Gtk::TreeModel::Row row, estadisticas::TipoCuenta data);
    /**\brief Set row data.
     *
     * Set avalue for the especified row and in the especify TipoCuenta.
     * \param "Gtk::TreeModel::Row row" Row to update.
     * \param "estadisticas::TipoCuenta data" Column to affect.
     * \param "double value" value to set.*/
    void set_row_data
        (Gtk::TreeModel::Row row, estadisticas::TipoCuenta data, double value);

    /**\brief Clear the liststore ref.
     *
     * Clear the data on the liststore. */
    void clear();
/*---------------------------------------------------------------------------*
 *                                                                           *
 *                     Inner or nested class for the model                   *
 *                                                                           *
 *---------------------------------------------------------------------------*/
public:
    /**\brief Class to define the model for the ListStore.
     *
     * This class is defined to be used as a ListStore model.
     * This inherits from the Gtk::TreeModelColumnRecord.*/
    class Model : public Gtk::TreeModelColumnRecord{
    public:
        /*\brief Constructor of Model Class.
         *
         * Prepares the model adding at its father class the columns.*/
        Model()
        {
            add(branch);
            add(partners);
            add(children);
            add(saving_partners);
            add(saving_children);
            add(deposit);
            add(fixed_deposit);
            add(loan);
            add(defaulting);
        }

        Gtk::TreeModelColumn<std::string> branch; // Branch
        Gtk::TreeModelColumn<int> partners;       // Number of partners
        Gtk::TreeModelColumn<int> children;       // Number of children
        Gtk::TreeModelColumn<double> saving_partners; // Saving of partners
        Gtk::TreeModelColumn<double> saving_children; // Saving of children
        Gtk::TreeModelColumn<double> deposit; // Deposit on demand general
        Gtk::TreeModelColumn<double> fixed_deposit; // Fixed deposit 
        Gtk::TreeModelColumn<double> loan; // Loan gived by the branch
        Gtk::TreeModelColumn<double> defaulting; // % of delay in payment
    };
private:
    Glib::RefPtr<Gtk::ListStore> list;// List Store model
    Gtk::TreeView *tree_view; // Widget TreeView pointer
    Model model; // Model of the listStore
    estadisticas::Estadistica * stats[MAXMONTH][STATISTICSNUM];
    int year; // Year for the new capture this should be the next year.
    saving_type saving;
};
} //Namespace end
#endif
